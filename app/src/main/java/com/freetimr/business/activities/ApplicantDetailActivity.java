package com.freetimr.business.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freetimr.business.R;
import com.freetimr.business.adapter.CityAdapter;
import com.freetimr.business.adapter.JobListDialogAdapter;
import com.freetimr.business.adapter.SkillAdapter;
import com.freetimr.business.models.CandidateProfileModel;
import com.freetimr.business.models.CandidateSkills;
import com.freetimr.business.models.JobApplicationModel;
import com.freetimr.business.models.JobsModel;
import com.freetimr.business.models.ShortListCandidateModel;
import com.freetimr.business.models.SkillModel;
import com.freetimr.business.utils.FreeTimrURLs;
import com.freetimr.business.utils.FreeTimrUtils;
import com.freetimr.business.utils.ObservableScrollable;
import com.freetimr.business.utils.OnScrollChangedCallback;
import com.freetimr.business.utils.SystemBarTintManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.freetimr.business.utils.FreeTimrURLs.GET_CANDIDATE_PROFILE_BY_CANDIDATE_ID;

public class ApplicantDetailActivity extends AppCompatActivity {

    private Drawable mActionBarBackgroundDrawable;
    Toolbar mToolbar;
    private View mHeader;
    private int mLastDampedScroll;
    private int mInitialStatusBarColor;
    private int mFinalStatusBarColor;
    private SystemBarTintManager mStatusBarManager;

    ImageView profileImage, profileHeader;
    RecyclerView cityRecyclerView, skillsRecyclerView;
    TextView profileCandidateName, profileCandidateDesignationLocation, profileCandidateChat, profileCandidateDescription;

    SkillAdapter skillAdapter;
    CityAdapter cityAdapter;

    String candidateId;

    Button shortListButton, rejectButton;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_applicant_detail);

//        if (Build.VERSION.SDK_INT >= 21) {
//            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
//        }
//
//        // making notification bar transparent
//        changeStatusBarColor();

        mToolbar = (Toolbar) findViewById(R.id.profile_toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mActionBarBackgroundDrawable = getResources().getDrawable(R.color.background_color_toolbar);

        mStatusBarManager = new SystemBarTintManager(this);
        mStatusBarManager.setStatusBarTintEnabled(true);
        mInitialStatusBarColor = Color.TRANSPARENT;
        mFinalStatusBarColor = getResources().getColor(R.color.background_color_toolbar);

        ObservableScrollable scrollView = (ObservableScrollable) findViewById(R.id.profile_scrollview);
        mHeader = findViewById(R.id.profile_header);

        scrollView.setOnScrollChangedCallback(new OnScrollChangedCallback() {
            @Override
            public void onScroll (int l, int scrollPosition) {
                int headerHeight = mHeader.getHeight() - mToolbar.getHeight();
                float ratio = 0;
                if (scrollPosition > 0 && headerHeight > 0)
                    ratio = (float) Math.min(Math.max(scrollPosition, 0), headerHeight) / headerHeight;

                updateActionBarTransparency(ratio);
                updateStatusBarColor(ratio);
                updateParallaxEffect(scrollPosition);
            }
        });

        profileCandidateName = (TextView) findViewById(R.id.profile_candidate_name);
        profileCandidateDesignationLocation = (TextView) findViewById(R.id.profile_candidate_designation_location);
        profileCandidateChat = (TextView) findViewById(R.id.profile_candidate_chat);
        profileCandidateDescription = (TextView) findViewById(R.id.profile_candidate_description);
        profileImage = (ImageView) findViewById(R.id.profile_image);
        profileHeader = (ImageView) findViewById(R.id.profile_header);
        cityRecyclerView = (RecyclerView) findViewById(R.id.city_recycler_view);
        skillsRecyclerView = (RecyclerView) findViewById(R.id.skills_recycler_view);
        shortListButton = (Button) findViewById(R.id.shortlist_btn);
        rejectButton = (Button) findViewById(R.id.reject_btn);

        candidateId = getIntent().getStringExtra("CandidateId");
        getCandidateInfo(candidateId);

        shortListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                getJob();
            }
        });

    }

    private void updateActionBarTransparency (float scrollRatio) {
        int newAlpha = (int) (scrollRatio * 255);
        mActionBarBackgroundDrawable.setAlpha(newAlpha);
        mToolbar.setBackground(mActionBarBackgroundDrawable);
    }

    private void updateStatusBarColor (float scrollRatio) {
        int r = interpolate(Color.red(mInitialStatusBarColor), Color.red(mFinalStatusBarColor), 1 - scrollRatio);
        int g = interpolate(Color.green(mInitialStatusBarColor), Color.green(mFinalStatusBarColor), 1 - scrollRatio);
        int b = interpolate(Color.blue(mInitialStatusBarColor), Color.blue(mFinalStatusBarColor), 1 - scrollRatio);
        mStatusBarManager.setTintColor(Color.rgb(r, g, b));
    }

    private void updateParallaxEffect (int scrollPosition) {
        float damping = 0.5f;
        int dampedScroll = (int) (scrollPosition * damping);
        int offset = mLastDampedScroll - dampedScroll;
        mHeader.offsetTopAndBottom(offset);

        mLastDampedScroll = dampedScroll;
    }

    private int interpolate (int from, int to, float param) {
        return (int) (from * param + to * (1 - param));
    }

    private void getCandidateInfo (String candidateId) {

        FreeTimrUtils.showProgressDialog(this);

        Call<CandidateProfileModel[]> call = FreeTimrUtils.getRetrofit().getCandidateProfileInfo(FreeTimrURLs.HOST + GET_CANDIDATE_PROFILE_BY_CANDIDATE_ID + candidateId);
        call.enqueue(new Callback<CandidateProfileModel[]>() {
            @Override
            public void onResponse (Call<CandidateProfileModel[]> call, Response<CandidateProfileModel[]> response) {
                if (response.code() == 200) {
                    setData(response.body()[0]);
                } else {
                    FreeTimrUtils.showMessage(ApplicantDetailActivity.this, "Something went wrong. Please try again later.");
                }
                FreeTimrUtils.cancelCurrentDialog(ApplicantDetailActivity.this);
            }

            @Override
            public void onFailure (Call<CandidateProfileModel[]> call, Throwable t) {
                FreeTimrUtils.log(t.getLocalizedMessage());
                FreeTimrUtils.cancelCurrentDialog(ApplicantDetailActivity.this);
            }
        });
    }

    private void setData (CandidateProfileModel model) {
        profileCandidateName.setText(model.Candidate.CandidateName);
        profileCandidateDesignationLocation.setText(model.Location);
        profileCandidateDescription.setText(model.ProfileHeadline);
        Picasso.with(this).load(model.Candidate.CandidateImage).into(profileImage);

        skillAdapter = new SkillAdapter(this, new ArrayList<>(Arrays.asList(model.CandidateSkill)));
        skillsRecyclerView.setAdapter(skillAdapter);
        skillsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        cityAdapter = new CityAdapter(this, new ArrayList<>(Arrays.asList(model.CandidatePreferredCity)));
        cityRecyclerView.setAdapter(cityAdapter);
        cityRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
    }

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor () {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void shortListCandidate (ShortListCandidateModel model) {
        FreeTimrUtils.showProgressDialog(ApplicantDetailActivity.this);

        FreeTimrUtils.log(FreeTimrUtils.newGson().toJson(model));

        Call<JobApplicationModel> call = FreeTimrUtils.getRetrofit().shortListCandidate(model);
        call.enqueue(new Callback<JobApplicationModel>() {
            @Override
            public void onResponse (Call<JobApplicationModel> call, Response<JobApplicationModel> response) {
                if (response.code() == 200) {
                    rejectButton.setVisibility(View.GONE);
                    shortListButton.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 2));
                } else {
                    FreeTimrUtils.showMessage(ApplicantDetailActivity.this, "Something else went wrong. Please try again.");
                }
                FreeTimrUtils.cancelCurrentDialog(ApplicantDetailActivity.this);
            }

            @Override
            public void onFailure (Call<JobApplicationModel> call, Throwable t) {
                FreeTimrUtils.log(t.getLocalizedMessage());
                FreeTimrUtils.cancelCurrentDialog(ApplicantDetailActivity.this);
            }
        });
    }

    private void getJob () {
        FreeTimrUtils.showProgressDialog(this);

        Call<JobsModel[]> call = FreeTimrUtils.getRetrofit().getJobsByCompanyId(FreeTimrURLs.HOST + FreeTimrURLs.GET_JOB_BY_COMPANY + FreeTimrUtils.getCompanyModel(this).CompanyId);

        FreeTimrUtils.log(FreeTimrURLs.HOST + FreeTimrURLs.GET_JOB_BY_COMPANY + FreeTimrUtils.getCompanyModel(this).CompanyId);

        call.enqueue(new Callback<JobsModel[]>() {
            @Override
            public void onResponse (Call<JobsModel[]> call, Response<JobsModel[]> response) {
                FreeTimrUtils.log("" + response.code());
                if (response.body().length != 0) {
                    showJobsDialog(response.body());
                } else {
                    postAjobDialog();
                }
                FreeTimrUtils.cancelCurrentDialog(ApplicantDetailActivity.this);
            }

            @Override
            public void onFailure (Call<JobsModel[]> call, Throwable t) {
                FreeTimrUtils.log("" + t);
                FreeTimrUtils.cancelCurrentDialog(ApplicantDetailActivity.this);
            }
        });
    }

    public void postAjobDialog () {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.post_a_new_job_dialog, null);
        dialogBuilder.setView(dialogView);

        dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick (DialogInterface dialog, int whichButton) {
                Intent postJobIntent = new Intent(ApplicantDetailActivity.this, PostAJobActivity.class);
                postJobIntent.putExtra("CanSkip", false);
                startActivity(postJobIntent);
            }
        });
        dialogBuilder.setNegativeButton("Later", new DialogInterface.OnClickListener() {
            public void onClick (DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void showJobsDialog (JobsModel[] models) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.jobs_list_dialog, null);
        dialogBuilder.setView(dialogView);

        RecyclerView jobListDialog = (RecyclerView) dialogView.findViewById(R.id.job_list_dialog);

        final JobListDialogAdapter adapter = new JobListDialogAdapter(this, models);

        jobListDialog.setAdapter(adapter);
        jobListDialog.setLayoutManager(new LinearLayoutManager(this));

        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick (DialogInterface dialog, int whichButton) {
                ShortListCandidateModel model = new ShortListCandidateModel();
                model.CandidateId = candidateId;
                model.UpdatedByEmployerId = FreeTimrUtils.getAdminEmployeeInfo(ApplicantDetailActivity.this).EmployeeId;
                model.JobId = adapter.getJobId();

                shortListCandidate(model);
            }
        });

        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick (DialogInterface dialog, int whichButton) {

            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }
}
