package com.freetimr.business.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.freetimr.business.R;
import com.freetimr.business.adapter.ApplicantListAdapter;
import com.freetimr.business.models.CompanyModel;
import com.freetimr.business.models.JobApplicationModel;
import com.freetimr.business.models.JobsModel;
import com.freetimr.business.utils.FreeTimrURLs;
import com.freetimr.business.utils.FreeTimrUtils;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApplicantListActivity extends AppCompatActivity {

    ArrayList<JobApplicationModel> list;
    TextView jobTitle;
    RecyclerView applicantListRecyclerView;
    String jobTitleStr;
    ApplicantListAdapter adapter;
    Toolbar mToolbar;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_applicant_list);
        mToolbar = (Toolbar) findViewById(R.id.job_applicants_toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        jobTitleStr = getIntent().getStringExtra("JobApplicantJobTitle");
        list = getIntent().getParcelableArrayListExtra("JobApplicantList");

        jobTitle = (TextView) findViewById(R.id.applicant_job_title);
        applicantListRecyclerView = (RecyclerView) findViewById(R.id.applicant_list);

        jobTitle.setText(jobTitleStr);

        adapter = new ApplicantListAdapter(ApplicantListActivity.this, list);
        applicantListRecyclerView.setAdapter(adapter);
        applicantListRecyclerView.addItemDecoration(new DividerItemDecoration(ApplicantListActivity.this, DividerItemDecoration.VERTICAL));
        applicantListRecyclerView.setLayoutManager(new LinearLayoutManager(ApplicantListActivity.this));

    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

//    private void getCompanyModel (String companyId) {
//        FreeTimrUtils.showProgressDialog(this);
//        Call<CompanyModel> call = FreeTimrUtils.getRetrofit().getCompanyModel(FreeTimrURLs.HOST + FreeTimrURLs.GET_COMPANY + companyId);
//        call.enqueue(new Callback<CompanyModel>() {
//            @Override
//            public void onResponse (Call<CompanyModel> call, Response<CompanyModel> response) {
//                FreeTimrUtils.log("" + response.code());
//
//                adapter = new ApplicantListAdapter(ApplicantListActivity.this, list);
//                applicantListRecyclerView.setAdapter(adapter);
//                applicantListRecyclerView.addItemDecoration(new DividerItemDecoration(ApplicantListActivity.this, DividerItemDecoration.VERTICAL));
//                applicantListRecyclerView.setLayoutManager(new LinearLayoutManager(ApplicantListActivity.this));
//                FreeTimrUtils.cancelCurrentDialog(ApplicantListActivity.this);
//            }
//
//            @Override
//            public void onFailure (Call<CompanyModel> call, Throwable t) {
//                FreeTimrUtils.log("" + t);
//                FreeTimrUtils.cancelCurrentDialog(ApplicantListActivity.this);
//            }
//        });
//    }
}
