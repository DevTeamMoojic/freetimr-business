package com.freetimr.business.activities;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.freetimr.business.R;
import com.freetimr.business.adapter.CandidateListPagerAdapter;
import com.freetimr.business.models.CandidateProfileModel;
import com.freetimr.business.models.CandidateSearchRequestModel;
import com.freetimr.business.models.CandidateSkills;
import com.freetimr.business.models.SkillModel;
import com.freetimr.business.utils.FreeTimrUtils;
import com.freetimr.business.views.SlidingTabLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CandidateListActivity extends AppCompatActivity {

    SlidingTabLayout tabLayout;
    ViewPager jobListViewPager;
    Toolbar mToolbar;
    CandidateListPagerAdapter pagerAdapter;
    LinearLayout candidateContainer, noCandidateContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_list);
        mToolbar = (Toolbar) findViewById(R.id.job_list_toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tabLayout = (SlidingTabLayout) findViewById(R.id.jobs_tab);
        jobListViewPager = (ViewPager) findViewById(R.id.job_list_view_pager);
        candidateContainer = (LinearLayout) findViewById(R.id.candidate_container);
        noCandidateContainer = (LinearLayout) findViewById(R.id.no_candidate_container);

        getCandidateSearch();
    }

    private void getCandidateSearch() {

        FreeTimrUtils.showProgressDialog(this);

        CandidateSearchRequestModel candidateSearchRequestModel = new CandidateSearchRequestModel();
        candidateSearchRequestModel.AvailabilityTimeId = "";
        candidateSearchRequestModel.WorkLocationId = "";
        candidateSearchRequestModel.KeywordTosearch = "";
        candidateSearchRequestModel.Gender = "";
        candidateSearchRequestModel.Location = "";

        FreeTimrUtils.log("SEARCH: " + FreeTimrUtils.newGson().toJson(candidateSearchRequestModel));

        Call<CandidateProfileModel[]> call = FreeTimrUtils.getRetrofit().candidateSearch(candidateSearchRequestModel);
        call.enqueue(new Callback<CandidateProfileModel[]>() {
            @Override
            public void onResponse(Call<CandidateProfileModel[]> call, Response<CandidateProfileModel[]> response) {
                FreeTimrUtils.log("" + response.code());
                if (response.code() == 200) {
                    noCandidateContainer.setVisibility(View.GONE);
                    candidateContainer.setVisibility(View.VISIBLE);
                    mapToplogyAndCandidates(Arrays.asList(response.body()), FreeTimrUtils.getSkillsList(CandidateListActivity.this));
                } else {
                    noCandidateContainer.setVisibility(View.VISIBLE);
                    candidateContainer.setVisibility(View.GONE);
                    FreeTimrUtils.showMessage(CandidateListActivity.this, "No candidates to show.");
                }
                FreeTimrUtils.cancelCurrentDialog(CandidateListActivity.this);
            }

            @Override
            public void onFailure(Call<CandidateProfileModel[]> call, Throwable t) {
                FreeTimrUtils.log("" + t);
                FreeTimrUtils.cancelCurrentDialog(CandidateListActivity.this);
            }
        });
    }

    private void mapToplogyAndCandidates(List<CandidateProfileModel> candidateProfileModelList, ArrayList<SkillModel> candidateSkills) {
        if (candidateProfileModelList != null && candidateProfileModelList.size() > 0 && candidateSkills != null && candidateSkills.size() > 0) {
            FreeTimrUtils.cancelCurrentDialog(this);
            HashMap<Integer, ArrayList<CandidateProfileModel>> candidateMap = new HashMap<>();
            for (int i = 0; i < candidateProfileModelList.size(); i++) {
                CandidateProfileModel candidateProfileModelTemp = candidateProfileModelList.get(i);
                for (int j = 0; j < candidateProfileModelTemp.CandidateSkill.length; j++) {
                    SkillModel candidateSkillsTemp = getCategoryNameById(candidateProfileModelTemp.CandidateSkill[j].Skill.SkillId, candidateSkills);
                    if (candidateMap.containsKey(candidateSkillsTemp.SkillId)) {
                        candidateMap.get(candidateSkillsTemp.SkillId).add(candidateProfileModelTemp);
                    } else {
                        ArrayList<CandidateProfileModel> jobItemsModels = new ArrayList<>();
                        jobItemsModels.add(candidateProfileModelTemp);
                        candidateMap.put(candidateSkillsTemp.SkillId, jobItemsModels);
                    }
                }
            }

            FreeTimrUtils.log("" + candidateMap);
            initAdapter(candidateMap, candidateSkills.toArray(new SkillModel[candidateSkills.size()]));
        }
    }

    private SkillModel getCategoryNameById(int categoryId, ArrayList<SkillModel> candidateSkills) {
        for (int i = 0; i < candidateSkills.size(); i++) {
            if (candidateSkills.get(i).SkillId == categoryId)
                return candidateSkills.get(i);
        }
        return new SkillModel(99, "Un-Categories");
    }

    public void initAdapter(HashMap<Integer, ArrayList<CandidateProfileModel>> candidateMap, SkillModel[] skillModels) {
        pagerAdapter = new CandidateListPagerAdapter(getSupportFragmentManager(), skillModels, candidateMap);
        jobListViewPager.setAdapter(pagerAdapter);
        tabLayout.setViewPager(jobListViewPager);

        FreeTimrUtils.cancelCurrentDialog(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
