package com.freetimr.business.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import com.freetimr.business.R;
import com.freetimr.business.adapter.HomeRecyclerViewAdapter;
import com.freetimr.business.models.HomeModel;
import com.freetimr.business.utils.Constants;
import com.freetimr.business.utils.SharedPreferenceStore;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    ArrayList<HomeModel> list;
    HomeRecyclerViewAdapter adapter;
    RecyclerView mHomeRecyclerView;
    RelativeLayout mHome, mTermsAndConditions, mPrivacyPolicy, mVersions, mContactUs, mLogout;
    View mNavHeader;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        mNavHeader = navigationView.findViewById(R.id.nav_header);
        mHome = (RelativeLayout) navigationView.findViewById(R.id.menu_home_container);
        mTermsAndConditions = (RelativeLayout) navigationView.findViewById(R.id.menu_tandc_container);
        mPrivacyPolicy = (RelativeLayout) navigationView.findViewById(R.id.menu_privacy_policy_container);
        mVersions = (RelativeLayout) navigationView.findViewById(R.id.menu_version_container);
        mContactUs = (RelativeLayout) navigationView.findViewById(R.id.menu_contact_us_container);
        mLogout = (RelativeLayout) navigationView.findViewById(R.id.menu_logout_container);

        mHomeRecyclerView = (RecyclerView) findViewById(R.id.home_recycler_view);
        list = new ArrayList<>();
        list.add(new HomeModel("1", "JOB \nAPPLICATIONS", R.drawable.job_applications, R.drawable.category_background, Constants.HOME_ITEM_TYPE_JOB_APPLICATIONS));
        list.add(new HomeModel("2", "POST A JOB", R.drawable.post_a_job, R.drawable.category_background, Constants.HOME_ITEM_TYPE_POST_A_JOB));
        list.add(new HomeModel("3", "CANDIDATE \nSEARCH", R.drawable.candidate_search, R.drawable.category_background, Constants.HOME_ITEM_TYPE_CANDIDATE_SEARCH));
        list.add(new HomeModel("4", "MESSAGES", R.drawable.message, R.drawable.category_background, Constants.HOME_ITEM_TYPE_MESSAGE));
        list.add(new HomeModel("5", "MY ACCOUNT", R.drawable.my_account, R.drawable.category_background, Constants.HOME_ITEM_TYPE_MY_ACCOUNT));
        list.add(new HomeModel("6", "SUPPORT", R.drawable.support, R.drawable.category_background, Constants.HOME_ITEM_TYPE_SUPPORT));

        adapter = new HomeRecyclerViewAdapter(HomeActivity.this, list);
        mHomeRecyclerView.setAdapter(adapter);
        mHomeRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));

        mLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                SharedPreferenceStore.deleteValue(HomeActivity.this, Constants.KEY_COMPANY_MODEL);
                SharedPreferenceStore.deleteValue(HomeActivity.this, Constants.IS_FIRST_JOB_POSTED);
                SharedPreferenceStore.deleteValue(HomeActivity.this, Constants.EMPLOYEE_ADMIN_INFO);
                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        mTermsAndConditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                Intent intent = new Intent(HomeActivity.this, TermsAndConditionsActivity.class);
                startActivity(intent);
            }
        });

        mPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                Intent intent = new Intent(HomeActivity.this, PrivacyPolicyActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed () {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected (MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
