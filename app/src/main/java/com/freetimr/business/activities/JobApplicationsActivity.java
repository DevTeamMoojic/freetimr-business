package com.freetimr.business.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.freetimr.business.R;
import com.freetimr.business.adapter.JobApplicationsAdapter;
import com.freetimr.business.models.JobsModel;
import com.freetimr.business.utils.FreeTimrURLs;
import com.freetimr.business.utils.FreeTimrUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobApplicationsActivity extends AppCompatActivity {

    RecyclerView jobApplicationsRecyclerView;
    Toolbar mToolbar;
    JobApplicationsAdapter adapter;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_applications);
        mToolbar = (Toolbar) findViewById(R.id.job_applications_toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        jobApplicationsRecyclerView = (RecyclerView) findViewById(R.id.job_applications_rv);

        getJob();
    }

    private void getJob () {
        FreeTimrUtils.showProgressDialog(this);

        Call<JobsModel[]> call = FreeTimrUtils.getRetrofit().getJobsByCompanyId(FreeTimrURLs.HOST + FreeTimrURLs.GET_JOB_BY_COMPANY + FreeTimrUtils.getCompanyModel(this).CompanyId);

        FreeTimrUtils.log(FreeTimrURLs.HOST + FreeTimrURLs.GET_JOB_BY_COMPANY + FreeTimrUtils.getCompanyModel(this).CompanyId);

        call.enqueue(new Callback<JobsModel[]>() {
            @Override
            public void onResponse (Call<JobsModel[]> call, Response<JobsModel[]> response) {
                FreeTimrUtils.log("" + response.code());
                initAdapter(Arrays.asList(response.body()));
                FreeTimrUtils.cancelCurrentDialog(JobApplicationsActivity.this);
            }

            @Override
            public void onFailure (Call<JobsModel[]> call, Throwable t) {
                FreeTimrUtils.log("" + t);
                FreeTimrUtils.cancelCurrentDialog(JobApplicationsActivity.this);
            }
        });
    }

    private void initAdapter (List<JobsModel> list) {
        adapter = new JobApplicationsAdapter(this, list);
        jobApplicationsRecyclerView.setAdapter(adapter);
        jobApplicationsRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        jobApplicationsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
