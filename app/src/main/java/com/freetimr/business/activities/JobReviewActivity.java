package com.freetimr.business.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freetimr.business.R;
import com.freetimr.business.adapter.CityJobLocationAdapter;
import com.freetimr.business.adapter.DisabilitiesRecyclerViewAdapter;
import com.freetimr.business.adapter.LanguageRecyclerViewAdapter;
import com.freetimr.business.adapter.SkillsRecyclerViewAdapter;
import com.freetimr.business.adapter.SocialCredsRecyclerViewAdapter;
import com.freetimr.business.models.DisabilityModel;
import com.freetimr.business.models.JobLanguagesModel;
import com.freetimr.business.models.JobLocationsModel;
import com.freetimr.business.models.JobSkillsModel;
import com.freetimr.business.models.JobsModel;
import com.freetimr.business.models.PostJobRequestModel;
import com.freetimr.business.models.SocialMediaModel;
import com.freetimr.business.utils.FreeTimrUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class JobReviewActivity extends AppCompatActivity {

    TextView jobTitle, jobDescription, jobEligibility, jobType, jobPayPerDay, jobNumberHours, jobNumberDays, jobStartDate, jobEndDate, jobShiftTime, jobShiftStartTime, jobShiftEndTime, jobStartAge, jobEndAge, jobGender, jobDisabilityAllowed, jobIDProofRequired, jobSocialRequired, jobMinQualification, jobWorkLocation, jobActivity, jobTrainingProvided, jobTrainingPriorDays, jobTrainingDuration, jobTrainingPaid, jobNumberOfBreaks, jobNumberOfPositions, jobBenefitsLabel;
    RecyclerView jobDisabilityRecyclerView, jobSocialCredsRecyclerView, jobPreferredLanguagesRecyclerView, jobSkillsRecyclerView, jobCityLocationRecyclerView;
    LinearLayout jobTrainingDetailsContainer;
    Button continueBtn;
    Toolbar mToolbar;

    PostJobRequestModel postJobRequestModel;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_review);

        mToolbar = (Toolbar) findViewById(R.id.job_review_toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        postJobRequestModel = getIntent().getParcelableExtra("PostJobRequestModel");

        jobTitle = (TextView) findViewById(R.id.job_title_label);
        jobDescription = (TextView) findViewById(R.id.job_details_description_label);
        jobEligibility = (TextView) findViewById(R.id.job_eligibility_label);
        jobBenefitsLabel = (TextView) findViewById(R.id.job_benefits_label);
        jobType = (TextView) findViewById(R.id.job_type_label);
        jobPayPerDay = (TextView) findViewById(R.id.job_pay_per_day_label);
        jobNumberHours = (TextView) findViewById(R.id.job_number_hours);
        jobNumberDays = (TextView) findViewById(R.id.job_number_days);
        jobStartDate = (TextView) findViewById(R.id.job_start_date);
        jobEndDate = (TextView) findViewById(R.id.job_end_date);
        jobShiftTime = (TextView) findViewById(R.id.job_shift_time);
        jobShiftStartTime = (TextView) findViewById(R.id.job_shift_start_time);
        jobShiftEndTime = (TextView) findViewById(R.id.job_shift_end_time);
        jobStartAge = (TextView) findViewById(R.id.job_start_age);
        jobEndAge = (TextView) findViewById(R.id.job_end_age);
        jobGender = (TextView) findViewById(R.id.job_gender);
        jobDisabilityAllowed = (TextView) findViewById(R.id.job_disability_allowed);
        jobIDProofRequired = (TextView) findViewById(R.id.job_id_proof_required);
        jobSocialRequired = (TextView) findViewById(R.id.job_social_required);
        jobMinQualification = (TextView) findViewById(R.id.job_min_qualification);
        jobWorkLocation = (TextView) findViewById(R.id.job_work_location);
        jobActivity = (TextView) findViewById(R.id.job_activity);
        jobTrainingProvided = (TextView) findViewById(R.id.job_training_provided);
        jobTrainingPriorDays = (TextView) findViewById(R.id.job_training_prior_days);
        jobTrainingDuration = (TextView) findViewById(R.id.job_training_duration);
        jobTrainingPaid = (TextView) findViewById(R.id.job_training_paid);
        jobNumberOfBreaks = (TextView) findViewById(R.id.job_number_of_breaks);
        jobNumberOfPositions = (TextView) findViewById(R.id.job_number_of_positions);
        jobDisabilityRecyclerView = (RecyclerView) findViewById(R.id.job_disability_recycler_view);
        jobSocialCredsRecyclerView = (RecyclerView) findViewById(R.id.job_social_creds_recycler_view);
        jobPreferredLanguagesRecyclerView = (RecyclerView) findViewById(R.id.job_preferred_languages_recycler_view);
        jobSkillsRecyclerView = (RecyclerView) findViewById(R.id.job_skills_recycler_view);
        jobCityLocationRecyclerView = (RecyclerView) findViewById(R.id.job_city_location_recycler_view);
        jobTrainingDetailsContainer = (LinearLayout) findViewById(R.id.job_training_details_container);
        continueBtn = (Button) findViewById(R.id.review_btn_continue);

        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                onBackPressed();
            }
        });

        updateFields(postJobRequestModel);
    }

    private void updateFields (PostJobRequestModel model) {

        jobTitle.setText(model.JobTitle);
        jobDescription.setText(model.JobDescription);
        jobEligibility.setText(model.JobEligibility);
        jobBenefitsLabel.setText(model.JobBenefits);
        jobType.setText(FreeTimrUtils.getJobTypeById(this, model.JobTypeId));
        jobPayPerDay.setText("" + model.PayPerDay);
        jobNumberHours.setText("" + model.NoOfHours);
        jobNumberDays.setText("" + model.NoOfJobDays);

        SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyy");
        jobStartDate.setText("" + sdfDate.format(model.StartDate));
        jobEndDate.setText("" + sdfDate.format(model.EndDate));

        SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mm");
        jobShiftTime.setText("" + FreeTimrUtils.getAvailabilityTimeById(this, model.JobAvailabilityTimeId));
        jobShiftStartTime.setText("" + model.StartTime);
        jobShiftEndTime.setText("" + model.EndTime);

        jobStartAge.setText("" + model.AgeGroupFrom);
        jobEndAge.setText("" + model.AgeGroupTo);

        if (model.JobLocations.length != 0) {
            ArrayList<JobLocationsModel> list = new ArrayList<>();

            for (int i = 0; i < model.JobLocations.length; i++) {
                list.add(new JobLocationsModel(model.JobLocations[i].CityId, model.JobLocations[i].LocationName, FreeTimrUtils.getCityNameById(this, model.JobLocations[i].CityId)));
            }

            updateCityJobLocationsRecyclerView(list);
        }

        if (model.Gender.equals("M")) {
            jobGender.setText("Male");
        } else if (model.Gender.equals("F")) {
            jobGender.setText("Female");
        } else if (model.Gender.equals("B")) {
            jobGender.setText("Both");
        }

        if (model.IsDisabledAllowed) {
            jobDisabilityAllowed.setText("Yes");
            jobDisabilityRecyclerView.setVisibility(View.VISIBLE);

            ArrayList<DisabilityModel> list = new ArrayList<>();
            if (model.JobDisability.length != 0) {
                for (int i = 0; i < model.JobDisability.length; i++) {
                    list.add(new DisabilityModel(model.JobDisability[i].DisabilityId, FreeTimrUtils.getDisabilityById(this, model.JobDisability[i].DisabilityId)));
                }
            }

            initDisabilitiesAdapter(list);
        } else {
            jobDisabilityAllowed.setText("No");
            jobDisabilityRecyclerView.setVisibility(View.GONE);
        }
        jobIDProofRequired.setText(model.IsIdProofRequired == true ? "Yes" : "No");

        if (model.IsSocialRequired) {
            jobSocialRequired.setText("Yes");
            jobSocialCredsRecyclerView.setVisibility(View.VISIBLE);

            ArrayList<SocialMediaModel> list = new ArrayList<>();
            if (model.JobSocialMedias.length != 0) {
                for (int i = 0; i < model.JobSocialMedias.length; i++) {
                    list.add(new SocialMediaModel(model.JobSocialMedias[i].SocialMediaId, FreeTimrUtils.getSocialCredsById(this, model.JobSocialMedias[i].SocialMediaId)));
                }
            }

            initSocialCredsAdapter(list);
        } else {
            jobSocialRequired.setText("No");
            jobSocialCredsRecyclerView.setVisibility(View.GONE);
        }

        if (model.JobLanguages.length != 0) {
            ArrayList<JobLanguagesModel> list = new ArrayList<>();
            for (int i = 0; i < model.JobLanguages.length; i++) {
                list.add(new JobLanguagesModel(model.JobLanguages[i].LanguageId));
            }
            updateLanguageArray(list);
        }

        if (model.JobSkills.length != 0) {
            ArrayList<JobSkillsModel> list = new ArrayList<>();
            for (int i = 0; i < model.JobSkills.length; i++) {
                list.add(new JobSkillsModel(model.JobSkills[i].SkillId));
            }

            updateSkillsArray(list);
        }

        jobMinQualification.setText("" + FreeTimrUtils.getGraduationNameById(this, model.GraduationId));
        jobWorkLocation.setText("" + FreeTimrUtils.getWorkLocationById(this, model.WorkLocationId));
        jobActivity.setText("" + FreeTimrUtils.getJobActivityById(this, model.JobActivityId));

        if (model.IsTrainingProvided) {
            jobTrainingProvided.setText("Yes");
            jobTrainingDetailsContainer.setVisibility(View.VISIBLE);
            jobTrainingPriorDays.setText("" + model.TrainingPriorDays);
            jobTrainingDuration.setText("" + model.TrainingForNoOfDays);
            jobTrainingPaid.setText(model.IsPaidForTraining == true ? "Yes" : "No");
        } else {
            jobTrainingProvided.setText("No");
            jobTrainingDetailsContainer.setVisibility(View.GONE);
        }

        jobNumberOfBreaks.setText("" + model.NoOfBreaks);
        jobNumberOfPositions.setText("" + model.NoOfPositions);
    }

    private void updateCityJobLocationsRecyclerView (ArrayList<JobLocationsModel> list) {
        CityJobLocationAdapter cityJobLocationAdapter = new CityJobLocationAdapter(JobReviewActivity.this, list);
        jobCityLocationRecyclerView.setAdapter(cityJobLocationAdapter);
        jobCityLocationRecyclerView.setLayoutManager(new LinearLayoutManager(JobReviewActivity.this));
    }

    private void initDisabilitiesAdapter (ArrayList<DisabilityModel> list) {
        DisabilitiesRecyclerViewAdapter adapter = new DisabilitiesRecyclerViewAdapter(JobReviewActivity.this, list, null);
        jobDisabilityRecyclerView.setAdapter(adapter);
        jobDisabilityRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.HORIZONTAL));
    }

    private void initSocialCredsAdapter (ArrayList<SocialMediaModel> list) {
        SocialCredsRecyclerViewAdapter adapter = new SocialCredsRecyclerViewAdapter(JobReviewActivity.this, list, null);
        jobSocialCredsRecyclerView.setAdapter(adapter);
        jobSocialCredsRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.HORIZONTAL));
    }

    private void updateLanguageArray (ArrayList<JobLanguagesModel> list) {
        LanguageRecyclerViewAdapter languageRecyclerViewAdapter = new LanguageRecyclerViewAdapter(JobReviewActivity.this, list);
        jobPreferredLanguagesRecyclerView.setAdapter(languageRecyclerViewAdapter);
        jobPreferredLanguagesRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.HORIZONTAL));
    }

    private void updateSkillsArray (ArrayList<JobSkillsModel> list) {
        SkillsRecyclerViewAdapter skillsRecyclerViewAdapter = new SkillsRecyclerViewAdapter(JobReviewActivity.this, list);
        jobSkillsRecyclerView.setAdapter(skillsRecyclerViewAdapter);
        jobSkillsRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.HORIZONTAL));
    }
}
