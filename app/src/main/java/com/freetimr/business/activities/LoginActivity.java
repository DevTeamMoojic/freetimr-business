package com.freetimr.business.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.freetimr.business.R;
import com.freetimr.business.models.CompanyModel;
import com.freetimr.business.models.EmployeeModel;
import com.freetimr.business.models.JobsModel;
import com.freetimr.business.utils.Constants;
import com.freetimr.business.utils.FreeTimrURLs;
import com.freetimr.business.utils.FreeTimrUtils;
import com.freetimr.business.utils.SharedPreferenceStore;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    TextView businessSignup;
    EditText emailAddress, password;
    String emailStr, passwordStr;
    Button login;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        businessSignup = (TextView) findViewById(R.id.business_signup);
        emailAddress = (EditText) findViewById(R.id.business_email_address);
        password = (EditText) findViewById(R.id.business_password);
        login = (Button) findViewById(R.id.login_btn);

        businessSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                emailStr = emailAddress.getText().toString();
                passwordStr = password.getText().toString();

                if (emailStr.isEmpty() || passwordStr.isEmpty()) {
                    FreeTimrUtils.showMessage(LoginActivity.this, "All fields are mandatory.");
                } else {
                    LoginModel loginModel = new LoginModel();
                    loginModel.EmailId = emailStr;
                    loginModel.Passcode = passwordStr;

                    loginEmployee(loginModel);
                }
            }
        });
    }

    public static class LoginModel {
        public String EmailId;
        public String Passcode;
    }

    public void loginEmployee (LoginModel loginModel) {
        FreeTimrUtils.showProgressDialog(LoginActivity.this);

        FreeTimrUtils.log(FreeTimrUtils.newGson().toJson(loginModel));

        Call<EmployeeModel> call = FreeTimrUtils.getRetrofit().loginEmployee(loginModel);
        call.enqueue(new Callback<EmployeeModel>() {
            @Override
            public void onResponse (Call<EmployeeModel> call, Response<EmployeeModel> response) {
                if (response.code() == 200) {
                    FreeTimrUtils.saveAdminEmployeeInfo(LoginActivity.this, FreeTimrUtils.newGson().toJson(response.body()));
                    getCompanyModel(response.body().CompanyId);
                } else {
                    FreeTimrUtils.showMessage(LoginActivity.this, "Cannot find user.");
                    FreeTimrUtils.cancelCurrentDialog(LoginActivity.this);
                }
            }

            @Override
            public void onFailure (Call<EmployeeModel> call, Throwable t) {
                FreeTimrUtils.log(t.getLocalizedMessage());
//                FreeTimrUtils.cancelCurrentDialog(LoginActivity.this);
            }
        });
    }

    private void getCompanyModel (String companyId) {
        Call<CompanyModel> call = FreeTimrUtils.getRetrofit().getCompanyModel(FreeTimrURLs.HOST + FreeTimrURLs.GET_COMPANY + companyId);
        call.enqueue(new Callback<CompanyModel>() {
            @Override
            public void onResponse (Call<CompanyModel> call, Response<CompanyModel> response) {
                FreeTimrUtils.log("" + response.code());
                if (response.code() == 200) {
                    SharedPreferenceStore.storeValue(LoginActivity.this, Constants.KEY_COMPANY_MODEL, FreeTimrUtils.newGson().toJson(response.body()));
                    getJob();
                } else {
                    FreeTimrUtils.showMessage(LoginActivity.this, "Something went wrong. Please try again.");
                }
                FreeTimrUtils.cancelCurrentDialog(LoginActivity.this);
            }

            @Override
            public void onFailure (Call<CompanyModel> call, Throwable t) {
                FreeTimrUtils.log("" + t);
                FreeTimrUtils.cancelCurrentDialog(LoginActivity.this);
            }
        });
    }

    private void getJob () {
        Call<JobsModel[]> call = FreeTimrUtils.getRetrofit().getJobsByCompanyId(FreeTimrURLs.HOST + FreeTimrURLs.GET_JOB_BY_COMPANY + FreeTimrUtils.getCompanyModel(this).CompanyId);

        call.enqueue(new Callback<JobsModel[]>() {
            @Override
            public void onResponse (Call<JobsModel[]> call, Response<JobsModel[]> response) {
                FreeTimrUtils.log("" + response.code());
                if (response.body().length == 0) {
                    SharedPreferenceStore.storeValue(LoginActivity.this, Constants.IS_FIRST_JOB_POSTED, false);
                } else {
                    SharedPreferenceStore.storeValue(LoginActivity.this, Constants.IS_FIRST_JOB_POSTED, true);
                }
                switchToNextScreen();
                FreeTimrUtils.cancelCurrentDialog(LoginActivity.this);
            }

            @Override
            public void onFailure (Call<JobsModel[]> call, Throwable t) {
                FreeTimrUtils.log("" + t);
                FreeTimrUtils.cancelCurrentDialog(LoginActivity.this);
            }
        });
    }

    private void switchToNextScreen () {
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }
}
