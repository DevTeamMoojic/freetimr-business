package com.freetimr.business.activities;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.freetimr.business.R;
import com.freetimr.business.fragments.PostAJobStepFourFragments;
import com.freetimr.business.fragments.PostAJobStepOneFragments;
import com.freetimr.business.fragments.PostAJobStepThreeFragments;
import com.freetimr.business.fragments.PostAJobStepTwoFragments;
import com.freetimr.business.models.JobsModel;

public class PostAJobActivity extends AppCompatActivity {

    TextView skip;
    boolean canSkip, isEdit;
    JobsModel model;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_ajob);

        canSkip = getIntent().getBooleanExtra("CanSkip", true);
        isEdit = getIntent().getBooleanExtra("IsEdit", false);
        model = getIntent().getParcelableExtra("JobsModel");

        skip = (TextView) findViewById(R.id.skip_job_posting);

        if (canSkip) {
            skip.setVisibility(View.VISIBLE);
        } else {
            skip.setVisibility(View.GONE);
        }

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                Intent intent = new Intent(PostAJobActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });

        addFragment(PostAJobStepOneFragments.getInstance(isEdit, model));

    }

    public void addFragment (Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.commit();
    }
}
