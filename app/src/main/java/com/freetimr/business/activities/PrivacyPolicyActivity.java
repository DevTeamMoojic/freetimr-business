package com.freetimr.business.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.widget.TextView;

import com.freetimr.business.R;

public class PrivacyPolicyActivity extends AppCompatActivity {

    TextView privacyPolicyLabel;
    Toolbar mToolbar;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);

        mToolbar = (Toolbar) findViewById(R.id.privacy_policy_toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        privacyPolicyLabel = (TextView) findViewById(R.id.privacy_policy_label);

        privacyPolicyLabel.setText(Html.fromHtml("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur et velit quis massa ultricies gravida et non neque. Phasellus lorem magna, tincidunt vitae porta et, ultrices a lacus. Praesent nec molestie ex, ac malesuada erat. Duis bibendum in ante vel auctor. Ut varius mauris leo, et auctor urna sodales ut. Nullam eu nunc tempor, ornare quam sit amet, varius felis. Duis tincidunt, dolor quis lobortis pharetra, quam nisi laoreet odio, eget rutrum dui tellus a ante. Donec quis sem vel ex interdum efficitur. Duis non enim sapien. Phasellus ipsum diam, porta a nisl finibus, commodo luctus diam. Fusce eget venenatis leo. Integer scelerisque ullamcorper ipsum, nec lobortis enim tempus sit amet. Etiam viverra eleifend tortor sed tempor. Fusce luctus ante tellus, id sodales enim luctus sit amet.\n" +
                "\n" +
                "Donec porta quis orci in accumsan. Nulla vitae urna commodo, placerat tortor vel, accumsan elit. Morbi fermentum massa id nibh laoreet rutrum. Vestibulum orci augue, facilisis eget posuere sed, condimentum at nisl. Nam sagittis vel magna nec aliquam. Mauris vestibulum ut ante eu mattis. Nam gravida eleifend metus. Aliquam erat volutpat. Curabitur sodales sollicitudin bibendum. Duis eu tortor varius, elementum neque interdum, aliquet dui. Vestibulum eget lacus lobortis, varius erat et, ornare lorem. Quisque vestibulum mi a ligula tempor, eu ultricies tellus vestibulum.\n" +
                "\n" +
                "Aliquam erat volutpat. Aenean non aliquet diam. Aenean vel massa nibh. Aliquam vitae turpis est. Donec lacinia lacus eget dui gravida, et mollis lorem luctus. Nam ut sem ac lacus rhoncus elementum. In convallis nisi a egestas commodo. Nam semper consequat sodales. Phasellus id sapien eu velit interdum rutrum efficitur vel orci. Etiam suscipit malesuada purus, in ornare erat condimentum eu. Cras id porttitor neque. Suspendisse semper est non lacinia semper."));
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {

        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
