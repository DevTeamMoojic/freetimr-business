package com.freetimr.business.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.freetimr.business.R;
import com.freetimr.business.adapter.CityAutoCompleteAdapter;
import com.freetimr.business.fragments.PostAJobStepOneFragments;
import com.freetimr.business.models.CityModel;
import com.freetimr.business.models.CompanyModel;
import com.freetimr.business.models.EmployeeModel;
import com.freetimr.business.utils.Constants;
import com.freetimr.business.utils.FreeTimrUtils;
import com.freetimr.business.utils.SharedPreferenceStore;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    EditText companyName, companyDescription, emailAddress, phoneNumber, officialWebsite, employerName, employerPassword;
    AutoCompleteTextView headOfficeLocation;
    Button registerButton;
    String companyNameStr, companyDescriptionStr, emailAddressStr, phoneNumberStr, officialWebsiteStr, headOfficeLocationStr, employerNameStr, employerPasswordStr;
    int cityIdValue;
    ArrayList<CityModel> cityList;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        companyName = (EditText) findViewById(R.id.company_name_edit);
        companyDescription = (EditText) findViewById(R.id.company_description_edit);
        emailAddress = (EditText) findViewById(R.id.email_edit);
        phoneNumber = (EditText) findViewById(R.id.phone_number_edit);
        officialWebsite = (EditText) findViewById(R.id.official_website_edit);
        headOfficeLocation = (AutoCompleteTextView) findViewById(R.id.head_office_location_edit);
        employerName = (EditText) findViewById(R.id.employer_name_edit);
        employerPassword = (EditText) findViewById(R.id.employer_password_edit);
        registerButton = (Button) findViewById(R.id.register_btn);

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-SemiBold.otf");
        headOfficeLocation.setTypeface(tf);

        cityList = FreeTimrUtils.getCityList(RegisterActivity.this);
        CityAutoCompleteAdapter cityAutoCompleteAdapter = new CityAutoCompleteAdapter(RegisterActivity.this, cityList);
        headOfficeLocation.setAdapter(cityAutoCompleteAdapter);

        headOfficeLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick (AdapterView<?> parent, View view, int position, long id) {
                CityModel cityModel = (CityModel) parent.getAdapter().getItem(position);
                cityIdValue = cityModel.CityId;
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                companyNameStr = companyName.getText().toString();
                companyDescriptionStr = companyDescription.getText().toString();
                emailAddressStr = emailAddress.getText().toString();
                phoneNumberStr = phoneNumber.getText().toString();
                officialWebsiteStr = officialWebsite.getText().toString();
                headOfficeLocationStr = headOfficeLocation.getText().toString();
                employerNameStr = employerName.getText().toString();
                employerPasswordStr = employerPassword.getText().toString();

                if (companyNameStr.isEmpty() || companyDescriptionStr.isEmpty() || headOfficeLocationStr.isEmpty() || phoneNumberStr.isEmpty() || emailAddressStr.isEmpty() || employerNameStr.isEmpty() || employerPasswordStr.isEmpty()) {
                    FreeTimrUtils.showMessage(RegisterActivity.this, "Please fill all the fields.");
                    return;
                }

                if (phoneNumberStr.length() < 10) {
                    FreeTimrUtils.showMessage(RegisterActivity.this, "Please enter a valid Phone Number");
                    return;
                }

                if (employerPasswordStr.length() < 8) {
                    FreeTimrUtils.showMessage(RegisterActivity.this, "Please enter a valid Password");
                    return;
                }

                if (!FreeTimrUtils.isValidEmailId(emailAddressStr)) {
                    FreeTimrUtils.showMessage(RegisterActivity.this, "Please enter a valid email address");
                    return;
                }

                registerCompany();
            }
        });
    }

    private void registerCompany () {

        FreeTimrUtils.showProgressDialog(this);

        EmployeeModel employeeModel = new EmployeeModel();
        employeeModel.EmailId = emailAddressStr;
        employeeModel.MobileNumber = phoneNumberStr;
        employeeModel.EmployeeName = employerNameStr;
        employeeModel.Passcode = employerPasswordStr;
        employeeModel.IsMasterAdmin = true;

        final CompanyModel companyModel = new CompanyModel();
        companyModel.CompanyName = companyNameStr;
        companyModel.CompanyDescription = companyDescriptionStr;
        companyModel.CompanyDescription = companyDescriptionStr;
        companyModel.Website = officialWebsiteStr;
        companyModel.Employee = new EmployeeModel[]{employeeModel};
        companyModel.CityId = cityIdValue;

        FreeTimrUtils.log(FreeTimrUtils.newGson().toJson(companyModel));

        Call<String> registerCompanyCall = FreeTimrUtils.getRetrofit().registerCompany(companyModel);
        registerCompanyCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse (Call<String> call, Response<String> response) {
                FreeTimrUtils.log("" + response.code());
                if (response.code() == 201) {
                    companyModel.CompanyId = response.body();
                    SharedPreferenceStore.storeValue(RegisterActivity.this, Constants.KEY_COMPANY_MODEL, FreeTimrUtils.newGson().toJson(companyModel));

                    Intent intent = new Intent(RegisterActivity.this, PostAJobActivity.class);
                    startActivity(intent);
                    finish();

                    registerWithSendBird(response.body());
                } else {
                    FreeTimrUtils.showMessage(RegisterActivity.this, "Please try again");
                }

                FreeTimrUtils.cancelCurrentDialog(RegisterActivity.this);
            }

            @Override
            public void onFailure (Call<String> call, Throwable t) {
                FreeTimrUtils.log("" + t);
                FreeTimrUtils.cancelCurrentDialog(RegisterActivity.this);
            }
        });
    }

    private void registerWithSendBird (String userID) {
        SendBird.connect(userID, new SendBird.ConnectHandler() {
            @Override
            public void onConnected (User user, SendBirdException e) {
                if (e != null) {
                    // Error.
                    return;
                } else {
                    FreeTimrUtils.log("" + user.getProfileUrl());
                }
            }
        });
    }
}
