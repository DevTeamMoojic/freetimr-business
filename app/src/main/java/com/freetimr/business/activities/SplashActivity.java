package com.freetimr.business.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.freetimr.business.R;
import com.freetimr.business.models.EmployeeModel;
import com.freetimr.business.models.JobMastersModel;
import com.freetimr.business.utils.Constants;
import com.freetimr.business.utils.FreeTimrURLs;
import com.freetimr.business.utils.FreeTimrUtils;
import com.freetimr.business.utils.SharedPreferenceStore;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run () {
                getJobMaster();
            }
        }, 1000);
    }

    public void getJobMaster () {

//        FreeTimrUtils.showProgressDialog(this);

        Call<JobMastersModel> call = FreeTimrUtils.getRetrofit().getJobMaster(FreeTimrURLs.HOST + FreeTimrURLs.GET_JOB_MASTER);
        call.enqueue(new Callback<JobMastersModel>() {
            @Override
            public void onResponse (Call<JobMastersModel> call, Response<JobMastersModel> response) {
                FreeTimrUtils.log("" + response.code());
                SharedPreferenceStore.storeValue(SplashActivity.this, Constants.KEY_JOB_MASTER, FreeTimrUtils.newGson().toJson(response.body()));
                switchToNextPage();
//                FreeTimrUtils.cancelCurrentDialog(SplashActivity.this);
            }

            @Override
            public void onFailure (Call<JobMastersModel> call, Throwable t) {
                FreeTimrUtils.log("" + t);
//                FreeTimrUtils.cancelCurrentDialog(SplashActivity.this);
            }
        });
    }

    public void switchToNextPage () {
        String companyModel = SharedPreferenceStore.getValue(SplashActivity.this, Constants.KEY_COMPANY_MODEL, "");
        if (companyModel.isEmpty()) {
            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        } else {
//            EmployeeModel employeeModel = FreeTimrUtils.getAdminEmployeeInfo(SplashActivity.this);
//            if (employeeModel != null) {
//                Intent intent = new Intent(SplashActivity.this, PostAJobActivity.class);
//                startActivity(intent);
//                finish();
//            } else {
            boolean isFirstJobPosted = SharedPreferenceStore.getValue(SplashActivity.this, Constants.IS_FIRST_JOB_POSTED, false);
            if (isFirstJobPosted) {
                Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(SplashActivity.this, PostAJobActivity.class);
                startActivity(intent);
                finish();
            }
//            }

        }
    }
}
