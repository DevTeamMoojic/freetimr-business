package com.freetimr.business.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freetimr.business.R;
import com.freetimr.business.activities.ApplicantDetailActivity;
import com.freetimr.business.models.JobApplicationModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Admin on 25-Mar-17.
 */

public class ApplicantListAdapter extends RecyclerView.Adapter<ApplicantListAdapter.ApplicantViewHolder> {

    Context context;
    LayoutInflater inflater;
    ArrayList<JobApplicationModel> list;

    public ApplicantListAdapter (Context context, ArrayList<JobApplicationModel> list) {
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ApplicantViewHolder onCreateViewHolder (ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.applicant_list_item, parent, false);
        return new ApplicantViewHolder(view);
    }

    @Override
    public void onBindViewHolder (ApplicantViewHolder holder, int position) {

        final JobApplicationModel model = list.get(position);

        holder.initial.setText(model.Candidate.CandidateName.substring(0, 1));
        holder.name.setText(model.Candidate.CandidateName);
        holder.appliedOn.setText(new SimpleDateFormat("dd-MM-yyyy").format(model.StatusDate));
        if (model.Candidate.Gender.equals("M")) {
            holder.gender.setText("Male");
        } else if (model.Candidate.Gender.equals("F")) {
            holder.gender.setText("Female");
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                Intent intent = new Intent(context, ApplicantDetailActivity.class);
                intent.putExtra("CandidateId", model.CandidateId);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount () {
        return list.size();
    }

    class ApplicantViewHolder extends RecyclerView.ViewHolder {

        TextView initial, name, gender, appliedOn;

        public ApplicantViewHolder (View itemView) {
            super(itemView);
            initial = (TextView) itemView.findViewById(R.id.applicant_name_initial);
            name = (TextView) itemView.findViewById(R.id.applicant_name);
            gender = (TextView) itemView.findViewById(R.id.applicant_gender);
            appliedOn = (TextView) itemView.findViewById(R.id.applicant_applied_on);
        }
    }

}
