package com.freetimr.business.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freetimr.business.R;
import com.freetimr.business.fragments.MyBottomSheetDialogFragment;
import com.freetimr.business.utils.BottomSheetListOnItemClickListener;

/**
 * Created by Admin on 21-Mar-17.
 */

public class BottomSheetListAdapter extends RecyclerView.Adapter<BottomSheetListAdapter.BottomSheetViewHolder> {

    String[] strings;
    Context context;
    LayoutInflater inflater;
    BottomSheetListOnItemClickListener bottomSheetListOnItemClickListener;
    MyBottomSheetDialogFragment myBottomSheetDialogFragment;

    public BottomSheetListAdapter(Context context, String[] strings, BottomSheetListOnItemClickListener bottomSheetListOnItemClickListener, MyBottomSheetDialogFragment myBottomSheetDialogFragment) {
        this.strings = strings;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.bottomSheetListOnItemClickListener = bottomSheetListOnItemClickListener;
        this.myBottomSheetDialogFragment = myBottomSheetDialogFragment;
    }

    @Override
    public BottomSheetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.bottom_sheet_list_item, parent, false);
        return new BottomSheetViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BottomSheetViewHolder holder, final int position) {
        holder.textView.setText(strings[position]);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetListOnItemClickListener.onListItemSelected(position);
                myBottomSheetDialogFragment.dismiss();
            }
        });
    }

    @Override
    public int getItemCount() {
        return strings.length;
    }

    class BottomSheetViewHolder extends RecyclerView.ViewHolder {

        TextView textView;

        public BottomSheetViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.bottom_sheet_list_label);
        }
    }

}

