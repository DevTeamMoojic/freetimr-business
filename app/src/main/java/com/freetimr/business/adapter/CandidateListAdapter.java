package com.freetimr.business.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freetimr.business.R;
import com.freetimr.business.activities.ApplicantDetailActivity;
import com.freetimr.business.models.CandidateProfileModel;

import java.util.ArrayList;

/**
 * Created by Admin on 07-Apr-17.
 */

public class CandidateListAdapter extends RecyclerView.Adapter<CandidateListAdapter.CandidateViewHolder> {

    ArrayList<CandidateProfileModel> list;
    Context context;
    LayoutInflater inflater;

    public CandidateListAdapter (Context context, ArrayList<CandidateProfileModel> list) {
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public CandidateViewHolder onCreateViewHolder (ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.candidate_list_item, parent, false);
        return new CandidateViewHolder(view);
    }

    @Override
    public void onBindViewHolder (CandidateViewHolder holder, int position) {
        final CandidateProfileModel model = list.get(position);
        holder.initials.setText(model.Candidate.CandidateName.substring(0, 1));
        holder.name.setText(model.Candidate.CandidateName);
        if (model.Candidate.Gender.equals("M")) {
            holder.gender.setText("Male");
        } else if (model.Candidate.Gender.equals("F")) {
            holder.gender.setText("Female");
        }

        if (model.CandidateExperience != null || model.CandidateExperience.length != 0) {
            for (int i = 0; i < model.CandidateExperience.length; i++) {
                ;
                if (model.CandidateExperience[i].IsCurrentEmployer)
                    holder.currentLocation.setText(model.CandidateExperience[i].EmployerName);
            }
        } else {
            holder.currentLocation.setVisibility(View.GONE);
        }

        holder.currentLocation.setText(model.Location);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                Intent intent = new Intent(context, ApplicantDetailActivity.class);
                intent.putExtra("CandidateId", model.CandidateId);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount () {
        return list.size();
    }

    class CandidateViewHolder extends RecyclerView.ViewHolder {

        TextView name, gender, age, currentCompany, currentLocation, initials, updateDate;

        public CandidateViewHolder (View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.candidate_name);
            gender = (TextView) itemView.findViewById(R.id.candidate_gender);
            age = (TextView) itemView.findViewById(R.id.candidate_age);
            currentCompany = (TextView) itemView.findViewById(R.id.candidate_current_company);
            currentLocation = (TextView) itemView.findViewById(R.id.candidate_current_locations);
            initials = (TextView) itemView.findViewById(R.id.candidate_name_initial);
            updateDate = (TextView) itemView.findViewById(R.id.profile_updated_date);
        }
    }
}
