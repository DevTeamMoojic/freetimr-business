package com.freetimr.business.adapter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.freetimr.business.R;
import com.freetimr.business.models.CandidateProfileModel;
import com.freetimr.business.utils.FreeTimrUtils;

import java.util.ArrayList;

/**
 * Created by Admin on 07-Apr-17.
 */

public class CandidateListFragment extends Fragment {

    RecyclerView mJobListRecyclerView;
    ArrayList<CandidateProfileModel> list;
    CandidateListAdapter adapter;

    public static CandidateListFragment getInstance(ArrayList<CandidateProfileModel> list) {
        CandidateListFragment fragment = new CandidateListFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("CandidateList", list);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        list = getArguments().getParcelableArrayList("CandidateList");

        FreeTimrUtils.log("" + list.size());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.candidate_list_fragment, container, false);

        mJobListRecyclerView = (RecyclerView) rootView.findViewById(R.id.candidate_list_recycler_view);

        adapter = new CandidateListAdapter(this.getActivity(), list);
        mJobListRecyclerView.setAdapter(adapter);
        mJobListRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        return rootView;
    }
}
