package com.freetimr.business.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.freetimr.business.models.CandidateProfileModel;
import com.freetimr.business.models.SkillModel;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 07-Apr-17.
 */

public class CandidateListPagerAdapter extends FragmentStatePagerAdapter {

    ArrayList<SkillModel> skillModelList;
    SkillModel[] categoryModels;
    HashMap<Integer, ArrayList<CandidateProfileModel>> jobMap;

    public CandidateListPagerAdapter(FragmentManager fm, SkillModel[] categoryModels, HashMap<Integer, ArrayList<CandidateProfileModel>> jobMap) {
        super(fm);
        this.categoryModels = categoryModels;
        this.jobMap = jobMap;
        skillModelList = new ArrayList<>();
        initAllData(categoryModels);
    }

    private void initAllData(SkillModel[] categoryModels) {
        for (SkillModel jobCategoryModel : categoryModels)
            if (jobMap.containsKey(jobCategoryModel.SkillId))
                skillModelList.add(jobCategoryModel);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return skillModelList.get(position).SkillName;
    }

    @Override
    public Fragment getItem(int position) {
        return CandidateListFragment.getInstance(jobMap.get(skillModelList.get(position).SkillId));
    }

    @Override
    public int getCount() {
        return skillModelList.size();
    }
}


