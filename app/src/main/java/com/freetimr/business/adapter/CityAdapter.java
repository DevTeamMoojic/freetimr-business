package com.freetimr.business.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.freetimr.business.R;
import com.freetimr.business.models.CandidatePreferredCityModel;
import com.freetimr.business.models.CityModel;
import com.freetimr.business.views.CustomTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by varunbarve on 19/09/17.
 */

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.CityViewHolder> {

    Context context;
    LayoutInflater inflater;
    ArrayList<CandidatePreferredCityModel> list;

    public CityAdapter (Context context, ArrayList<CandidatePreferredCityModel> list) {
        this.list = list;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public CityViewHolder onCreateViewHolder (ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.city_list_item, parent, false);
        return new CityViewHolder(view);
    }

    @Override
    public void onBindViewHolder (CityViewHolder holder, final int position) {

        final CandidatePreferredCityModel cityModel = list.get(position);

        holder.name.setText(cityModel.City.CityName);
        Picasso.with(context).load("http://137.59.54.53/FreeTimr/Images/City/" + cityModel.City.CityImage).into(holder.cityImage);
        holder.itemView.setTag(position);

//        holder.cityImage.setColorFilter(Color.parseColor("#AAffffff"));
        holder.name.setTextColor(Color.parseColor("#97a1aa"));

    }

    @Override
    public int getItemCount () {
        return list.size();
    }

    public class CityViewHolder extends RecyclerView.ViewHolder {

        CustomTextView name;
        ImageView cityImage;

        public CityViewHolder (View itemView) {
            super(itemView);
            name = (CustomTextView) itemView.findViewById(R.id.city_name);
            cityImage = (ImageView) itemView.findViewById(R.id.city_image);
        }
    }
}
