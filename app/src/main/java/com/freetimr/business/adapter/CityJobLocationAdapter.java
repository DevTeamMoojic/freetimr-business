package com.freetimr.business.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freetimr.business.R;
import com.freetimr.business.models.JobLocationsModel;

import java.util.ArrayList;

/**
 * Created by Admin on 22-Mar-17.
 */

public class CityJobLocationAdapter extends RecyclerView.Adapter<CityJobLocationAdapter.CityJobLocationsViewHolder> {

    Context context;
    LayoutInflater inflater;
    ArrayList<JobLocationsModel> list;

    public CityJobLocationAdapter(Context context, ArrayList<JobLocationsModel> list) {
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public CityJobLocationsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.additional_locations_fields, parent, false);
        return new CityJobLocationsViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onBindViewHolder(CityJobLocationsViewHolder holder, int position) {
        holder.cityName.setText("" + list.get(position).CityName);
        holder.jobLocation.setText(list.get(position).LocationName);
    }

    class CityJobLocationsViewHolder extends RecyclerView.ViewHolder {

        TextView cityName, jobLocation;

        public CityJobLocationsViewHolder(View itemView) {
            super(itemView);
            cityName = (TextView) itemView.findViewById(R.id.job_location_city);
            jobLocation = (TextView) itemView.findViewById(R.id.job_absolute_locations_edit);
        }
    }

}
