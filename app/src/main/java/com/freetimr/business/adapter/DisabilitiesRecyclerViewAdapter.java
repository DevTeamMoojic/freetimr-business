package com.freetimr.business.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.freetimr.business.R;
import com.freetimr.business.fragments.PostAJobStepTwoFragments;
import com.freetimr.business.models.DisabilityModel;

import java.util.ArrayList;

/**
 * Created by varunbarve on 23/3/17.
 */

public class DisabilitiesRecyclerViewAdapter extends RecyclerView.Adapter<DisabilitiesRecyclerViewAdapter.DisabilitiesViewHolder> {

    Context context;
    LayoutInflater inflater;
    ArrayList<DisabilityModel> list;
    PostAJobStepTwoFragments fragment;

    public DisabilitiesRecyclerViewAdapter (Context context, ArrayList<DisabilityModel> list, PostAJobStepTwoFragments fragment) {
        this.context = context;
        this.list = list;
        this.fragment = fragment;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public DisabilitiesViewHolder onCreateViewHolder (ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.disabilities_list_item, parent, false);
        return new DisabilitiesViewHolder(view);
    }

    @Override
    public void onBindViewHolder (DisabilitiesViewHolder holder, final int position) {
        holder.name.setText(list.get(position).DisabilityName);
        if (fragment == null) {
            holder.checkBox.setVisibility(View.GONE);
        } else {
            holder.checkBox.setVisibility(View.VISIBLE);
        }
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged (CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    fragment.addDisability(list.get(position).DisabilityId);
                else
                    fragment.removeDisability(position);
            }
        });
    }

    @Override
    public int getItemCount () {
        return list.size();
    }

    class DisabilitiesViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        CheckBox checkBox;

        public DisabilitiesViewHolder (View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.disability_name);
            checkBox = (CheckBox) itemView.findViewById(R.id.disability_checked);
        }
    }

}
