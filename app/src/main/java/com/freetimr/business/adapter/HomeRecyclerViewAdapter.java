package com.freetimr.business.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.freetimr.business.R;
import com.freetimr.business.activities.CandidateListActivity;
import com.freetimr.business.activities.CompanyProfileActivity;
import com.freetimr.business.activities.JobApplicationsActivity;
import com.freetimr.business.activities.MessagesActivity;
import com.freetimr.business.activities.PostAJobActivity;
import com.freetimr.business.activities.SplashActivity;
import com.freetimr.business.models.HomeModel;
import com.freetimr.business.utils.Constants;
import com.freetimr.business.utils.FreeTimrUtils;
import com.freetimr.business.utils.SharedPreferenceStore;

import java.util.ArrayList;

/**
 * Created by Admin on 23-Mar-17.
 */

public class HomeRecyclerViewAdapter extends RecyclerView.Adapter<HomeRecyclerViewAdapter.HomeViewHolder> {

    Context context;
    ArrayList<HomeModel> list;
    LayoutInflater inflater;

    public HomeRecyclerViewAdapter (Context context, ArrayList<HomeModel> list) {
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public HomeViewHolder onCreateViewHolder (ViewGroup parent, int viewType) {
        View view;
        view = inflater.inflate(R.layout.home_list_item, parent, false);
        return new HomeViewHolder(view);
    }

    @Override
    public void onBindViewHolder (HomeViewHolder holder, final int position) {
        holder.name.setText(list.get(position).getName());
        holder.background.setImageResource(list.get(position).getBackground());
        holder.logo.setImageResource(list.get(position).getLogo());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                Intent intent;
                switch (list.get(position).getItemType()) {
                    case Constants.HOME_ITEM_TYPE_JOB_APPLICATIONS:
                        boolean isFirstJobPosted = SharedPreferenceStore.getValue(context, Constants.IS_FIRST_JOB_POSTED, false);
                        if (isFirstJobPosted) {
                            intent = new Intent(context, JobApplicationsActivity.class);
                            context.startActivity(intent);
                        } else {
//                            FreeTimrUtils.showMessage(context, "Please post a job first.");
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final View dialogView = inflater.inflate(R.layout.post_a_new_job_dialog, null);
                            dialogBuilder.setView(dialogView);

                            dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick (DialogInterface dialog, int whichButton) {
                                    Intent postJobIntent = new Intent(context, PostAJobActivity.class);
                                    postJobIntent.putExtra("CanSkip", false);
                                    context.startActivity(postJobIntent);
                                }
                            });
                            dialogBuilder.setNegativeButton("Later", new DialogInterface.OnClickListener() {
                                public void onClick (DialogInterface dialog, int whichButton) {
                                    //pass
                                }
                            });
                            AlertDialog b = dialogBuilder.create();
                            b.show();
                        }

                        break;
                    case Constants.HOME_ITEM_TYPE_POST_A_JOB:
                        intent = new Intent(context, PostAJobActivity.class);
                        intent.putExtra("CanSkip", false);
                        context.startActivity(intent);
                        break;
                    case Constants.HOME_ITEM_TYPE_CANDIDATE_SEARCH:
                        intent = new Intent(context, CandidateListActivity.class);
                        context.startActivity(intent);
                        break;
                    case Constants.HOME_ITEM_TYPE_MESSAGE:
                        intent = new Intent(context, MessagesActivity.class);
                        context.startActivity(intent);
                        break;
                    case Constants.HOME_ITEM_TYPE_MY_ACCOUNT:
                        intent = new Intent(context, CompanyProfileActivity.class);
                        intent.putExtra("CompanyId", FreeTimrUtils.getCompanyModel(context).CompanyId);
                        context.startActivity(intent);
                        break;
                    case Constants.HOME_ITEM_TYPE_SUPPORT:
                        break;
                }

            }
        });
    }

    @Override
    public int getItemCount () {
        return list.size();
    }

    public class HomeViewHolder extends RecyclerView.ViewHolder {

        ImageView background, logo;
        TextView name;

        public HomeViewHolder (View itemView) {
            super(itemView);

            background = (ImageView) itemView.findViewById(R.id.background);
            logo = (ImageView) itemView.findViewById(R.id.logo);
            name = (TextView) itemView.findViewById(R.id.name);
        }
    }

}

