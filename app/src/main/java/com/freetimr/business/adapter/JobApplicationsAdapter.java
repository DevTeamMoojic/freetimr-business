package com.freetimr.business.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freetimr.business.R;
import com.freetimr.business.activities.JobDetailsActivity;
import com.freetimr.business.models.JobsModel;

import java.util.List;

/**
 * Created by Admin on 23-Mar-17.
 */

public class JobApplicationsAdapter extends RecyclerView.Adapter<JobApplicationsAdapter.JobApplicationsViewHolder> {

    Context context;
    LayoutInflater inflater;
    List<JobsModel> list;

    public JobApplicationsAdapter (Context context, List<JobsModel> list) {
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public JobApplicationsViewHolder onCreateViewHolder (ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.job_list_item, parent, false);
        return new JobApplicationsViewHolder(view);
    }

    @Override
    public void onBindViewHolder (JobApplicationsViewHolder holder, int position) {
        JobsModel model = list.get(position);
        holder.jobTitle.setText(model.JobTitle);
        holder.jobTitle.setText(model.JobTitle);

        // LOCATIONS
        if (model.JobLocations.length == 0) {
            holder.locationContainer.setVisibility(View.GONE);
        } else {
            holder.locationContainer.setVisibility(View.VISIBLE);
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < model.JobLocations.length; i++) {
                stringBuilder.append(model.JobLocations[i].LocationName + ",");
            }
            String jobLocations = stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1);

            holder.location.setText(jobLocations);
        }

        // PAY
        if (model.PayPerDay == 0) {
            holder.salaryContainer.setVisibility(View.GONE);
        } else {
            holder.salary.setText(model.PayPerDay + " (per day)");
        }

        holder.applied.setText("" + model.JobApplication.length);
        holder.itemView.setTag(model.JobId);
    }

    @Override
    public int getItemCount () {
        return list.size();
    }

    class JobApplicationsViewHolder extends RecyclerView.ViewHolder {

        TextView jobTitle, location, salary, applied;
        LinearLayout locationContainer, salaryContainer;

        public JobApplicationsViewHolder (final View itemView) {
            super(itemView);
            jobTitle = (TextView) itemView.findViewById(R.id.job_application_job_title);
            location = (TextView) itemView.findViewById(R.id.job_application_locations);
            locationContainer = (LinearLayout) itemView.findViewById(R.id.job_application_locations_container);
            salary = (TextView) itemView.findViewById(R.id.job_application_salary);
            salaryContainer = (LinearLayout) itemView.findViewById(R.id.job_application_salary_container);
            applied = (TextView) itemView.findViewById(R.id.job_application_number_applied);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick (View view) {
                    Intent intent = new Intent(context, JobDetailsActivity.class);
//                    intent.putExtra("JobsModel", (Parcelable) itemView.getTag());
                    intent.putExtra("JobId", (String) itemView.getTag());
                    context.startActivity(intent);
                }
            });
        }
    }

}
