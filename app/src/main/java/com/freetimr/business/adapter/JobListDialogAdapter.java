package com.freetimr.business.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.freetimr.business.R;
import com.freetimr.business.models.JobsModel;

/**
 * Created by varunbarve on 25/09/17.
 */

public class JobListDialogAdapter extends RecyclerView.Adapter<JobListDialogAdapter.JobListDialogViewHolder> {

    Context context;
    JobsModel[] models;
    LayoutInflater inflater;
    String jobId;
    int pos = -1;
    JobsModel model;
    private boolean onBind;

    public JobListDialogAdapter (Context context, JobsModel[] models) {
        this.context = context;
        this.models = models;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public JobListDialogViewHolder onCreateViewHolder (ViewGroup parent, int viewType) {
        View rootView = inflater.inflate(R.layout.job_list_dialog_item, parent, false);
        return new JobListDialogViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder (final JobListDialogViewHolder holder, final int position) {
        model = models[position];
        holder.checkBox.setText(model.JobTitle);
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Light.otf");
        holder.checkBox.setTypeface(font);

        onBind = true;
        if (pos == position) {
            holder.checkBox.setChecked(true);
        } else {
            holder.checkBox.setChecked(false);
        }
        onBind = false;

        holder.checkBox.setTag(position);
    }

    public void setJobId (String jobId) {
        this.jobId = jobId;
    }

    public String getJobId () {
        return jobId;
    }

    @Override
    public int getItemCount () {
        return models.length;
    }

    class JobListDialogViewHolder extends RecyclerView.ViewHolder {

        CheckBox checkBox;

        public JobListDialogViewHolder (View itemView) {
            super(itemView);
            checkBox = (CheckBox) itemView.findViewById(R.id.job_checkbox);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged (CompoundButton buttonView, boolean isChecked) {
                    if (!onBind) {
                        pos = (int) checkBox.getTag();
                        setJobId(model.JobId);
                        notifyDataSetChanged();
                    }
                }
            });
        }
    }
}
