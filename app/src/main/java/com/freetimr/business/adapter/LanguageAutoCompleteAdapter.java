package com.freetimr.business.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.freetimr.business.R;
import com.freetimr.business.models.CityModel;
import com.freetimr.business.models.LanguageModel;

import java.util.ArrayList;

/**
 * Created by varunbarve on 23/3/17.
 */

public class LanguageAutoCompleteAdapter extends ArrayAdapter<LanguageModel> implements Filterable {

    Context context;
    LayoutInflater inflater;
    ArrayList<LanguageModel> list;
    ArrayList<LanguageModel> listAll;
    ArrayList<LanguageModel> suggestions;

    public LanguageAutoCompleteAdapter(@NonNull Context context, @NonNull ArrayList<LanguageModel> list) {
        super(context, R.layout.city_autocomplete_list_item, list);
        this.context = context;
        this.list = list;
        this.listAll = (ArrayList<LanguageModel>) list.clone();
        this.suggestions = new ArrayList<>();
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LanguageViewHolder holder;
        if (convertView == null) {
            holder = new LanguageViewHolder();

            convertView = inflater.inflate(R.layout.city_autocomplete_list_item, parent, false);
            holder.name = (TextView) convertView.findViewById(R.id.city_name);

            convertView.setTag(holder);
        } else {
            holder = (LanguageViewHolder) convertView.getTag();
        }

        holder.name.setText(list.get(position).LanguageName);

        return convertView;
    }

    class LanguageViewHolder {
        TextView name;
    }

    @Nullable
    @Override
    public LanguageModel getItem(int position) {
        return list.get(position);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return new LanguageFilter();
    }

    class LanguageFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (LanguageModel language : listAll) {
                    if (language.LanguageName.toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                        suggestions.add(language);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<LanguageModel> filteredList = (ArrayList<LanguageModel>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (LanguageModel c : filteredList) {
                    add(c);
                }
                notifyDataSetChanged();
            }
        }
    }
}
