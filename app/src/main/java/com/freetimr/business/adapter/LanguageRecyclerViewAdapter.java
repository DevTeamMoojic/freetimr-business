package com.freetimr.business.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freetimr.business.R;
import com.freetimr.business.models.JobLanguagesModel;
import com.freetimr.business.models.LanguageModel;
import com.freetimr.business.utils.FreeTimrUtils;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by varunbarve on 23/3/17.
 */

public class LanguageRecyclerViewAdapter extends RecyclerView.Adapter<LanguageRecyclerViewAdapter.LanguageViewHolder> {

    Context context;
    LayoutInflater inflater;
    ArrayList<JobLanguagesModel> list;

    public LanguageRecyclerViewAdapter (Context context, ArrayList<JobLanguagesModel> list) {
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public LanguageViewHolder onCreateViewHolder (ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.language_item, parent, false);
        return new LanguageViewHolder(view);
    }

    @Override
    public void onBindViewHolder (LanguageViewHolder holder, int position) {
        holder.name.setText("#" + FreeTimrUtils.getLanguageNameById(context, list.get(position).LanguageId));
    }

    @Override
    public int getItemCount () {
        return list.size();
    }

    class LanguageViewHolder extends RecyclerView.ViewHolder {

        TextView name;

        public LanguageViewHolder (View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.language_name);
        }
    }

}
