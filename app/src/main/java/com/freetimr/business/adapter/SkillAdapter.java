package com.freetimr.business.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.freetimr.business.R;
import com.freetimr.business.models.CandidateSkills;
import com.freetimr.business.utils.FreeTimrUtils;
import com.freetimr.business.views.CustomTextView;

import java.util.ArrayList;


/**
 * Created by varunbarve on 19/09/17.
 */

public class SkillAdapter extends RecyclerView.Adapter<SkillAdapter.SkillViewHolder> {

    Context context;
    LayoutInflater inflater;
    ArrayList<CandidateSkills> list;

    public SkillAdapter(Context context, ArrayList<CandidateSkills> list) {
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public SkillViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_item_skill, parent, false);
        return new SkillAdapter.SkillViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onBindViewHolder(SkillViewHolder holder, int position) {

        final CandidateSkills model = list.get(position);

        holder.skillName.setText(FreeTimrUtils.getSkillName(context, model.Skill.SkillId));
        holder.experienceTime.setText(model.SkillExperienceInYears + "." + model.SkillExperienceInMonths + " yrs");
        holder.rating.setText("" + model.SkillRating);
    }

    public class SkillViewHolder extends RecyclerView.ViewHolder {

        CustomTextView skillName, experienceTime, rating;

        public SkillViewHolder(View itemView) {
            super(itemView);
            skillName = (CustomTextView) itemView.findViewById(R.id.skill_name);
            experienceTime = (CustomTextView) itemView.findViewById(R.id.skill_exp);
            rating = (CustomTextView) itemView.findViewById(R.id.skill_rating);
        }
    }
}
