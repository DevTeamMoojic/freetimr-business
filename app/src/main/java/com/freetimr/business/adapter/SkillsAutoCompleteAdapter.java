package com.freetimr.business.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.freetimr.business.R;
import com.freetimr.business.models.SkillModel;

import java.util.ArrayList;

/**
 * Created by varunbarve on 23/3/17.
 */

public class SkillsAutoCompleteAdapter extends ArrayAdapter<SkillModel> implements Filterable {

    Context context;
    LayoutInflater inflater;
    ArrayList<SkillModel> list;
    ArrayList<SkillModel> listAll;
    ArrayList<SkillModel> suggestions;

    public SkillsAutoCompleteAdapter(@NonNull Context context, @NonNull ArrayList<SkillModel> list) {
        super(context, R.layout.city_autocomplete_list_item, list);
        this.context = context;
        this.list = list;
        this.listAll = (ArrayList<SkillModel>) list.clone();
        this.suggestions = new ArrayList<>();
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        SkillViewHolder holder;
        if (convertView == null) {
            holder = new SkillViewHolder();

            convertView = inflater.inflate(R.layout.city_autocomplete_list_item, parent, false);
            holder.name = (TextView) convertView.findViewById(R.id.city_name);

            convertView.setTag(holder);
        } else {
            holder = (SkillViewHolder) convertView.getTag();
        }

        holder.name.setText(list.get(position).SkillName);

        return convertView;
    }

    class SkillViewHolder {
        TextView name;
    }

    @Nullable
    @Override
    public SkillModel getItem(int position) {
        return list.get(position);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return new SkillFilter();
    }

    class SkillFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (SkillModel skillModel : listAll) {
                    if (skillModel.SkillName.toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                        suggestions.add(skillModel);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<SkillModel> filteredList = (ArrayList<SkillModel>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (SkillModel c : filteredList) {
                    add(c);
                }
                notifyDataSetChanged();
            }
        }
    }
}

