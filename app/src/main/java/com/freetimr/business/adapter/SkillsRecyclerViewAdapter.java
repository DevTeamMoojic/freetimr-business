package com.freetimr.business.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freetimr.business.R;
import com.freetimr.business.models.JobSkillsModel;
import com.freetimr.business.models.LanguageModel;
import com.freetimr.business.models.SkillModel;
import com.freetimr.business.utils.FreeTimrUtils;

import java.util.ArrayList;

/**
 * Created by varunbarve on 23/3/17.
 */

public class SkillsRecyclerViewAdapter extends RecyclerView.Adapter<SkillsRecyclerViewAdapter.SkillsViewHolder> {

    Context context;
    LayoutInflater inflater;
    ArrayList<JobSkillsModel> list;

    public SkillsRecyclerViewAdapter (Context context, ArrayList<JobSkillsModel> list) {
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public SkillsViewHolder onCreateViewHolder (ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.language_item, parent, false);
        return new SkillsViewHolder(view);
    }

    @Override
    public void onBindViewHolder (SkillsViewHolder holder, int position) {
        holder.name.setText("#" + FreeTimrUtils.getSkillName(context, list.get(position).SkillId));
    }

    @Override
    public int getItemCount () {
        return list.size();
    }

    class SkillsViewHolder extends RecyclerView.ViewHolder {

        TextView name;

        public SkillsViewHolder (View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.language_name);
        }
    }

}
