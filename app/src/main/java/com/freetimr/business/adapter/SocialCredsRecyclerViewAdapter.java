package com.freetimr.business.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.freetimr.business.R;
import com.freetimr.business.fragments.PostAJobStepTwoFragments;
import com.freetimr.business.models.SocialMediaModel;

import java.util.ArrayList;

/**
 * Created by varunbarve on 23/3/17.
 */

public class SocialCredsRecyclerViewAdapter extends RecyclerView.Adapter<SocialCredsRecyclerViewAdapter.SocialCredsViewHolder> {

    Context context;
    LayoutInflater inflater;
    ArrayList<SocialMediaModel> list;
    PostAJobStepTwoFragments fragment;

    public SocialCredsRecyclerViewAdapter (Context context, ArrayList<SocialMediaModel> list, PostAJobStepTwoFragments fragment) {
        this.context = context;
        this.list = list;
        this.fragment = fragment;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public SocialCredsViewHolder onCreateViewHolder (ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.disabilities_list_item, parent, false);
        return new SocialCredsViewHolder(view);
    }

    @Override
    public void onBindViewHolder (SocialCredsViewHolder holder, final int position) {
        holder.name.setText(list.get(position).SocialMediaName);
        if (fragment == null) {
            holder.checkBox.setVisibility(View.GONE);
        } else {
            holder.checkBox.setVisibility(View.VISIBLE);
        }
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged (CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    fragment.addSocialCreds(list.get(position).SocialMediaId);
                else
                    fragment.removeSocialCreds(list.get(position).SocialMediaId);
            }
        });
    }

    @Override
    public int getItemCount () {
        return list.size();
    }

    class SocialCredsViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        CheckBox checkBox;

        public SocialCredsViewHolder (View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.disability_name);
            checkBox = (CheckBox) itemView.findViewById(R.id.disability_checked);
        }
    }

}

