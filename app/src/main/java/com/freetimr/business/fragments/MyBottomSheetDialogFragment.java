package com.freetimr.business.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freetimr.business.R;
import com.freetimr.business.adapter.BottomSheetListAdapter;
import com.freetimr.business.utils.BottomSheetListOnItemClickListener;

/**
 * Created by Admin on 21-Mar-17.
 */

public class MyBottomSheetDialogFragment extends BottomSheetDialogFragment {

    BottomSheetListOnItemClickListener bottomSheetListOnItemClickListener;
    String[] strings;
    String title;
    RecyclerView bottomSheetRecyclerView;
    BottomSheetListAdapter adapter;
    TextView bottomSheetTitle;

    public static MyBottomSheetDialogFragment getInstance(String title, String[] strings, BottomSheetListOnItemClickListener bottomSheetListOnItemClickListener) {
        MyBottomSheetDialogFragment myBottomSheetDialogFragment = new MyBottomSheetDialogFragment();
        Bundle args = new Bundle();
        args.putString("BottomSheetTitle", title);
        args.putStringArray("StringArray", strings);
        args.putSerializable("BottomSheetListOnItemClickListener", bottomSheetListOnItemClickListener);
        myBottomSheetDialogFragment.setArguments(args);
        return myBottomSheetDialogFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        title = getArguments().getString("BottomSheetTitle");
        strings = getArguments().getStringArray("StringArray");
        bottomSheetListOnItemClickListener = (BottomSheetListOnItemClickListener) getArguments().getSerializable("BottomSheetListOnItemClickListener");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bottom_sheet_layout, container, false);
        bottomSheetTitle = (TextView) v.findViewById(R.id.bottom_sheet_title);
        bottomSheetTitle.setText(title);
        bottomSheetRecyclerView = (RecyclerView) v.findViewById(R.id.bottom_sheet_recycler_view);
        adapter = new BottomSheetListAdapter(MyBottomSheetDialogFragment.this.getActivity(), strings, bottomSheetListOnItemClickListener, MyBottomSheetDialogFragment.this);
        bottomSheetRecyclerView.setAdapter(adapter);
        bottomSheetRecyclerView.setLayoutManager(new LinearLayoutManager(MyBottomSheetDialogFragment.this.getActivity()));
        return v;
    }
}
