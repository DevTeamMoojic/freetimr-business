package com.freetimr.business.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.freetimr.business.R;
import com.freetimr.business.activities.HomeActivity;
import com.freetimr.business.activities.JobReviewActivity;
import com.freetimr.business.models.PostJobRequestModel;
import com.freetimr.business.utils.Constants;
import com.freetimr.business.utils.FreeTimrUtils;
import com.freetimr.business.utils.SharedPreferenceStore;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 20-Mar-17.
 */

public class PostAJobStepFourFragments extends Fragment {

    PostJobRequestModel postJobRequestModel;
    Button payBtn, reviewBtn;

    public static PostAJobStepFourFragments getInstance (PostJobRequestModel postJobRequestModel) {
        PostAJobStepFourFragments fragment = new PostAJobStepFourFragments();
        Bundle args = new Bundle();
        args.putParcelable("PostJobRequestModel", postJobRequestModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate (@Nullable Bundle savedInstanceState) {
        postJobRequestModel = getArguments().getParcelable("PostJobRequestModel");
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView (LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_post_job_step_four, container, false);
        payBtn = (Button) rootView.findViewById(R.id.pay_btn);
        reviewBtn = (Button) rootView.findViewById(R.id.review_btn);

        payBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                postAJob();
            }
        });

        reviewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                Intent intent = new Intent(PostAJobStepFourFragments.this.getActivity(), JobReviewActivity.class);
                intent.putExtra("PostJobRequestModel",postJobRequestModel);
                startActivity(intent);
            }
        });
        return rootView;
    }

    private void postAJob () {
        FreeTimrUtils.showProgressDialog(PostAJobStepFourFragments.this.getActivity());
        FreeTimrUtils.log(FreeTimrUtils.newGson().toJson(postJobRequestModel));
        Call<PostJobRequestModel> call = FreeTimrUtils.getRetrofit().postAJob(postJobRequestModel);
        call.enqueue(new Callback<PostJobRequestModel>() {
            @Override
            public void onResponse (Call<PostJobRequestModel> call, Response<PostJobRequestModel> response) {
                if (response.code() == 200) {
                    SharedPreferenceStore.storeValue(PostAJobStepFourFragments.this.getActivity(), Constants.IS_FIRST_JOB_POSTED, true);
                    Intent intent = new Intent(PostAJobStepFourFragments.this.getActivity(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    getActivity().finish();

                } else {
                    FreeTimrUtils.showMessage(PostAJobStepFourFragments.this.getActivity(), "Something went wrong. Please try again later.");
                }
                FreeTimrUtils.cancelCurrentDialog(PostAJobStepFourFragments.this.getActivity());
            }

            @Override
            public void onFailure (Call<PostJobRequestModel> call, Throwable t) {
                FreeTimrUtils.log("" + t);
                FreeTimrUtils.cancelCurrentDialog(PostAJobStepFourFragments.this.getActivity());
            }
        });
    }
}

