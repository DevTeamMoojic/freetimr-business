package com.freetimr.business.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.freetimr.business.R;
import com.freetimr.business.adapter.CityAutoCompleteAdapter;
import com.freetimr.business.adapter.CityJobLocationAdapter;
import com.freetimr.business.models.CompanyModel;
import com.freetimr.business.models.JobCategoryModel;
import com.freetimr.business.models.JobHeaderModel;
import com.freetimr.business.models.JobLocationsModel;
import com.freetimr.business.models.JobTypeModel;
import com.freetimr.business.models.JobsModel;
import com.freetimr.business.models.PostJobRequestModel;
import com.freetimr.business.utils.BottomSheetListOnItemClickListener;
import com.freetimr.business.utils.Constants;
import com.freetimr.business.utils.FreeTimrURLs;
import com.freetimr.business.utils.FreeTimrUtils;
import com.freetimr.business.utils.SharedPreferenceStore;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by varunbarve on 15/3/17.
 */

@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
public class PostAJobStepOneFragments extends Fragment {

    private static final int SHIFT_MORNING = 1;
    private static final int SHIFT_NOON = 2;
    private static final int SHIFT_EVENING = 3;
    private static final int SHIFT_NIGHT = 4;

    boolean isEdit;

    Button continueButton;
    TextView jobHeader, jobCategory, jobStartDate, jobEndDate, jobLocationsAdd, shiftMorning, shiftNoon, shiftEvening, shiftNight, shiftStartTime, shiftEndTime, jobType;
    EditText companyName, payPerDayEdit, numberOfHoursEdit, numberOfDaysEdit, jobTitle, jobDescription, jobEligibility, jobBenifits;
    RecyclerView cityJobLocationRecyclerView;
    PostJobRequestModel postJobRequestModel;
    String jobTitleStr = "", jobDescriptionStr = "", jobEligibilityStr = "", jobBenifitsStr = "", companyNameStr = "", jobTypeStr = "", companyIdStr = "", jobStartTimeStr = "", jobEndTimeStr = "", payPerDay = "", numberOfHours = "", numberOfDays = "";
    int jobAvailabilityId = -1, jobCategoryId = -1, jobTypeId = -1;
    Date jobStartDateStr, jobEndDateStr;
    ArrayList<JobLocationsModel> jobLocationsModelsForRequest = new ArrayList<>();
    CityJobLocationAdapter cityJobLocationAdapter;
    CompanyModel companyModel;
    JobsModel model;

    Calendar myCalendar = Calendar.getInstance();

    DatePickerDialog.OnDateSetListener startDate = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet (DatePicker view, int year, int monthOfYear,
                               int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            jobStartDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
            jobStartDateStr = myCalendar.getTime();
        }
    };

    DatePickerDialog.OnDateSetListener endDate = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet (DatePicker view, int year, int monthOfYear,
                               int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            jobEndDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
            jobEndDateStr = myCalendar.getTime();
        }
    };

    public static PostAJobStepOneFragments getInstance (boolean isEdit, JobsModel model) {
        PostAJobStepOneFragments fragment = new PostAJobStepOneFragments();
        Bundle bundle = new Bundle();
        bundle.putBoolean("IsEdit", isEdit);
        bundle.putParcelable("JobsModel", model);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate (@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isEdit = getArguments().getBoolean("IsEdit");
            model = getArguments().getParcelable("JobsModel");
        }
    }

    @Nullable
    @Override
    public View onCreateView (LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_post_job_step_one, container, false);

        companyModel = FreeTimrUtils.getCompanyModel(PostAJobStepOneFragments.this.getActivity());

        continueButton = (Button) rootView.findViewById(R.id.continue_btn);
        jobHeader = (TextView) rootView.findViewById(R.id.job_header_label);
        jobStartDate = (TextView) rootView.findViewById(R.id.job_start_date);
        jobEndDate = (TextView) rootView.findViewById(R.id.job_end_date);
        cityJobLocationRecyclerView = (RecyclerView) rootView.findViewById(R.id.city_job_location_rv);
        jobLocationsAdd = (TextView) rootView.findViewById(R.id.job_locations_add);
        shiftMorning = (TextView) rootView.findViewById(R.id.shift_morning);
        shiftNoon = (TextView) rootView.findViewById(R.id.shift_noon);
        shiftEvening = (TextView) rootView.findViewById(R.id.shift_evening);
        shiftNight = (TextView) rootView.findViewById(R.id.shift_night);
        shiftStartTime = (TextView) rootView.findViewById(R.id.job_start_shift_time);
        shiftEndTime = (TextView) rootView.findViewById(R.id.job_end_shift_time);
        companyName = (EditText) rootView.findViewById(R.id.company_name_edit);
        jobType = (TextView) rootView.findViewById(R.id.job_type_edit);
        payPerDayEdit = (EditText) rootView.findViewById(R.id.pay_per_day_edit);
        numberOfHoursEdit = (EditText) rootView.findViewById(R.id.number_of_hours_edit);
        numberOfDaysEdit = (EditText) rootView.findViewById(R.id.number_of_days_edit);
        jobCategory = (TextView) rootView.findViewById(R.id.job_category_label);
        jobTitle = (EditText) rootView.findViewById(R.id.job_title_label);
        jobDescription = (EditText) rootView.findViewById(R.id.job_description_label);
        jobBenifits = (EditText) rootView.findViewById(R.id.job_benefits_label);
        jobEligibility = (EditText) rootView.findViewById(R.id.job_eligibility_label);

        if (isEdit) {

            FreeTimrUtils.log(FreeTimrUtils.newGson().toJson(model));

            jobTitleStr = model.JobTitle;
            jobDescriptionStr = model.JobDescription;
            jobEligibilityStr = model.JobEligibility;
            jobBenifitsStr = model.JobBenefits;
            companyIdStr = model.CompanyId;
            companyNameStr = model.CompanyName;
            payPerDay = "" + model.PayPerDay;
            numberOfDays = "" + model.NoOfJobDays;
            numberOfHours = "" + model.NoOfHours;
            jobCategoryId = model.JobCategoryId;
            jobTypeId = model.JobTypeId;
            jobStartDateStr = model.StartDate;
            jobEndDateStr = model.EndDate;
            jobStartTimeStr = model.StartTime;
            jobEndTimeStr = model.EndTime;
            jobAvailabilityId = model.JobAvailabilityTimeId;

            jobLocationsModelsForRequest = new ArrayList<>();
            for (int i = 0; i < model.JobLocations.length; i++) {
                jobLocationsModelsForRequest.add(new JobLocationsModel(model.JobLocations[i].CityId, model.JobLocations[i].LocationName, FreeTimrUtils.getCityNameById(this.getActivity(), model.JobLocations[i].CityId)));
            }
            updateCityJobLocationsRecyclerView();

            jobTitle.setText(model.JobTitle);
            jobDescription.setText(model.JobDescription);
            jobEligibility.setText(model.JobEligibility);
            jobBenifits.setText(model.JobBenefits);
            companyName.setText(model.CompanyName);
            payPerDayEdit.setText("" + model.PayPerDay);
            numberOfDaysEdit.setText("" + model.NoOfJobDays);
            numberOfHoursEdit.setText("" + model.NoOfHours);
            jobCategory.setText(FreeTimrUtils.getCategoryNameById(this.getActivity(), model.JobCategoryId));
            jobType.setText(FreeTimrUtils.getJobTypeById(this.getActivity(), model.JobTypeId));
            selectShift(jobAvailabilityId);

            SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
            jobStartDate.setText(sdfDate.format(model.StartDate));
            jobEndDate.setText(sdfDate.format(model.EndDate));

            shiftStartTime.setText(model.StartTime);
            shiftEndTime.setText(model.EndTime);
        }

        jobCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {

                final ArrayList<JobCategoryModel> jobCategoryModelsList = FreeTimrUtils.getJobCategoryList(PostAJobStepOneFragments.this.getActivity());

                final String[] strings = new String[jobCategoryModelsList.size()];
                for (int i = 0; i < jobCategoryModelsList.size(); i++) {
                    strings[i] = jobCategoryModelsList.get(i).JobCategoryName;
                }

                FreeTimrUtils.showBottomSheetDialog(PostAJobStepOneFragments.this.getFragmentManager(), "Job Category", strings, new BottomSheetListOnItemClickListener() {
                    @Override
                    public void onListItemSelected (int position) {
                        jobCategoryId = jobCategoryModelsList.get(position).JobCategoryId;
                        jobCategory.setText(strings[position]);
                    }
                });
            }
        });

        jobType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                final ArrayList<JobTypeModel> jobTypeModelsList = FreeTimrUtils.getJobTypeList(PostAJobStepOneFragments.this.getActivity());

                final String[] strings = new String[jobTypeModelsList.size()];
                for (int i = 0; i < jobTypeModelsList.size(); i++) {
                    strings[i] = jobTypeModelsList.get(i).JobTypeName;
                }

                FreeTimrUtils.showBottomSheetDialog(PostAJobStepOneFragments.this.getFragmentManager(), "Job Category", strings, new BottomSheetListOnItemClickListener() {
                    @Override
                    public void onListItemSelected (int position) {
                        jobTypeId = jobTypeModelsList.get(position).JobTypeId;
                        jobType.setText(strings[position]);
                    }
                });
            }
        });

        shiftStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                showStartTimePickerDialog();
            }
        });

        shiftEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                showEndTimePickerDialog();
            }
        });

        shiftMorning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                selectShift(SHIFT_MORNING);
            }
        });

        shiftNoon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                selectShift(SHIFT_NOON);
            }
        });

        shiftEvening.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                selectShift(SHIFT_EVENING);
            }
        });

        shiftNight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                selectShift(SHIFT_NIGHT);
            }
        });

        jobLocationsAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                if (jobLocationsModelsForRequest.size() < 10)
                    addJobLocations();
                else
                    FreeTimrUtils.showMessage(PostAJobStepOneFragments.this.getActivity(), "You cannot add more locations");
            }
        });

        jobStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                new DatePickerDialog(PostAJobStepOneFragments.this.getActivity(), startDate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        jobEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                new DatePickerDialog(PostAJobStepOneFragments.this.getActivity(), endDate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        jobHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
//                getJobHeaderList();
            }
        });

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {

                jobTitleStr = jobTitle.getText().toString();
                jobDescriptionStr = jobDescription.getText().toString();
                jobEligibilityStr = jobEligibility.getText().toString();
                jobBenifitsStr = jobBenifits.getText().toString();
                companyIdStr = companyModel.CompanyId;
                companyNameStr = companyName.getText().toString();
                payPerDay = payPerDayEdit.getText().toString();
                numberOfDays = numberOfDaysEdit.getText().toString();
                numberOfHours = numberOfHoursEdit.getText().toString();

                if (jobCategoryId == -1 || jobTitleStr.isEmpty() || jobDescriptionStr.isEmpty() || jobEligibilityStr.isEmpty() || jobBenifitsStr.isEmpty() || jobTypeId == -1 || companyNameStr.isEmpty() || jobLocationsModelsForRequest.size() == 0 || payPerDay.isEmpty() || numberOfDays.isEmpty() || numberOfHours.isEmpty() || jobStartDateStr == null || jobEndDateStr == null || jobAvailabilityId == -1 || jobStartTimeStr.isEmpty() || jobEndTimeStr.isEmpty()) {
                    FreeTimrUtils.showMessage(PostAJobStepOneFragments.this.getActivity(), "Please fill all the fields");
                } else {
                    postJobRequestModel = new PostJobRequestModel();

                    postJobRequestModel.CompanyId = companyIdStr;
                    postJobRequestModel.JobTitle = jobTitleStr;
                    postJobRequestModel.CompanyName = companyNameStr;
                    postJobRequestModel.JobTypeId = jobTypeId;
                    postJobRequestModel.JobCategoryId = jobCategoryId;
                    postJobRequestModel.JobDescription = jobDescriptionStr;
                    postJobRequestModel.JobEligibility = jobEligibilityStr;
                    postJobRequestModel.JobBenefits = jobBenifitsStr;
                    postJobRequestModel.CreatedBy = companyModel.CompanyName;

                    postJobRequestModel.JobLocations = new JobLocationsModel[jobLocationsModelsForRequest.size()];
                    postJobRequestModel.JobLocations = jobLocationsModelsForRequest.toArray(postJobRequestModel.JobLocations);

                    postJobRequestModel.PayPerDay = Double.parseDouble(payPerDay);
                    postJobRequestModel.NoOfHours = Double.parseDouble(numberOfHours);
                    postJobRequestModel.NoOfJobDays = Double.parseDouble(numberOfDays);
                    postJobRequestModel.StartTime = jobStartTimeStr;
                    postJobRequestModel.EndTime = jobEndTimeStr;
                    postJobRequestModel.StartDate = jobStartDateStr;
                    postJobRequestModel.EndDate = jobEndDateStr;
                    postJobRequestModel.JobAvailabilityTimeId = jobAvailabilityId;

                    if (isEdit) {
                        addFragment(PostAJobStepTwoFragments.getInstance(postJobRequestModel, model, isEdit));
                    } else {
                        addFragment(PostAJobStepTwoFragments.getInstance(postJobRequestModel));
                    }
                }
            }
        });

        updateCityJobLocationsRecyclerView();

        return rootView;
    }

    private void showStartTimePickerDialog () {
        Calendar mCurrentTime = Calendar.getInstance();
        int hour = mCurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mCurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(PostAJobStepOneFragments.this.getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet (TimePicker timePicker, int selectedHour, int selectedMinute) {
                jobStartTimeStr = selectedHour + ":" + selectedMinute;
                shiftStartTime.setText(FreeTimrUtils.changeFormat(selectedHour + ":" + selectedMinute));
            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.show();

    }

    private void showEndTimePickerDialog () {
        Calendar mCurrentTime = Calendar.getInstance();
        int hour = mCurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mCurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(PostAJobStepOneFragments.this.getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet (TimePicker timePicker, int selectedHour, int selectedMinute) {
                jobEndTimeStr = selectedHour + ":" + selectedMinute;
                shiftEndTime.setText(FreeTimrUtils.changeFormat(selectedHour + ":" + selectedMinute));
            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.show();

    }

    private void selectShift (int shiftId) {
        switch (shiftId) {
            case SHIFT_MORNING:
                shiftMorning.setBackground(getResources().getDrawable(R.drawable.blue_curved_background));
                shiftNoon.setBackground(getResources().getDrawable(R.drawable.light_blue_curved_background));
                shiftEvening.setBackground(getResources().getDrawable(R.drawable.light_blue_curved_background));
                shiftNight.setBackground(getResources().getDrawable(R.drawable.light_blue_curved_background));

                shiftMorning.setTextColor(Color.parseColor("#ffffff"));
                shiftNoon.setTextColor(Color.parseColor("#85a1bb"));
                shiftEvening.setTextColor(Color.parseColor("#85a1bb"));
                shiftNight.setTextColor(Color.parseColor("#85a1bb"));

                jobAvailabilityId = SHIFT_MORNING;
                break;
            case SHIFT_NOON:
                shiftMorning.setBackground(getResources().getDrawable(R.drawable.light_blue_curved_background));
                shiftNoon.setBackground(getResources().getDrawable(R.drawable.blue_curved_background));
                shiftEvening.setBackground(getResources().getDrawable(R.drawable.light_blue_curved_background));
                shiftNight.setBackground(getResources().getDrawable(R.drawable.light_blue_curved_background));

                shiftMorning.setTextColor(Color.parseColor("#85a1bb"));
                shiftNoon.setTextColor(Color.parseColor("#ffffff"));
                shiftEvening.setTextColor(Color.parseColor("#85a1bb"));
                shiftNight.setTextColor(Color.parseColor("#85a1bb"));

                jobAvailabilityId = SHIFT_NOON;
                break;
            case SHIFT_EVENING:
                shiftMorning.setBackground(getResources().getDrawable(R.drawable.light_blue_curved_background));
                shiftNoon.setBackground(getResources().getDrawable(R.drawable.light_blue_curved_background));
                shiftEvening.setBackground(getResources().getDrawable(R.drawable.blue_curved_background));
                shiftNight.setBackground(getResources().getDrawable(R.drawable.light_blue_curved_background));

                shiftMorning.setTextColor(Color.parseColor("#85a1bb"));
                shiftNoon.setTextColor(Color.parseColor("#85a1bb"));
                shiftEvening.setTextColor(Color.parseColor("#ffffff"));
                shiftNight.setTextColor(Color.parseColor("#85a1bb"));

                jobAvailabilityId = SHIFT_EVENING;
                break;
            case SHIFT_NIGHT:
                shiftMorning.setBackground(getResources().getDrawable(R.drawable.light_blue_curved_background));
                shiftNoon.setBackground(getResources().getDrawable(R.drawable.light_blue_curved_background));
                shiftEvening.setBackground(getResources().getDrawable(R.drawable.light_blue_curved_background));
                shiftNight.setBackground(getResources().getDrawable(R.drawable.blue_curved_background));

                shiftMorning.setTextColor(Color.parseColor("#85a1bb"));
                shiftNoon.setTextColor(Color.parseColor("#85a1bb"));
                shiftEvening.setTextColor(Color.parseColor("#85a1bb"));
                shiftNight.setTextColor(Color.parseColor("#ffffff"));

                jobAvailabilityId = SHIFT_NIGHT;
                break;
            default:
                shiftMorning.setBackground(getResources().getDrawable(R.drawable.blue_curved_background));
                shiftNoon.setBackground(getResources().getDrawable(R.drawable.light_blue_curved_background));
                shiftEvening.setBackground(getResources().getDrawable(R.drawable.light_blue_curved_background));
                shiftNight.setBackground(getResources().getDrawable(R.drawable.light_blue_curved_background));

                shiftMorning.setTextColor(Color.parseColor("#ffffff"));
                shiftNoon.setTextColor(Color.parseColor("#85a1bb"));
                shiftEvening.setTextColor(Color.parseColor("#85a1bb"));
                shiftNight.setTextColor(Color.parseColor("#85a1bb"));

                jobAvailabilityId = SHIFT_MORNING;
                break;

        }
    }

    int cityIdTemp = -1;
    String cityNameTemp = "";
    String jobLocationTemp = "";

    private void addJobLocations () {

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View alertLayout = inflater.inflate(R.layout.location_city_dialog, null);
        final AutoCompleteTextView cityAutoCompleteTextView = (AutoCompleteTextView) alertLayout.findViewById(R.id.city_autocomplete);
        final EditText jobAbsoluteLocation = (EditText) alertLayout.findViewById(R.id.job_absolute_locations);

        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Montserrat-SemiBold.otf");
        cityAutoCompleteTextView.setTypeface(tf);

        final CityAutoCompleteAdapter adapter = new CityAutoCompleteAdapter(PostAJobStepOneFragments.this.getActivity(), FreeTimrUtils.getCityList(PostAJobStepOneFragments.this.getActivity()));

        cityAutoCompleteTextView.setAdapter(adapter);
        cityAutoCompleteTextView.setThreshold(1);
        cityAutoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged (CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged (CharSequence s, int start, int before, int count) {
                cityAutoCompleteTextView.showDropDown();
                adapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged (Editable s) {

            }
        });

        cityAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick (AdapterView<?> adapterView, View view, int i, long l) {
                cityIdTemp = adapter.getItem(i).CityId;
                cityNameTemp = adapter.getItem(i).CityName;
                cityAutoCompleteTextView.setText(adapter.getItem(i).CityName);
            }
        });

        AlertDialog.Builder alert = new AlertDialog.Builder(PostAJobStepOneFragments.this.getActivity());
        alert.setView(alertLayout);
        alert.setCancelable(true);
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick (DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alert.setPositiveButton("Add", new DialogInterface.OnClickListener() {

            @Override
            public void onClick (DialogInterface dialog, int which) {
                jobLocationTemp = jobAbsoluteLocation.getText().toString();
                if (jobLocationTemp.isEmpty() || cityIdTemp == -1) {
                    FreeTimrUtils.showMessage(PostAJobStepOneFragments.this.getActivity(), "Please fill all the fields");
                } else {
                    jobLocationsModelsForRequest.add(new JobLocationsModel(cityIdTemp, jobLocationTemp, cityNameTemp));
                    updateCityJobLocationsRecyclerView();
                }
            }
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }

    private void updateCityJobLocationsRecyclerView () {
        cityJobLocationAdapter = new CityJobLocationAdapter(PostAJobStepOneFragments.this.getActivity(), jobLocationsModelsForRequest);
        cityJobLocationRecyclerView.setAdapter(cityJobLocationAdapter);
        cityJobLocationRecyclerView.setLayoutManager(new LinearLayoutManager(PostAJobStepOneFragments.this.getActivity()));
    }

//    public void getJobHeaderList () {
//        FreeTimrUtils.showProgressDialog(PostAJobStepOneFragments.this.getActivity());
//
//        String candidateId = SharedPreferenceStore.getValue(PostAJobStepOneFragments.this.getActivity(), Constants.KEY_COMPANY_MODEL, "");
//        CompanyModel companyModel = FreeTimrUtils.newGson().fromJson(candidateId, CompanyModel.class);
//        Call<JobHeaderRequest> call = FreeTimrUtils.getRetrofit().getJobHeader(FreeTimrURLs.HOST + FreeTimrURLs.GET_JOB_HEADERS + "68fd04a6-1163-47bb-ac80-6a0ca2e7d72f");//+companyModel.CompanyId
//        call.enqueue(new Callback<JobHeaderRequest>() {
//            @Override
//            public void onResponse (Call<JobHeaderRequest> call, Response<JobHeaderRequest> response) {
//                FreeTimrUtils.log("" + response.code());
//
//                final JobHeaderModel[] jobHeaderModels = response.body().JobHeader;
//
//                final String[] strings = new String[jobHeaderModels.length];
//                for (int i = 0; i < jobHeaderModels.length; i++) {
//                    strings[i] = jobHeaderModels[i].JobHeaderName;
//                }
//
//                FreeTimrUtils.showBottomSheetDialog(PostAJobStepOneFragments.this.getFragmentManager(), "Job Header", strings, new BottomSheetListOnItemClickListener() {
//                    @Override
//                    public void onListItemSelected (int position) {
////                        postJobRequestModel.JobHeaderId = jobHeaderModels[position].JobHeaderId;
//                        jobHeaderId = jobHeaderModels[position].JobHeaderId;
//                        jobHeader.setText(strings[position]);
//                    }
//                });
//
//                FreeTimrUtils.cancelCurrentDialog(PostAJobStepOneFragments.this.getActivity());
//            }
//
//            @Override
//            public void onFailure (Call<JobHeaderRequest> call, Throwable t) {
//                FreeTimrUtils.log("" + t);
//                FreeTimrUtils.cancelCurrentDialog(PostAJobStepOneFragments.this.getActivity());
//            }
//        });
//    }

    public class JobHeaderRequest {
        JobHeaderModel[] JobHeader;
    }

    public void addFragment (Fragment fragment) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.commit();
    }
}

