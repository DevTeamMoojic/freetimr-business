package com.freetimr.business.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.freetimr.business.R;
import com.freetimr.business.activities.JobDetailsActivity;
import com.freetimr.business.models.JobsModel;
import com.freetimr.business.models.PostJobRequestModel;
import com.freetimr.business.utils.FreeTimrUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 20-Mar-17.
 */

public class PostAJobStepThreeFragments extends Fragment {

    PostJobRequestModel postJobRequestModel;
    RadioButton workLocationHome, workLocationField, workLocationOffice, jobActivityMobile, jobActivityStanding, jobActivitySeated, trainingYes, trainingNo, paidYes, paidNo;
    EditText priorDays, duration, breakEdit, noOfPositions;
    RelativeLayout priorDaysContainer, durationContainer, trainingPayContainer;
    Button continueBtn, reviewBtn;

    int workLocationId = -1, jobActivityId = -1;
    String numberOfDaysPrior = "", durationDays = "", numberOfBreaks = "", numberOfPositions = "";
    boolean isTrainingProvided, isCandidatePaid;
    JobsModel model;
    boolean isEdit = false;

    public static PostAJobStepThreeFragments getInstance (PostJobRequestModel postJobRequestModel) {
        PostAJobStepThreeFragments fragment = new PostAJobStepThreeFragments();
        Bundle args = new Bundle();
        args.putParcelable("PostJobRequestModel", postJobRequestModel);
        fragment.setArguments(args);
        return fragment;
    }

    public static PostAJobStepThreeFragments getInstance (PostJobRequestModel postJobRequestModel, JobsModel model, boolean isEdit) {
        PostAJobStepThreeFragments fragment = new PostAJobStepThreeFragments();
        Bundle args = new Bundle();
        args.putParcelable("PostJobRequestModel", postJobRequestModel);
        args.putParcelable("JobsModel", model);
        args.putBoolean("IsEdit", isEdit);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate (@Nullable Bundle savedInstanceState) {
        postJobRequestModel = getArguments().getParcelable("PostJobRequestModel");
        model = getArguments().getParcelable("JobsModel");
        isEdit = getArguments().getBoolean("IsEdit");
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView (LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_post_job_step_three, container, false);

        workLocationHome = (RadioButton) rootView.findViewById(R.id.work_location_home);
        workLocationField = (RadioButton) rootView.findViewById(R.id.work_location_field);
        workLocationOffice = (RadioButton) rootView.findViewById(R.id.work_location_office);
        jobActivityMobile = (RadioButton) rootView.findViewById(R.id.job_activity_mobile);
        jobActivityStanding = (RadioButton) rootView.findViewById(R.id.job_activity_standing);
        jobActivitySeated = (RadioButton) rootView.findViewById(R.id.job_activity_seated);
        trainingYes = (RadioButton) rootView.findViewById(R.id.training_yes);
        trainingNo = (RadioButton) rootView.findViewById(R.id.training_no);
        paidYes = (RadioButton) rootView.findViewById(R.id.paid_yes);
        paidNo = (RadioButton) rootView.findViewById(R.id.paid_no);
        priorDays = (EditText) rootView.findViewById(R.id.training_prior_days);
        duration = (EditText) rootView.findViewById(R.id.training_number_days);
        priorDaysContainer = (RelativeLayout) rootView.findViewById(R.id.prior_days_container);
        durationContainer = (RelativeLayout) rootView.findViewById(R.id.duration_container);
        trainingPayContainer = (RelativeLayout) rootView.findViewById(R.id.training_pay_container);
        breakEdit = (EditText) rootView.findViewById(R.id.break_edit);
        continueBtn = (Button) rootView.findViewById(R.id.continue_btn_step_three);
        noOfPositions = (EditText) rootView.findViewById(R.id.no_of_positions);
        reviewBtn = (Button) rootView.findViewById(R.id.review_btn_step_three);

        workLocationHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                workLocationId = 3;
            }
        });
        workLocationField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                workLocationId = 2;
            }
        });
        workLocationOffice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                workLocationId = 1;
            }
        });

        jobActivityMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                jobActivityId = 1;
            }
        });
        jobActivityStanding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                jobActivityId = 2;
            }
        });
        jobActivitySeated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                jobActivityId = 3;
            }
        });

        trainingYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                priorDaysContainer.setVisibility(View.VISIBLE);
                durationContainer.setVisibility(View.VISIBLE);
                trainingPayContainer.setVisibility(View.VISIBLE);
                isTrainingProvided = true;
            }
        });

        trainingNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                priorDaysContainer.setVisibility(View.GONE);
                durationContainer.setVisibility(View.GONE);
                trainingPayContainer.setVisibility(View.GONE);
                isTrainingProvided = false;
            }
        });

        paidYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                isCandidatePaid = true;
            }
        });

        paidNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                isCandidatePaid = false;
            }
        });

        if (isEdit) {
            numberOfBreaks = "" + model.NoOfBreaks;
            numberOfPositions = "" + model.NoOfPositions;
            workLocationId = model.WorkLocationId;
            jobActivityId = model.JobActivityId;
            isTrainingProvided = model.IsTrainingProvided;
            isCandidatePaid = model.IsPaidForTraining;

            if (isTrainingProvided) {
                numberOfDaysPrior = "" + model.TrainingPriorDays;
                durationDays = "" + model.TrainingForNoOfDays;
            }

            if (model.WorkLocationId == 1) {
                workLocationOffice.setChecked(true);
                workLocationField.setChecked(false);
                workLocationHome.setChecked(false);
            } else if (model.WorkLocationId == 2) {
                workLocationOffice.setChecked(false);
                workLocationField.setChecked(true);
                workLocationHome.setChecked(false);
            } else if (model.WorkLocationId == 3) {
                workLocationOffice.setChecked(false);
                workLocationField.setChecked(false);
                workLocationHome.setChecked(true);
            }

            if (model.JobActivityId == 1) {
                jobActivityMobile.setChecked(true);
                jobActivityStanding.setChecked(false);
                jobActivitySeated.setChecked(false);
            } else if (model.JobActivityId == 2) {
                jobActivityMobile.setChecked(false);
                jobActivityStanding.setChecked(true);
                jobActivitySeated.setChecked(false);
            } else if (model.JobActivityId == 3) {
                jobActivityMobile.setChecked(false);
                jobActivityStanding.setChecked(false);
                jobActivitySeated.setChecked(true);
            }

            if (model.IsTrainingProvided) {
                trainingYes.setChecked(true);
                trainingNo.setChecked(false);

                priorDaysContainer.setVisibility(View.VISIBLE);
                durationContainer.setVisibility(View.VISIBLE);
                trainingPayContainer.setVisibility(View.VISIBLE);
            } else {
                trainingYes.setChecked(false);
                trainingNo.setChecked(true);

                priorDaysContainer.setVisibility(View.GONE);
                durationContainer.setVisibility(View.GONE);
                trainingPayContainer.setVisibility(View.GONE);
            }

            if (model.IsPaidForTraining) {
                paidYes.setChecked(true);
                paidNo.setChecked(false);
            } else {
                paidYes.setChecked(false);
                paidNo.setChecked(true);
            }

            breakEdit.setText("" + model.NoOfBreaks);
            priorDays.setText("" + model.TrainingPriorDays);
            duration.setText("" + model.TrainingForNoOfDays);
            noOfPositions.setText("" + model.NoOfPositions);

            reviewBtn.setVisibility(View.VISIBLE);
            continueBtn.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1));
            continueBtn.setText("Update");
        }

        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                numberOfBreaks = breakEdit.getText().toString();
                numberOfDaysPrior = priorDays.getText().toString();
                durationDays = duration.getText().toString();
                numberOfPositions = noOfPositions.getText().toString();

                if (workLocationId == -1) {
                    FreeTimrUtils.showMessage(PostAJobStepThreeFragments.this.getActivity(), "Please select a work location");
                    return;
                }

                if (jobActivityId == -1) {
                    FreeTimrUtils.showMessage(PostAJobStepThreeFragments.this.getActivity(), "Please select a type of activity");
                    return;
                }

                if (numberOfBreaks.isEmpty()) {
                    FreeTimrUtils.showMessage(PostAJobStepThreeFragments.this.getActivity(), "Please add number of breaks");
                    return;
                }

                if (isTrainingProvided) {
                    if (numberOfDaysPrior.isEmpty()) {
                        FreeTimrUtils.showMessage(PostAJobStepThreeFragments.this.getActivity(), "How many days Prior?");
                        return;
                    }

                    if (durationDays.isEmpty()) {
                        FreeTimrUtils.showMessage(PostAJobStepThreeFragments.this.getActivity(), "How many days training?");
                        return;
                    }
                }

                if (numberOfPositions.isEmpty()) {
                    FreeTimrUtils.showMessage(PostAJobStepThreeFragments.this.getActivity(), "Enter number of positions");
                    return;
                }

                // EVERYTHING IS VALID
                postJobRequestModel.WorkLocationId = workLocationId;
                postJobRequestModel.JobActivityId = jobActivityId;
                postJobRequestModel.IsTrainingProvided = isTrainingProvided;
                postJobRequestModel.IsPaidForTraining = isCandidatePaid;
                postJobRequestModel.TrainingPriorDays = numberOfDaysPrior.isEmpty() ? 0 : Integer.parseInt(numberOfDaysPrior);
                postJobRequestModel.TrainingForNoOfDays = durationDays.isEmpty() ? 0 : Integer.parseInt(durationDays);
                postJobRequestModel.NoOfBreaks = numberOfBreaks.isEmpty() ? 0 : Integer.parseInt(numberOfBreaks);
                postJobRequestModel.NoOfPositions = numberOfPositions.isEmpty() ? 0 : Integer.parseInt(numberOfPositions);

                FreeTimrUtils.log(FreeTimrUtils.newGson().toJson(postJobRequestModel));

                if (isEdit) {
                    updateJob(postJobRequestModel);
                } else {
                    addFragment(PostAJobStepFourFragments.getInstance(postJobRequestModel));
                }
            }
        });
        return rootView;
    }

    public void addFragment (Fragment fragment) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.commit();
    }

    private void updateJob (PostJobRequestModel postJobRequestModel) {
        FreeTimrUtils.showProgressDialog(this.getActivity());
        postJobRequestModel.JobId = model.JobId;

        Call<String> call = FreeTimrUtils.getRetrofit().updateJob(postJobRequestModel);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse (Call<String> call, Response<String> response) {
                FreeTimrUtils.log("" + response.code());
                if (response.code() == 200) {
                    Intent intent = new Intent(PostAJobStepThreeFragments.this.getActivity(), JobDetailsActivity.class);
                    intent.putExtra("JobId", response.body());
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    FreeTimrUtils.showMessage(PostAJobStepThreeFragments.this.getActivity(), "Something went wrong. Please try again.");
                }
                FreeTimrUtils.cancelCurrentDialog(PostAJobStepThreeFragments.this.getActivity());
            }

            @Override
            public void onFailure (Call<String> call, Throwable t) {
                FreeTimrUtils.log(t.getLocalizedMessage());
                FreeTimrUtils.cancelCurrentDialog(PostAJobStepThreeFragments.this.getActivity());
            }
        });
    }
}
