package com.freetimr.business.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.freetimr.business.R;
import com.freetimr.business.adapter.DisabilitiesRecyclerViewAdapter;
import com.freetimr.business.adapter.LanguageAutoCompleteAdapter;
import com.freetimr.business.adapter.LanguageRecyclerViewAdapter;
import com.freetimr.business.adapter.SkillsAutoCompleteAdapter;
import com.freetimr.business.adapter.SkillsRecyclerViewAdapter;
import com.freetimr.business.adapter.SocialCredsRecyclerViewAdapter;
import com.freetimr.business.models.DisabilityModel;
import com.freetimr.business.models.GraduationModel;
import com.freetimr.business.models.JobDisabilitiesModel;
import com.freetimr.business.models.JobLanguagesModel;
import com.freetimr.business.models.JobMastersModel;
import com.freetimr.business.models.JobSkillsModel;
import com.freetimr.business.models.JobSocialMediasModel;
import com.freetimr.business.models.JobsModel;
import com.freetimr.business.models.LanguageModel;
import com.freetimr.business.models.PostJobRequestModel;
import com.freetimr.business.models.SkillModel;
import com.freetimr.business.utils.BottomSheetListOnItemClickListener;
import com.freetimr.business.utils.FreeTimrUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Created by varunbarve on 16/3/17.
 */

public class PostAJobStepTwoFragments extends Fragment {

    PostJobRequestModel postJobRequestModel;
    TextView startAge, endAge, minQualText, addSkills, addLanguage;
    RadioButton genderMale, genderFemale, genderBoth, disabilityYes, disabilityNo, idProofYes, idProofNo, socialCredsYes, socialCredsNo;
    RecyclerView preferredLanguagesRecyclerView, skillsRequiredRecyclerView, disabilityRecyclerView, socialCredsRecyclerView;
    RelativeLayout minQualificationsContainer;
    Button continueBtn;
    String genderStr = "";
    int ageGroupFrom = -1, ageGroupTo = -1, graduationId = -1;
    boolean idProofRequired, isDisabilityAccepted, socialCredsRequired;
    ArrayList<JobDisabilitiesModel> jobDisabilitiesModels = new ArrayList<>();
    ArrayList<JobSocialMediasModel> jobSocialMediasModels = new ArrayList<>();
    LanguageRecyclerViewAdapter languageRecyclerViewAdapter;
    SkillsRecyclerViewAdapter skillsRecyclerViewAdapter;
    ArrayList<JobLanguagesModel> jobLanguageModelsForRequest = new ArrayList<>();
    ArrayList<JobSkillsModel> jobSkillsModelsForRequest = new ArrayList<>();
    JobsModel model;
    boolean isEdit = false;

    String[] ageArray = new String[]{"18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40"};

    public static PostAJobStepTwoFragments getInstance (PostJobRequestModel postJobRequestModel) {
        PostAJobStepTwoFragments fragment = new PostAJobStepTwoFragments();
        Bundle args = new Bundle();
        args.putParcelable("PostJobRequestModel", postJobRequestModel);
        fragment.setArguments(args);
        return fragment;
    }

    public static PostAJobStepTwoFragments getInstance (PostJobRequestModel postJobRequestModel, JobsModel model, boolean isEdit) {
        PostAJobStepTwoFragments fragment = new PostAJobStepTwoFragments();
        Bundle args = new Bundle();
        args.putParcelable("PostJobRequestModel", postJobRequestModel);
        args.putParcelable("JobsModel", model);
        args.putBoolean("IsEdit", isEdit);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate (@Nullable Bundle savedInstanceState) {
        postJobRequestModel = getArguments().getParcelable("PostJobRequestModel");
        model = getArguments().getParcelable("JobsModel");
        isEdit = getArguments().getBoolean("IsEdit");
        super.onCreate(savedInstanceState);
    }

    private void initDisabilitiesAdapter () {
        DisabilitiesRecyclerViewAdapter adapter = new DisabilitiesRecyclerViewAdapter(PostAJobStepTwoFragments.this.getActivity(), FreeTimrUtils.getDisableList(PostAJobStepTwoFragments.this.getActivity()), PostAJobStepTwoFragments.this);
        disabilityRecyclerView.setAdapter(adapter);
        disabilityRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.HORIZONTAL));
    }

    private void initSocialCredsAdapter () {
        SocialCredsRecyclerViewAdapter adapter = new SocialCredsRecyclerViewAdapter(PostAJobStepTwoFragments.this.getActivity(), FreeTimrUtils.getSocialCredsList(PostAJobStepTwoFragments.this.getActivity()), PostAJobStepTwoFragments.this);
        socialCredsRecyclerView.setAdapter(adapter);
        socialCredsRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.HORIZONTAL));
    }

    public void addDisability (int id) {
        jobDisabilitiesModels.add(new JobDisabilitiesModel(id));
    }

    public void addSocialCreds (int id) {
        jobSocialMediasModels.add(new JobSocialMediasModel(id));
    }

    public void removeDisability (int disabilityid) {
        Iterator<JobDisabilitiesModel> it = jobDisabilitiesModels.iterator();
        while (it.hasNext()) {
            if (it.next().DisabilityId == disabilityid) {
                it.remove();
            }
        }
//        jobDisabilitiesModels.remove(position);
    }

    public void removeSocialCreds (int socialMediaId) {
        Iterator<JobSocialMediasModel> it = jobSocialMediasModels.iterator();
        while (it.hasNext()) {
            if (it.next().SocialMediaId == socialMediaId) {
                it.remove();
            }
        }
//        jobSocialMediasModels.remove(position);
    }

    @Nullable
    @Override
    public View onCreateView (LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_post_job_step_two, container, false);

        startAge = (TextView) rootView.findViewById(R.id.start_age);
        endAge = (TextView) rootView.findViewById(R.id.end_age);
        genderMale = (RadioButton) rootView.findViewById(R.id.gender_male);
        genderFemale = (RadioButton) rootView.findViewById(R.id.gender_female);
        genderBoth = (RadioButton) rootView.findViewById(R.id.gender_both);
        disabilityYes = (RadioButton) rootView.findViewById(R.id.disability_yes);
        disabilityNo = (RadioButton) rootView.findViewById(R.id.disability_no);
        idProofYes = (RadioButton) rootView.findViewById(R.id.id_proof_yes);
        idProofNo = (RadioButton) rootView.findViewById(R.id.id_proof_no);
        socialCredsYes = (RadioButton) rootView.findViewById(R.id.social_creds_yes);
        socialCredsNo = (RadioButton) rootView.findViewById(R.id.social_creds_no);
        preferredLanguagesRecyclerView = (RecyclerView) rootView.findViewById(R.id.preferred_languages_recycler_view);
        skillsRequiredRecyclerView = (RecyclerView) rootView.findViewById(R.id.skills_recycler_view);
        disabilityRecyclerView = (RecyclerView) rootView.findViewById(R.id.disability_recycler_view);
        socialCredsRecyclerView = (RecyclerView) rootView.findViewById(R.id.social_creds_recycler_view);
        minQualificationsContainer = (RelativeLayout) rootView.findViewById(R.id.min_qualifications_container);
        minQualText = (TextView) rootView.findViewById(R.id.min_qual_text);
        addSkills = (TextView) rootView.findViewById(R.id.add_skills);
        addLanguage = (TextView) rootView.findViewById(R.id.add_language);
        continueBtn = (Button) rootView.findViewById(R.id.continue_btn_step_two);

        initDisabilitiesAdapter();
        initSocialCredsAdapter();

        if (isEdit) {
            ageGroupFrom = model.AgeGroupFrom;
            ageGroupTo = model.AgeGroupTo;
            genderStr = model.Gender;
            isDisabilityAccepted = model.IsDisabledAllowed;
            if (isDisabilityAccepted) {
                for (int i = 0; i < model.JobDisability.length; i++) {
                    jobDisabilitiesModels.add(new JobDisabilitiesModel(model.JobDisability[i].DisabilityId));
                }
            }

            socialCredsRequired = model.IsSocialRequired;
            if (socialCredsRequired) {
                for (int i = 0; i < model.JobSocialMedias.length; i++) {
                    jobSocialMediasModels.add(new JobSocialMediasModel(model.JobSocialMedias[i].SocialMediaId));
                }
            }

            graduationId = model.GraduationId;

            if (model.JobLanguages != null) {
                for (int i = 0; i < model.JobLanguages.length; i++) {
                    jobLanguageModelsForRequest.add(new JobLanguagesModel(model.JobLanguages[i].LanguageId));
                }
            }

            if (model.JobSkills != null) {
                for (int i = 0; i < model.JobSkills.length; i++) {
                    jobSkillsModelsForRequest.add(new JobSkillsModel(model.JobSkills[i].SkillId));
                }
            }

            startAge.setText("" + model.AgeGroupFrom);
            endAge.setText("" + model.AgeGroupTo);

            if (model.Gender.equalsIgnoreCase("M")) {
                genderMale.setChecked(true);
            } else if (model.Gender.equalsIgnoreCase("F")) {
                genderFemale.setChecked(true);
            } else if (model.Gender.equalsIgnoreCase("B")) {
                genderBoth.setChecked(true);
            }

            if (model.IsDisabledAllowed) {
                disabilityYes.setChecked(true);
                disabilityNo.setChecked(false);
                if (disabilityRecyclerView.getVisibility() == View.GONE)
                    disabilityRecyclerView.setVisibility(View.VISIBLE);
            } else {
                disabilityYes.setChecked(false);
                disabilityNo.setChecked(true);
                if (disabilityRecyclerView.getVisibility() == View.VISIBLE)
                    disabilityRecyclerView.setVisibility(View.GONE);
            }

            if (model.IsIdProofRequired) {
                idProofYes.setChecked(true);
                idProofNo.setChecked(false);
            } else {
                idProofYes.setChecked(false);
                idProofNo.setChecked(true);
            }

            if (model.IsSocialRequired) {
                socialCredsYes.setChecked(true);
                socialCredsNo.setChecked(false);
                if (socialCredsRecyclerView.getVisibility() == View.GONE)
                    socialCredsRecyclerView.setVisibility(View.VISIBLE);
            } else {
                socialCredsYes.setChecked(false);
                socialCredsNo.setChecked(true);
                if (socialCredsRecyclerView.getVisibility() == View.VISIBLE)
                    socialCredsRecyclerView.setVisibility(View.GONE);
            }

            minQualText.setText(FreeTimrUtils.getGraduationNameById(PostAJobStepTwoFragments.this.getActivity(), model.GraduationId));

            if (!jobSkillsModelsForRequest.isEmpty()) {
                updateSkillsArray(jobSkillsModelsForRequest);
            }

            if (!jobLanguageModelsForRequest.isEmpty()) {
                updateLanguageArray(jobLanguageModelsForRequest);
            }
        }

        addLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                addLanguage();
            }
        });

        addSkills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                setAddSkills();
            }
        });

        startAge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                FreeTimrUtils.showBottomSheetDialog(PostAJobStepTwoFragments.this.getFragmentManager(), "From", ageArray, new BottomSheetListOnItemClickListener() {
                    @Override
                    public void onListItemSelected (int position) {
                        ageGroupFrom = Integer.parseInt(ageArray[position]);
                        startAge.setText(ageArray[position]);
                    }
                });
            }
        });

        endAge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                FreeTimrUtils.showBottomSheetDialog(PostAJobStepTwoFragments.this.getFragmentManager(), "To", ageArray, new BottomSheetListOnItemClickListener() {
                    @Override
                    public void onListItemSelected (int position) {
                        ageGroupTo = Integer.parseInt(ageArray[position]);
                        endAge.setText(ageArray[position]);
                    }
                });
            }
        });

        genderMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged (CompoundButton buttonView, boolean isChecked) {
                genderStr = "M";
            }
        });

        genderFemale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged (CompoundButton buttonView, boolean isChecked) {
                genderStr = "F";
            }
        });

        genderBoth.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged (CompoundButton buttonView, boolean isChecked) {
                genderStr = "B";
            }
        });

        disabilityYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                if (disabilityRecyclerView.getVisibility() == View.GONE)
                    disabilityRecyclerView.setVisibility(View.VISIBLE);
                isDisabilityAccepted = true;
            }
        });

        disabilityNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                if (disabilityRecyclerView.getVisibility() == View.VISIBLE)
                    disabilityRecyclerView.setVisibility(View.GONE);
                isDisabilityAccepted = false;
            }
        });

        idProofYes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged (CompoundButton buttonView, boolean isChecked) {
                idProofRequired = true;
            }
        });

        idProofNo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged (CompoundButton buttonView, boolean isChecked) {
                idProofRequired = false;
            }
        });

        socialCredsYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                if (socialCredsRecyclerView.getVisibility() == View.GONE)
                    socialCredsRecyclerView.setVisibility(View.VISIBLE);
                socialCredsRequired = true;
            }
        });

        socialCredsNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                if (socialCredsRecyclerView.getVisibility() == View.VISIBLE)
                    socialCredsRecyclerView.setVisibility(View.GONE);
                socialCredsRequired = false;
            }
        });

        minQualificationsContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                JobMastersModel jobMastersModel = FreeTimrUtils.getJobMasterModel(PostAJobStepTwoFragments.this.getActivity());
                final GraduationModel[] graduationModels = jobMastersModel.Graduation;

                final String[] strings = new String[graduationModels.length];
                for (int i = 0; i < graduationModels.length; i++) {
                    strings[i] = graduationModels[i].GraduationName;
                }

                FreeTimrUtils.showBottomSheetDialog(PostAJobStepTwoFragments.this.getFragmentManager(), "Min. Qualification", strings, new BottomSheetListOnItemClickListener() {
                    @Override
                    public void onListItemSelected (int position) {
                        graduationId = graduationModels[position].GraduationId;
                        minQualText.setText(graduationModels[position].GraduationName);
                    }
                });
            }
        });

        // PROCEED
        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                if (ageGroupFrom == -1 || ageGroupTo == -1) {
                    FreeTimrUtils.showMessage(PostAJobStepTwoFragments.this.getActivity(), "Please select age group");
                    return;
                }

                if (genderStr.isEmpty()) {
                    FreeTimrUtils.showMessage(PostAJobStepTwoFragments.this.getActivity(), "Please select gender");
                    return;
                }

                if (isDisabilityAccepted) {
                    if (jobDisabilitiesModels.size() == 0) {
                        FreeTimrUtils.showMessage(PostAJobStepTwoFragments.this.getActivity(), "Please select disabilities");
                        return;
                    }
                }

                if (socialCredsRequired) {
                    if (jobSocialMediasModels.size() == 0) {
                        FreeTimrUtils.showMessage(PostAJobStepTwoFragments.this.getActivity(), "Please select social medias");
                        return;
                    }
                }

                if (graduationId == -1) {
                    FreeTimrUtils.showMessage(PostAJobStepTwoFragments.this.getActivity(), "Please add min. qualification");
                    return;
                }

                if (jobLanguageModelsForRequest.size() == 0) {
                    FreeTimrUtils.showMessage(PostAJobStepTwoFragments.this.getActivity(), "Please add preferred languages");
                    return;
                }

                if (jobSkillsModelsForRequest.size() == 0) {
                    FreeTimrUtils.showMessage(PostAJobStepTwoFragments.this.getActivity(), "Please add skills");
                    return;
                }

                // EVERYTHING VALIDATED
                postJobRequestModel.AgeGroupFrom = ageGroupFrom;
                postJobRequestModel.AgeGroupTo = ageGroupTo;
                postJobRequestModel.Gender = genderStr;
                postJobRequestModel.AgeGroupFrom = ageGroupFrom;

                postJobRequestModel.IsDisabledAllowed = isDisabilityAccepted;
                if (isDisabilityAccepted) {
                    postJobRequestModel.JobDisability = new JobDisabilitiesModel[jobDisabilitiesModels.size()];
                    postJobRequestModel.JobDisability = jobDisabilitiesModels.toArray(postJobRequestModel.JobDisability);
                } else {
                    postJobRequestModel.JobDisability = new JobDisabilitiesModel[]{};
                }

                postJobRequestModel.IsSocialRequired = socialCredsRequired;
                if (socialCredsRequired) {
                    postJobRequestModel.JobSocialMedias = new JobSocialMediasModel[jobSocialMediasModels.size()];
                    postJobRequestModel.JobSocialMedias = jobSocialMediasModels.toArray(postJobRequestModel.JobSocialMedias);
                }

                postJobRequestModel.IsIdProofRequired = idProofRequired;
                postJobRequestModel.GraduationId = graduationId;
                postJobRequestModel.IsIdProofRequired = idProofRequired;

                postJobRequestModel.JobLanguages = new JobLanguagesModel[jobLanguageModelsForRequest.size()];
                postJobRequestModel.JobLanguages = jobLanguageModelsForRequest.toArray(postJobRequestModel.JobLanguages);

                postJobRequestModel.JobSkills = new JobSkillsModel[jobSkillsModelsForRequest.size()];
                postJobRequestModel.JobSkills = jobSkillsModelsForRequest.toArray(postJobRequestModel.JobSkills);

                FreeTimrUtils.log(FreeTimrUtils.newGson().toJson(postJobRequestModel));

                if (isEdit) {
                    addFragment(PostAJobStepThreeFragments.getInstance(postJobRequestModel, model, isEdit));
                } else {
                    addFragment(PostAJobStepThreeFragments.getInstance(postJobRequestModel));
                }
            }
        });

        return rootView;
    }

    private void addLanguage () {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View alertLayout = inflater.inflate(R.layout.language_dialog, null);
        final AutoCompleteTextView languageAutoCompleteTextView = (AutoCompleteTextView) alertLayout.findViewById(R.id.language_autocomplete);

        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Montserrat-SemiBold.otf");
        languageAutoCompleteTextView.setTypeface(tf);

        final LanguageAutoCompleteAdapter adapter = new LanguageAutoCompleteAdapter(PostAJobStepTwoFragments.this.getActivity(), FreeTimrUtils.getLanguageList(PostAJobStepTwoFragments.this.getActivity()));

        languageAutoCompleteTextView.setAdapter(adapter);
        languageAutoCompleteTextView.setThreshold(1);
        languageAutoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged (CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged (CharSequence s, int start, int before, int count) {
                languageAutoCompleteTextView.showDropDown();
                adapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged (Editable s) {

            }
        });

        languageAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick (AdapterView<?> adapterView, View view, int i, long l) {
                languageAutoCompleteTextView.setText(adapter.getItem(i).LanguageName);
                jobLanguageModelsForRequest.add(new JobLanguagesModel(adapter.getItem(i).LanguageId));
            }
        });

        AlertDialog.Builder alert = new AlertDialog.Builder(PostAJobStepTwoFragments.this.getActivity());
        alert.setView(alertLayout);
        alert.setCancelable(false);
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick (DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alert.setPositiveButton("Add",null);
        AlertDialog dialog = alert.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow (final DialogInterface dialog) {

                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick (View view) {
                        // TODO Do something

                        if (jobLanguageModelsForRequest.size() == 0) {
                            FreeTimrUtils.showMessage(PostAJobStepTwoFragments.this.getActivity(), "Please add a language");
                        } else {
                            updateLanguageArray(jobLanguageModelsForRequest);
                            dialog.dismiss();
                        }
                    }
                });
            }
        });
        dialog.show();
    }

    private void setAddSkills () {
        final int[] skillId = {0};
        final String[] skillName = {""};

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View alertLayout = inflater.inflate(R.layout.skills_dialog, null);
        final AutoCompleteTextView skillAutoCompleteTextView = (AutoCompleteTextView) alertLayout.findViewById(R.id.skills_autocomplete);
        final EditText expMonths = (EditText) alertLayout.findViewById(R.id.exp_months);
        final EditText expYears = (EditText) alertLayout.findViewById(R.id.exp_years);

        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Montserrat-SemiBold.otf");
        skillAutoCompleteTextView.setTypeface(tf);

        final SkillsAutoCompleteAdapter adapter = new SkillsAutoCompleteAdapter(PostAJobStepTwoFragments.this.getActivity(), FreeTimrUtils.getSkillsList(PostAJobStepTwoFragments.this.getActivity()));

        skillAutoCompleteTextView.setAdapter(adapter);
        skillAutoCompleteTextView.setThreshold(1);
        skillAutoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged (CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged (CharSequence s, int start, int before, int count) {
                skillAutoCompleteTextView.showDropDown();
                adapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged (Editable s) {

            }
        });

        skillAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick (AdapterView<?> adapterView, View view, int i, long l) {
                skillId[0] = adapter.getItem(i).SkillId;
                skillName[0] = adapter.getItem(i).SkillName;
                skillAutoCompleteTextView.setText(skillName[0]);
            }
        });

        AlertDialog.Builder alert = new AlertDialog.Builder(PostAJobStepTwoFragments.this.getActivity());
        alert.setView(alertLayout);
        alert.setCancelable(true);
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick (DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alert.setPositiveButton("Add", new DialogInterface.OnClickListener() {

            @Override
            public void onClick (DialogInterface dialog, int which) {

                if (skillId[0] == 0) {
                    FreeTimrUtils.showMessage(PostAJobStepTwoFragments.this.getActivity(), "Please add a skill");
                } else {
                    String expMonthStr = expMonths.getText().toString();
                    String expYearStr = expYears.getText().toString();

                    jobSkillsModelsForRequest.add(new JobSkillsModel(skillId[0], expYearStr.isEmpty() ? 0 : Integer.parseInt(expYearStr), expMonthStr.isEmpty() ? 0 : Integer.parseInt(expMonthStr)));
                    updateSkillsArray(jobSkillsModelsForRequest);
                }
            }
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }

    private void updateLanguageArray (ArrayList<JobLanguagesModel> list) {
        languageRecyclerViewAdapter = new LanguageRecyclerViewAdapter(PostAJobStepTwoFragments.this.getActivity(), list);
        preferredLanguagesRecyclerView.setAdapter(languageRecyclerViewAdapter);
        preferredLanguagesRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.HORIZONTAL));
    }

    private void updateSkillsArray (ArrayList<JobSkillsModel> list) {
        skillsRecyclerViewAdapter = new SkillsRecyclerViewAdapter(PostAJobStepTwoFragments.this.getActivity(), list);
        skillsRequiredRecyclerView.setAdapter(skillsRecyclerViewAdapter);
        skillsRequiredRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.HORIZONTAL));
    }

    public void addFragment (Fragment fragment) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.commit();
    }
}
