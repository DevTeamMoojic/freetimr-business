package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 21-Mar-17.
 */

public class AvailabilityTimeModel implements Parcelable {

    public int AvailabilityTimeId;
    public String AvailabilityTimeName;

    protected AvailabilityTimeModel(Parcel in) {
        AvailabilityTimeId = in.readInt();
        AvailabilityTimeName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(AvailabilityTimeId);
        dest.writeString(AvailabilityTimeName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AvailabilityTimeModel> CREATOR = new Creator<AvailabilityTimeModel>() {
        @Override
        public AvailabilityTimeModel createFromParcel(Parcel in) {
            return new AvailabilityTimeModel(in);
        }

        @Override
        public AvailabilityTimeModel[] newArray(int size) {
            return new AvailabilityTimeModel[size];
        }
    };
}
