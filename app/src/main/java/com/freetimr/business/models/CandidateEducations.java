package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 17-Feb-17.
 */

public class CandidateEducations implements Parcelable {

    public int GraduationId;

    public String GraduationName;
    public String EducationMode;
    public int SpecializationId;
    public String SpecializationName;
    public String InstituteName;
    public int GraduatedYear;
    public int GradingSystem;
    public String GradingSystemName;
    public String Marks;

    public CandidateEducations() {
    }

    protected CandidateEducations(Parcel in) {
        GraduationId = in.readInt();
        EducationMode = in.readString();
        SpecializationId = in.readInt();
        InstituteName = in.readString();
        GraduatedYear = in.readInt();
        GradingSystem = in.readInt();
        Marks = in.readString();
        GraduationName = in.readString();
        SpecializationName = in.readString();
        GradingSystemName = in.readString();
    }

    public static final Creator<CandidateEducations> CREATOR = new Creator<CandidateEducations>() {
        @Override
        public CandidateEducations createFromParcel(Parcel in) {
            return new CandidateEducations(in);
        }

        @Override
        public CandidateEducations[] newArray(int size) {
            return new CandidateEducations[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(GraduationId);
        parcel.writeString(EducationMode);
        parcel.writeInt(SpecializationId);
        parcel.writeString(InstituteName);
        parcel.writeInt(GraduatedYear);
        parcel.writeInt(GradingSystem);
        parcel.writeString(Marks);
        parcel.writeString(GraduationName);
        parcel.writeString(SpecializationName);
        parcel.writeString(GradingSystemName);
    }
}
