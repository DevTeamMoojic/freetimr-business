package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 17-Feb-17.
 */

public class CandidateExperiences implements Parcelable {

    public String EmployerName;
    public boolean IsCurrentEmployer;
    public String DurationFromMonth;
    public int DurationFromYear;
    public String DurationToMonth;
    public int DurationToYear;
    public String Designation;
    public String JobProfile;
    public String Location;

    public CandidateExperiences() {
    }

    protected CandidateExperiences(Parcel in) {
        EmployerName = in.readString();
        IsCurrentEmployer = in.readByte() != 0;
        DurationFromMonth = in.readString();
        DurationFromYear = in.readInt();
        DurationToMonth = in.readString();
        DurationToYear = in.readInt();
        Designation = in.readString();
        JobProfile = in.readString();
        Location = in.readString();
    }

    public static final Creator<CandidateExperiences> CREATOR = new Creator<CandidateExperiences>() {
        @Override
        public CandidateExperiences createFromParcel(Parcel in) {
            return new CandidateExperiences(in);
        }

        @Override
        public CandidateExperiences[] newArray(int size) {
            return new CandidateExperiences[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(EmployerName);
        parcel.writeByte((byte) (IsCurrentEmployer ? 1 : 0));
        parcel.writeString(DurationFromMonth);
        parcel.writeInt(DurationFromYear);
        parcel.writeString(DurationToMonth);
        parcel.writeInt(DurationToYear);
        parcel.writeString(Designation);
        parcel.writeString(JobProfile);
        parcel.writeString(Location);
    }
}
