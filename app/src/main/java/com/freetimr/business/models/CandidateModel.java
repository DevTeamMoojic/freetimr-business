package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by Admin on 24-Mar-17.
 */

public class CandidateModel implements Parcelable {

    public String CandidateId;
    public String CandidateName;
    public String Gender;
    public String EmailId;
    public String MobileNo;
    public boolean IsVerified;
    public String CandidateImage;
    public Date DateOfBirth;
    public Date LastLoginDate;
    public Date DeactivatedDate;
    public Date CreatedDate;

    protected CandidateModel(Parcel in) {
        CandidateId = in.readString();
        CandidateName = in.readString();
        Gender = in.readString();
        EmailId = in.readString();
        MobileNo = in.readString();
        IsVerified = in.readByte() != 0;
        CandidateImage = in.readString();
        DateOfBirth = (Date) in.readSerializable();
        LastLoginDate = (Date) in.readSerializable();
        DeactivatedDate = (Date) in.readSerializable();
        CreatedDate = (Date) in.readSerializable();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(CandidateId);
        dest.writeString(CandidateName);
        dest.writeString(Gender);
        dest.writeString(EmailId);
        dest.writeString(MobileNo);
        dest.writeByte((byte) (IsVerified ? 1 : 0));
        dest.writeString(CandidateImage);
        dest.writeSerializable(DateOfBirth);
        dest.writeSerializable(LastLoginDate);
        dest.writeSerializable(DeactivatedDate);
        dest.writeSerializable(CreatedDate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CandidateModel> CREATOR = new Creator<CandidateModel>() {
        @Override
        public CandidateModel createFromParcel(Parcel in) {
            return new CandidateModel(in);
        }

        @Override
        public CandidateModel[] newArray(int size) {
            return new CandidateModel[size];
        }
    };
}
