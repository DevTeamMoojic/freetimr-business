package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by varunbarve on 02/08/17.
 */

public class CandidatePreferredCityModel implements Parcelable{

    public int CandidatePreferredCityId;
    public CityModel City;

    public CandidatePreferredCityModel (){}

    protected CandidatePreferredCityModel (Parcel in) {
        CandidatePreferredCityId = in.readInt();
        City = in.readParcelable(CityModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel (Parcel dest, int flags) {
        dest.writeInt(CandidatePreferredCityId);
        dest.writeParcelable(City, flags);
    }

    @Override
    public int describeContents () {
        return 0;
    }

    public static final Creator<CandidatePreferredCityModel> CREATOR = new Creator<CandidatePreferredCityModel>() {
        @Override
        public CandidatePreferredCityModel createFromParcel (Parcel in) {
            return new CandidatePreferredCityModel(in);
        }

        @Override
        public CandidatePreferredCityModel[] newArray (int size) {
            return new CandidatePreferredCityModel[size];
        }
    };
}
