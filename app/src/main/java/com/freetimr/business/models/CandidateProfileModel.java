package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by Admin on 07-Apr-17.
 */

public class CandidateProfileModel implements Parcelable {

    public String CandidateProfileId;
    public String CandidateId;
    public Date DateOfBirth;
    public String ProfileHeadline;
    public String Location;
    public String Pincode;
    public int OccupationId;
    public int ExperienceYears;
    public int ExperienceMonths;
    public int WorkLocationId;
    public Date CreatedDate;
    public Date UpdatedDate;
    public CandidateAvailabilities[] CandidateAvailability;
    public CandidateEducations[] CandidateEducation;
    public CandidateExperiences[] CandidateExperience;
    public CandidateSkills[] CandidateSkill;
    public CandidatePreferredCityModel[] CandidatePreferredCity;
    public CandidateSkillRoleModel[] CandidateSkillRole;
    public CandidateModel Candidate;

    protected CandidateProfileModel(Parcel in) {
        CandidateProfileId = in.readString();
        CandidateId = in.readString();
        ProfileHeadline = in.readString();
        Location = in.readString();
        Pincode = in.readString();
        OccupationId = in.readInt();
        ExperienceYears = in.readInt();
        ExperienceMonths = in.readInt();
        WorkLocationId = in.readInt();
        CandidateAvailability = in.createTypedArray(CandidateAvailabilities.CREATOR);
        CandidateEducation = in.createTypedArray(CandidateEducations.CREATOR);
        CandidateExperience = in.createTypedArray(CandidateExperiences.CREATOR);
        CandidateSkill = in.createTypedArray(CandidateSkills.CREATOR);
        CandidatePreferredCity = in.createTypedArray(CandidatePreferredCityModel.CREATOR);
        CandidateSkillRole = in.createTypedArray(CandidateSkillRoleModel.CREATOR);
        Candidate = in.readParcelable(CandidateModel.class.getClassLoader());
        DateOfBirth = (java.util.Date) in.readSerializable();
        CreatedDate = (java.util.Date) in.readSerializable();
        UpdatedDate = (java.util.Date) in.readSerializable();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(CandidateProfileId);
        dest.writeString(CandidateId);
        dest.writeString(ProfileHeadline);
        dest.writeString(Location);
        dest.writeString(Pincode);
        dest.writeInt(OccupationId);
        dest.writeInt(ExperienceYears);
        dest.writeInt(ExperienceMonths);
        dest.writeInt(WorkLocationId);
        dest.writeTypedArray(CandidateAvailability, flags);
        dest.writeTypedArray(CandidateEducation, flags);
        dest.writeTypedArray(CandidateExperience, flags);
        dest.writeTypedArray(CandidateSkill, flags);
        dest.writeParcelable(Candidate, flags);
        dest.writeSerializable(DateOfBirth);
        dest.writeSerializable(CreatedDate);
        dest.writeSerializable(UpdatedDate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CandidateProfileModel> CREATOR = new Creator<CandidateProfileModel>() {
        @Override
        public CandidateProfileModel createFromParcel(Parcel in) {
            return new CandidateProfileModel(in);
        }

        @Override
        public CandidateProfileModel[] newArray(int size) {
            return new CandidateProfileModel[size];
        }
    };
}
