package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 07-Apr-17.
 */

public class CandidateSearchRequestModel implements Parcelable {

    public String AvailabilityTimeId;
    public String WorkLocationId;
    public String KeywordTosearch;
    public String Gender;
    public String Location;

    public CandidateSearchRequestModel() {
    }

    protected CandidateSearchRequestModel(Parcel in) {
        AvailabilityTimeId = in.readString();
        WorkLocationId = in.readString();
        KeywordTosearch = in.readString();
        Gender = in.readString();
        Location = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(AvailabilityTimeId);
        dest.writeString(WorkLocationId);
        dest.writeString(KeywordTosearch);
        dest.writeString(Gender);
        dest.writeString(Location);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CandidateSearchRequestModel> CREATOR = new Creator<CandidateSearchRequestModel>() {
        @Override
        public CandidateSearchRequestModel createFromParcel(Parcel in) {
            return new CandidateSearchRequestModel(in);
        }

        @Override
        public CandidateSearchRequestModel[] newArray(int size) {
            return new CandidateSearchRequestModel[size];
        }
    };
}
