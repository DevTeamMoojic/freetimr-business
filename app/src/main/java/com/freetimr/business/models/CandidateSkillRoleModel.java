package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by varunbarve on 02/08/17.
 */

public class CandidateSkillRoleModel implements Parcelable{
    protected CandidateSkillRoleModel (Parcel in) {
    }

    @Override
    public void writeToParcel (Parcel dest, int flags) {
    }

    @Override
    public int describeContents () {
        return 0;
    }

    public static final Creator<CandidateSkillRoleModel> CREATOR = new Creator<CandidateSkillRoleModel>() {
        @Override
        public CandidateSkillRoleModel createFromParcel (Parcel in) {
            return new CandidateSkillRoleModel(in);
        }

        @Override
        public CandidateSkillRoleModel[] newArray (int size) {
            return new CandidateSkillRoleModel[size];
        }
    };
}
