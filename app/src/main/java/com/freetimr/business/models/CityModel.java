package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 21-Mar-17.
 */

public class CityModel implements Parcelable{

    public int CityId;
    public String CityName;
    public String CityImage;
    public String CompanyImageUrl;
    public boolean isSelected;
    public int CandidatePreferredCityId;

    public CityModel(){}


    protected CityModel (Parcel in) {
        CityId = in.readInt();
        CityName = in.readString();
        CityImage = in.readString();
        CompanyImageUrl = in.readString();
        isSelected = in.readByte() != 0;
        CandidatePreferredCityId = in.readInt();
    }

    @Override
    public void writeToParcel (Parcel dest, int flags) {
        dest.writeInt(CityId);
        dest.writeString(CityName);
        dest.writeString(CityImage);
        dest.writeString(CompanyImageUrl);
        dest.writeByte((byte) (isSelected ? 1 : 0));
        dest.writeInt(CandidatePreferredCityId);
    }

    @Override
    public int describeContents () {
        return 0;
    }

    public static final Creator<CityModel> CREATOR = new Creator<CityModel>() {
        @Override
        public CityModel createFromParcel (Parcel in) {
            return new CityModel(in);
        }

        @Override
        public CityModel[] newArray (int size) {
            return new CityModel[size];
        }
    };

    @Override
    public String toString () {
        return CityName;
    }
}