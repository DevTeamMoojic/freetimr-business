package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by varunbarve on 24/09/17.
 */

public class CloseJobModel implements Parcelable {

    public String JobId;
    public String EmployeeId;

    public CloseJobModel (String jobId, String employeeId) {
        JobId = jobId;
        EmployeeId = employeeId;
    }

    protected CloseJobModel (Parcel in) {
        JobId = in.readString();
        EmployeeId = in.readString();
    }

    @Override
    public void writeToParcel (Parcel dest, int flags) {
        dest.writeString(JobId);
        dest.writeString(EmployeeId);
    }

    @Override
    public int describeContents () {
        return 0;
    }

    public static final Creator<CloseJobModel> CREATOR = new Creator<CloseJobModel>() {
        @Override
        public CloseJobModel createFromParcel (Parcel in) {
            return new CloseJobModel(in);
        }

        @Override
        public CloseJobModel[] newArray (int size) {
            return new CloseJobModel[size];
        }
    };
}
