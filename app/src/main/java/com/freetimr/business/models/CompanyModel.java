package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by varunbarve on 14/3/17.
 */

public class CompanyModel implements Parcelable {

    public String CompanyId;
    public String CompanyName;
    public int CityId;
    public String Website;
    public String CompanyDescription;
    public String ReferalCode;
    public String CompanyImage;
    public String CompanyImageUrl;
    public String CreatedDate;
    public String UpdatedDate;
    public EmployeeModel[] Employee;

    public CompanyModel () {
    }

    protected CompanyModel (Parcel in) {
        CompanyId = in.readString();
        CompanyName = in.readString();
        CityId = in.readInt();
        Website = in.readString();
        CompanyDescription = in.readString();
        ReferalCode = in.readString();
        CompanyImage = in.readString();
        CompanyImageUrl = in.readString();
        CreatedDate = in.readString();
        UpdatedDate = in.readString();
        Employee = in.createTypedArray(EmployeeModel.CREATOR);
    }

    @Override
    public void writeToParcel (Parcel dest, int flags) {
        dest.writeString(CompanyId);
        dest.writeString(CompanyName);
        dest.writeInt(CityId);
        dest.writeString(Website);
        dest.writeString(CompanyDescription);
        dest.writeString(ReferalCode);
        dest.writeString(CompanyImage);
        dest.writeString(CompanyImageUrl);
        dest.writeString(CreatedDate);
        dest.writeString(UpdatedDate);
        dest.writeTypedArray(Employee, flags);
    }

    @Override
    public int describeContents () {
        return 0;
    }

    public static final Creator<CompanyModel> CREATOR = new Creator<CompanyModel>() {
        @Override
        public CompanyModel createFromParcel (Parcel in) {
            return new CompanyModel(in);
        }

        @Override
        public CompanyModel[] newArray (int size) {
            return new CompanyModel[size];
        }
    };
}
