package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 21-Mar-17.
 */

public class DisabilityModel implements Parcelable {

    public int DisabilityId;
    public String DisabilityName;

    public DisabilityModel (int disabilityId, String disabilityName) {
        DisabilityId = disabilityId;
        DisabilityName = disabilityName;
    }

    protected DisabilityModel(Parcel in) {
        DisabilityId = in.readInt();
        DisabilityName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(DisabilityId);
        dest.writeString(DisabilityName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DisabilityModel> CREATOR = new Creator<DisabilityModel>() {
        @Override
        public DisabilityModel createFromParcel(Parcel in) {
            return new DisabilityModel(in);
        }

        @Override
        public DisabilityModel[] newArray(int size) {
            return new DisabilityModel[size];
        }
    };
}
