package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by varunbarve on 14/3/17.
 */

public class EmployeeModel implements Parcelable{

    public String EmployeeId;
    public String EmployeeName;
    public String CompanyId;
    public String EmailId;
    public String Passcode;
    public String MobileNumber;
    public boolean IsMasterAdmin;
    public String LastLoginDate;
    public String CreatedDate;
    public String UpdatedDate;

    public EmployeeModel(){}

    protected EmployeeModel (Parcel in) {
        EmployeeId = in.readString();
        EmployeeName = in.readString();
        CompanyId = in.readString();
        EmailId = in.readString();
        Passcode = in.readString();
        MobileNumber = in.readString();
        IsMasterAdmin = in.readByte() != 0;
        LastLoginDate = in.readString();
        CreatedDate = in.readString();
        UpdatedDate = in.readString();
    }

    @Override
    public void writeToParcel (Parcel dest, int flags) {
        dest.writeString(EmployeeId);
        dest.writeString(EmployeeName);
        dest.writeString(CompanyId);
        dest.writeString(EmailId);
        dest.writeString(Passcode);
        dest.writeString(MobileNumber);
        dest.writeByte((byte) (IsMasterAdmin ? 1 : 0));
        dest.writeString(LastLoginDate);
        dest.writeString(CreatedDate);
        dest.writeString(UpdatedDate);
    }

    @Override
    public int describeContents () {
        return 0;
    }

    public static final Creator<EmployeeModel> CREATOR = new Creator<EmployeeModel>() {
        @Override
        public EmployeeModel createFromParcel (Parcel in) {
            return new EmployeeModel(in);
        }

        @Override
        public EmployeeModel[] newArray (int size) {
            return new EmployeeModel[size];
        }
    };
}
