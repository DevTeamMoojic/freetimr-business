package com.freetimr.business.models;

/**
 * Created by Admin on 23-Mar-17.
 */

public class HomeModel {

    String id;
    String name;
    int background;
    int logo;
    int itemType;

    public HomeModel(String id, String name, int logo, int background, int itemType) {
        this.id = id;
        this.name = name;
        this.background = background;
        this.logo = logo;
        this.itemType = itemType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBackground() {
        return background;
    }

    public void setBackground(int background) {
        this.background = background;
    }

    public int getLogo() {
        return logo;
    }

    public void setLogo(int logo) {
        this.logo = logo;
    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }
}

