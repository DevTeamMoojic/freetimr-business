package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 21-Mar-17.
 */

public class JobActivityModel implements Parcelable {

    public int JobActivityId;
    public String JobActivityName;

    protected JobActivityModel(Parcel in) {
        JobActivityId = in.readInt();
        JobActivityName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(JobActivityId);
        dest.writeString(JobActivityName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<JobActivityModel> CREATOR = new Creator<JobActivityModel>() {
        @Override
        public JobActivityModel createFromParcel(Parcel in) {
            return new JobActivityModel(in);
        }

        @Override
        public JobActivityModel[] newArray(int size) {
            return new JobActivityModel[size];
        }
    };
}
