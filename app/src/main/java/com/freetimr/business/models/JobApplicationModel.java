package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by Admin on 24-Mar-17.
 */

public class JobApplicationModel implements Parcelable {

    public int JobApplicationId;
    public String JobId;
    public String CandidateId;
    public int JobApplicationStatusId;
    public Date StatusDate;
    public JobApplicationStatusModel JobApplicationStatus;
    public CandidateModel Candidate;

    protected JobApplicationModel(Parcel in) {
        JobApplicationId = in.readInt();
        JobId = in.readString();
        CandidateId = in.readString();
        JobApplicationStatusId = in.readInt();
        JobApplicationStatus = in.readParcelable(JobApplicationStatusModel.class.getClassLoader());
        Candidate = in.readParcelable(CandidateModel.class.getClassLoader());
        StatusDate = (Date) in.readSerializable();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(JobApplicationId);
        dest.writeString(JobId);
        dest.writeString(CandidateId);
        dest.writeInt(JobApplicationStatusId);
        dest.writeParcelable(JobApplicationStatus, flags);
        dest.writeParcelable(Candidate, flags);
        dest.writeSerializable(StatusDate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<JobApplicationModel> CREATOR = new Creator<JobApplicationModel>() {
        @Override
        public JobApplicationModel createFromParcel(Parcel in) {
            return new JobApplicationModel(in);
        }

        @Override
        public JobApplicationModel[] newArray(int size) {
            return new JobApplicationModel[size];
        }
    };
}
