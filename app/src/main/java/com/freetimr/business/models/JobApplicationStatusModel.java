package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 24-Mar-17.
 */

public class JobApplicationStatusModel implements Parcelable {

    public int JobApplicationStatusId;
    public String JobApplicationStatusName;

    public JobApplicationStatusModel(int jobApplicationStatusId, String jobApplicationStatusName) {
        JobApplicationStatusId = jobApplicationStatusId;
        JobApplicationStatusName = jobApplicationStatusName;
    }

    protected JobApplicationStatusModel(Parcel in) {
        JobApplicationStatusId = in.readInt();
        JobApplicationStatusName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(JobApplicationStatusId);
        dest.writeString(JobApplicationStatusName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<JobApplicationStatusModel> CREATOR = new Creator<JobApplicationStatusModel>() {
        @Override
        public JobApplicationStatusModel createFromParcel(Parcel in) {
            return new JobApplicationStatusModel(in);
        }

        @Override
        public JobApplicationStatusModel[] newArray(int size) {
            return new JobApplicationStatusModel[size];
        }
    };
}
