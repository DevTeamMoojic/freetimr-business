package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by varunbarve on 23/3/17.
 */

public class JobDisabilitiesModel implements Parcelable {

    public int DisabilityId;
    public String CreatedBy;
    public String UpdatedBy;

    public JobDisabilitiesModel (int disabilityId) {
        DisabilityId = disabilityId;
    }

    protected JobDisabilitiesModel (Parcel in) {
        DisabilityId = in.readInt();
        CreatedBy = in.readString();
        UpdatedBy = in.readString();
    }

    @Override
    public void writeToParcel (Parcel dest, int flags) {
        dest.writeInt(DisabilityId);
        dest.writeString(CreatedBy);
        dest.writeString(UpdatedBy);
    }

    @Override
    public int describeContents () {
        return 0;
    }

    public static final Creator<JobDisabilitiesModel> CREATOR = new Creator<JobDisabilitiesModel>() {
        @Override
        public JobDisabilitiesModel createFromParcel (Parcel in) {
            return new JobDisabilitiesModel(in);
        }

        @Override
        public JobDisabilitiesModel[] newArray (int size) {
            return new JobDisabilitiesModel[size];
        }
    };
}
