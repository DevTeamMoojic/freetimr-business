package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 23-Mar-17.
 */

public class JobDisabilityModel implements Parcelable {

    public int JobDisabilityId;
    public String JobId;
    public int DisabilityId;
    public String CreatedBy;
    public String UpdatedBy;

    protected JobDisabilityModel (Parcel in) {
        JobDisabilityId = in.readInt();
        JobId = in.readString();
        DisabilityId = in.readInt();
        CreatedBy = in.readString();
        UpdatedBy = in.readString();
    }

    @Override
    public void writeToParcel (Parcel dest, int flags) {
        dest.writeInt(JobDisabilityId);
        dest.writeString(JobId);
        dest.writeInt(DisabilityId);
        dest.writeString(CreatedBy);
        dest.writeString(UpdatedBy);
    }

    @Override
    public int describeContents () {
        return 0;
    }

    public static final Creator<JobDisabilityModel> CREATOR = new Creator<JobDisabilityModel>() {
        @Override
        public JobDisabilityModel createFromParcel (Parcel in) {
            return new JobDisabilityModel(in);
        }

        @Override
        public JobDisabilityModel[] newArray (int size) {
            return new JobDisabilityModel[size];
        }
    };
}
