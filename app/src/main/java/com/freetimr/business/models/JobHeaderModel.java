package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 21-Mar-17.
 */

public class JobHeaderModel implements Parcelable {

    public int JobHeaderId;
    public String JobHeaderName;

    protected JobHeaderModel(Parcel in) {
        JobHeaderId = in.readInt();
        JobHeaderName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(JobHeaderId);
        dest.writeString(JobHeaderName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<JobHeaderModel> CREATOR = new Creator<JobHeaderModel>() {
        @Override
        public JobHeaderModel createFromParcel(Parcel in) {
            return new JobHeaderModel(in);
        }

        @Override
        public JobHeaderModel[] newArray(int size) {
            return new JobHeaderModel[size];
        }
    };
}
