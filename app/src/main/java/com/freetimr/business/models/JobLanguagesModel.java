package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by varunbarve on 23/3/17.
 */

public class JobLanguagesModel implements Parcelable {

    public int LanguageId;
    public String CreatedBy;
    public String UpdatedBy;

    public JobLanguagesModel (int languageId) {
        LanguageId = languageId;
    }

    protected JobLanguagesModel (Parcel in) {
        LanguageId = in.readInt();
        CreatedBy = in.readString();
        UpdatedBy = in.readString();
    }

    @Override
    public void writeToParcel (Parcel dest, int flags) {
        dest.writeInt(LanguageId);
        dest.writeString(CreatedBy);
        dest.writeString(UpdatedBy);
    }

    @Override
    public int describeContents () {
        return 0;
    }

    public static final Creator<JobLanguagesModel> CREATOR = new Creator<JobLanguagesModel>() {
        @Override
        public JobLanguagesModel createFromParcel (Parcel in) {
            return new JobLanguagesModel(in);
        }

        @Override
        public JobLanguagesModel[] newArray (int size) {
            return new JobLanguagesModel[size];
        }
    };
}
