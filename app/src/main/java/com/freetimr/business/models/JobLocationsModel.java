package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 22-Mar-17.
 */

public class JobLocationsModel implements Parcelable {

    public int CityId;
    public String CityName;
    public String LocationName;

    public JobLocationsModel(int cityId, String locationName, String cityName) {
        CityId = cityId;
        LocationName = locationName;
        CityName = cityName;
    }

    protected JobLocationsModel(Parcel in) {
        CityId = in.readInt();
        LocationName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(CityId);
        dest.writeString(LocationName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<JobLocationsModel> CREATOR = new Creator<JobLocationsModel>() {
        @Override
        public JobLocationsModel createFromParcel(Parcel in) {
            return new JobLocationsModel(in);
        }

        @Override
        public JobLocationsModel[] newArray(int size) {
            return new JobLocationsModel[size];
        }
    };
}
