package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 23-Mar-17.
 */

public class JobSkillModel implements Parcelable{

    public int JobSkillId;
    public String JobId;
    public int SkillId;
    public int SkillExperienceInYears;
    public int SkillExperienceInMonths;
    public String CreatedBy;
    public String CreatedDate;
    public String UpdatedBy;

    protected JobSkillModel (Parcel in) {
        JobSkillId = in.readInt();
        JobId = in.readString();
        SkillId = in.readInt();
        SkillExperienceInYears = in.readInt();
        SkillExperienceInMonths = in.readInt();
        CreatedBy = in.readString();
        CreatedDate = in.readString();
        UpdatedBy = in.readString();
    }

    @Override
    public void writeToParcel (Parcel dest, int flags) {
        dest.writeInt(JobSkillId);
        dest.writeString(JobId);
        dest.writeInt(SkillId);
        dest.writeInt(SkillExperienceInYears);
        dest.writeInt(SkillExperienceInMonths);
        dest.writeString(CreatedBy);
        dest.writeString(CreatedDate);
        dest.writeString(UpdatedBy);
    }

    @Override
    public int describeContents () {
        return 0;
    }

    public static final Creator<JobSkillModel> CREATOR = new Creator<JobSkillModel>() {
        @Override
        public JobSkillModel createFromParcel (Parcel in) {
            return new JobSkillModel(in);
        }

        @Override
        public JobSkillModel[] newArray (int size) {
            return new JobSkillModel[size];
        }
    };
}
