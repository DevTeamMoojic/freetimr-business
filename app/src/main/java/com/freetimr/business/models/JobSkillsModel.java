package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by varunbarve on 23/3/17.
 */

public class JobSkillsModel implements Parcelable {

    public int SkillId;
    public int SkillExperienceInYears;
    public int SkillExperienceInMonths;
    public String CreatedBy;
    public String CreatedDate;
    public String UpdatedBy;

    public JobSkillsModel (int skillId) {
        SkillId = skillId;
    }

    public JobSkillsModel (int skillId, int skillExperienceInYears, int skillExperienceInMonths) {
        SkillId = skillId;
        SkillExperienceInYears = skillExperienceInYears;
        SkillExperienceInMonths = skillExperienceInMonths;
    }

    protected JobSkillsModel (Parcel in) {
        SkillId = in.readInt();
        SkillExperienceInYears = in.readInt();
        SkillExperienceInMonths = in.readInt();
        CreatedBy = in.readString();
        CreatedDate = in.readString();
        UpdatedBy = in.readString();
    }

    @Override
    public void writeToParcel (Parcel dest, int flags) {
        dest.writeInt(SkillId);
        dest.writeInt(SkillExperienceInYears);
        dest.writeInt(SkillExperienceInMonths);
        dest.writeString(CreatedBy);
        dest.writeString(CreatedDate);
        dest.writeString(UpdatedBy);
    }

    @Override
    public int describeContents () {
        return 0;
    }

    public static final Creator<JobSkillsModel> CREATOR = new Creator<JobSkillsModel>() {
        @Override
        public JobSkillsModel createFromParcel (Parcel in) {
            return new JobSkillsModel(in);
        }

        @Override
        public JobSkillsModel[] newArray (int size) {
            return new JobSkillsModel[size];
        }
    };
}
