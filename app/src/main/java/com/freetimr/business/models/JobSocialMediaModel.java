package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 23-Mar-17.
 */

public class JobSocialMediaModel implements Parcelable {

    public int JobSocialMediaId;
    public String JobId;
    public int SocialMediaId;
    public String CreatedBy;
    public String UpdatedBy;

    protected JobSocialMediaModel (Parcel in) {
        JobSocialMediaId = in.readInt();
        JobId = in.readString();
        SocialMediaId = in.readInt();
        CreatedBy = in.readString();
        UpdatedBy = in.readString();
    }

    @Override
    public void writeToParcel (Parcel dest, int flags) {
        dest.writeInt(JobSocialMediaId);
        dest.writeString(JobId);
        dest.writeInt(SocialMediaId);
        dest.writeString(CreatedBy);
        dest.writeString(UpdatedBy);
    }

    @Override
    public int describeContents () {
        return 0;
    }

    public static final Creator<JobSocialMediaModel> CREATOR = new Creator<JobSocialMediaModel>() {
        @Override
        public JobSocialMediaModel createFromParcel (Parcel in) {
            return new JobSocialMediaModel(in);
        }

        @Override
        public JobSocialMediaModel[] newArray (int size) {
            return new JobSocialMediaModel[size];
        }
    };
}
