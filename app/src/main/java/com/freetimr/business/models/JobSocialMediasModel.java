package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by varunbarve on 23/3/17.
 */

public class JobSocialMediasModel implements Parcelable {

    public int SocialMediaId;
    public String CreatedBy;
    public String UpdatedBy;

    public JobSocialMediasModel (int socialMediaId) {
        SocialMediaId = socialMediaId;
    }

    protected JobSocialMediasModel (Parcel in) {
        SocialMediaId = in.readInt();
        CreatedBy = in.readString();
        UpdatedBy = in.readString();
    }

    @Override
    public void writeToParcel (Parcel dest, int flags) {
        dest.writeInt(SocialMediaId);
        dest.writeString(CreatedBy);
        dest.writeString(UpdatedBy);
    }

    @Override
    public int describeContents () {
        return 0;
    }

    public static final Creator<JobSocialMediasModel> CREATOR = new Creator<JobSocialMediasModel>() {
        @Override
        public JobSocialMediasModel createFromParcel (Parcel in) {
            return new JobSocialMediasModel(in);
        }

        @Override
        public JobSocialMediasModel[] newArray (int size) {
            return new JobSocialMediasModel[size];
        }
    };
}
