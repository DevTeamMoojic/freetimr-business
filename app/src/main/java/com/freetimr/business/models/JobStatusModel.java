package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 21-Mar-17.
 */

public class JobStatusModel implements Parcelable {
    protected JobStatusModel(Parcel in) {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<JobStatusModel> CREATOR = new Creator<JobStatusModel>() {
        @Override
        public JobStatusModel createFromParcel(Parcel in) {
            return new JobStatusModel(in);
        }

        @Override
        public JobStatusModel[] newArray(int size) {
            return new JobStatusModel[size];
        }
    };
}
