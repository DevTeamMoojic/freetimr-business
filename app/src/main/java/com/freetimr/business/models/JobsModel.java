package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Admin on 21-Mar-17.
 */

public class JobsModel implements Parcelable {

    public String JobId;
    public String CompanyId;
    public String CompanyName;
    public String JobTitle;
    public JobTypeModel JobType;
    public int JobTypeId;
    public int JobCategoryId;
    public String JobDescription;
    public String JobEligibility;
    public String JobBenefits;
    public double PayPerDay;
    public double NoOfHours;
    public double NoOfJobDays;
    public Date StartDate;
    public Date EndDate;
    public int JobAvailabilityTimeId;
    public String StartTime;
    public String EndTime;
    public int AgeGroupFrom;
    public int AgeGroupTo;
    public String Gender;
    public boolean IsDisabledAllowed;
    public boolean IsIdProofRequired;
    public boolean IsSocialRequired;
    public int GraduationId;
    public int WorkLocationId;
    public int JobActivityId;
    public boolean IsTrainingProvided;
    public int TrainingPriorDays;
    public int TrainingForNoOfDays;
    public boolean IsPaidForTraining;
    public int NoOfBreaks;
    public int NoOfPositions;
    public String CreatedBy;
    public JobDisabilityModel[] JobDisability;
    @SerializedName("JobLanguage")
    public JobLanguageModel[] JobLanguages;
    @SerializedName("JobLocation")
    public JobLocationModel[] JobLocations;
    @SerializedName("JobSkill")
    public JobSkillModel[] JobSkills;
    @SerializedName("JobSocialMedia")
    public JobSocialMediaModel[] JobSocialMedias;
    public Date CreatedDate;
    public Date UpdatedDate;
    public String UpdatedBy;
    public JobApplicationModel[] JobApplication;
    public WorkLocationModel WorkLocation;
    public JobActivityModel JobActivity;

    public JobsModel() {
    }


    protected JobsModel(Parcel in) {
        JobId = in.readString();
        CompanyId = in.readString();
        CompanyName = in.readString();
        JobTitle = in.readString();
        JobType = in.readParcelable(JobTypeModel.class.getClassLoader());
        JobTypeId = in.readInt();
        JobCategoryId = in.readInt();
        JobDescription = in.readString();
        JobEligibility = in.readString();
        JobBenefits = in.readString();
        PayPerDay = in.readDouble();
        NoOfHours = in.readDouble();
        NoOfJobDays = in.readDouble();
        JobAvailabilityTimeId = in.readInt();
        StartTime = in.readString();
        EndTime = in.readString();
        AgeGroupFrom = in.readInt();
        AgeGroupTo = in.readInt();
        Gender = in.readString();
        IsDisabledAllowed = in.readByte() != 0;
        IsIdProofRequired = in.readByte() != 0;
        IsSocialRequired = in.readByte() != 0;
        GraduationId = in.readInt();
        WorkLocationId = in.readInt();
        JobActivityId = in.readInt();
        IsTrainingProvided = in.readByte() != 0;
        TrainingPriorDays = in.readInt();
        TrainingForNoOfDays = in.readInt();
        IsPaidForTraining = in.readByte() != 0;
        NoOfBreaks = in.readInt();
        NoOfPositions = in.readInt();
        CreatedBy = in.readString();
        JobDisability = in.createTypedArray(JobDisabilityModel.CREATOR);
        JobLanguages = in.createTypedArray(JobLanguageModel.CREATOR);
        JobLocations = in.createTypedArray(JobLocationModel.CREATOR);
        JobSkills = in.createTypedArray(JobSkillModel.CREATOR);
        JobSocialMedias = in.createTypedArray(JobSocialMediaModel.CREATOR);
        UpdatedBy = in.readString();
        JobApplication = in.createTypedArray(JobApplicationModel.CREATOR);
        WorkLocation = in.readParcelable(WorkLocationModel.class.getClassLoader());
        JobActivity = in.readParcelable(JobActivityModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(JobId);
        dest.writeString(CompanyId);
        dest.writeString(CompanyName);
        dest.writeString(JobTitle);
        dest.writeParcelable(JobType, flags);
        dest.writeInt(JobTypeId);
        dest.writeInt(JobCategoryId);
        dest.writeString(JobDescription);
        dest.writeString(JobEligibility);
        dest.writeString(JobBenefits);
        dest.writeDouble(PayPerDay);
        dest.writeDouble(NoOfHours);
        dest.writeDouble(NoOfJobDays);
        dest.writeInt(JobAvailabilityTimeId);
        dest.writeString(StartTime);
        dest.writeString(EndTime);
        dest.writeInt(AgeGroupFrom);
        dest.writeInt(AgeGroupTo);
        dest.writeString(Gender);
        dest.writeByte((byte) (IsDisabledAllowed ? 1 : 0));
        dest.writeByte((byte) (IsIdProofRequired ? 1 : 0));
        dest.writeByte((byte) (IsSocialRequired ? 1 : 0));
        dest.writeInt(GraduationId);
        dest.writeInt(WorkLocationId);
        dest.writeInt(JobActivityId);
        dest.writeByte((byte) (IsTrainingProvided ? 1 : 0));
        dest.writeInt(TrainingPriorDays);
        dest.writeInt(TrainingForNoOfDays);
        dest.writeByte((byte) (IsPaidForTraining ? 1 : 0));
        dest.writeInt(NoOfBreaks);
        dest.writeInt(NoOfPositions);
        dest.writeString(CreatedBy);
        dest.writeTypedArray(JobDisability, flags);
        dest.writeTypedArray(JobLanguages, flags);
        dest.writeTypedArray(JobLocations, flags);
        dest.writeTypedArray(JobSkills, flags);
        dest.writeTypedArray(JobSocialMedias, flags);
        dest.writeString(UpdatedBy);
        dest.writeTypedArray(JobApplication, flags);
        dest.writeParcelable(WorkLocation, flags);
        dest.writeParcelable(JobActivity, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<JobsModel> CREATOR = new Creator<JobsModel>() {
        @Override
        public JobsModel createFromParcel(Parcel in) {
            return new JobsModel(in);
        }

        @Override
        public JobsModel[] newArray(int size) {
            return new JobsModel[size];
        }
    };
}
