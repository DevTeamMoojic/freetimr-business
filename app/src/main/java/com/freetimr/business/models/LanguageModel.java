package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 21-Mar-17.
 */

public class LanguageModel implements Parcelable {

    public int LanguageId;
    public String LanguageName;
    public String CreatedBy;
    public String UpdatedBy;

    public LanguageModel (int languageId) {
        LanguageId = languageId;
    }

    protected LanguageModel (Parcel in) {
        LanguageId = in.readInt();
        LanguageName = in.readString();
        CreatedBy = in.readString();
        UpdatedBy = in.readString();
    }

    @Override
    public void writeToParcel (Parcel dest, int flags) {
        dest.writeInt(LanguageId);
        dest.writeString(LanguageName);
        dest.writeString(CreatedBy);
        dest.writeString(UpdatedBy);
    }

    @Override
    public int describeContents () {
        return 0;
    }

    public static final Creator<LanguageModel> CREATOR = new Creator<LanguageModel>() {
        @Override
        public LanguageModel createFromParcel (Parcel in) {
            return new LanguageModel(in);
        }

        @Override
        public LanguageModel[] newArray (int size) {
            return new LanguageModel[size];
        }
    };
}
