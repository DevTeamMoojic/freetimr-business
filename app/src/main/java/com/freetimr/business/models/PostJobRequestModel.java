package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Admin on 21-Mar-17.
 */

public class PostJobRequestModel implements Parcelable {

    public String JobId;
    public String CompanyId;
    public String CompanyName;
    public String JobTitle;
    public String JobType;
    public int JobTypeId;
    public int JobCategoryId;
    public String JobDescription;
    public String JobEligibility;
    public String JobBenefits;
    public double PayPerDay;
    public double NoOfHours;
    public double NoOfJobDays;
    public Date StartDate;
    public Date EndDate;
    public int JobAvailabilityTimeId;
    public String StartTime;
    public String EndTime;
    public int AgeGroupFrom;
    public int AgeGroupTo;
    public String Gender;
    public boolean IsDisabledAllowed;
    public boolean IsIdProofRequired;
    public boolean IsSocialRequired;
    public int GraduationId;
    public int WorkLocationId;
    public int JobActivityId;
    public boolean IsTrainingProvided;
    public int TrainingPriorDays;
    public int TrainingForNoOfDays;
    public boolean IsPaidForTraining;
    public int NoOfBreaks;
    public int NoOfPositions;
    public String CreatedBy;
    public JobDisabilitiesModel[] JobDisability;
    @SerializedName("JobLanguage")
    public JobLanguagesModel[] JobLanguages;
    @SerializedName("JobLocation")
    public JobLocationsModel[] JobLocations;
    @SerializedName("JobSkill")
    public JobSkillsModel[] JobSkills;
    @SerializedName("JobSocialMedia")
    public JobSocialMediasModel[] JobSocialMedias;

    public PostJobRequestModel () {
    }


    protected PostJobRequestModel (Parcel in) {
        JobId = in.readString();
        CompanyId = in.readString();
        JobTitle = in.readString();
        CompanyName = in.readString();
        JobType = in.readString();
        JobTypeId = in.readInt();
        JobCategoryId = in.readInt();
        JobDescription = in.readString();
        JobEligibility = in.readString();
        JobBenefits = in.readString();
        PayPerDay = in.readDouble();
        NoOfHours = in.readDouble();
        NoOfJobDays = in.readDouble();
        JobAvailabilityTimeId = in.readInt();
        StartTime = in.readString();
        EndTime = in.readString();
        AgeGroupFrom = in.readInt();
        AgeGroupTo = in.readInt();
        Gender = in.readString();
        IsDisabledAllowed = in.readByte() != 0;
        IsIdProofRequired = in.readByte() != 0;
        IsSocialRequired = in.readByte() != 0;
        GraduationId = in.readInt();
        WorkLocationId = in.readInt();
        JobActivityId = in.readInt();
        IsTrainingProvided = in.readByte() != 0;
        TrainingPriorDays = in.readInt();
        TrainingForNoOfDays = in.readInt();
        IsPaidForTraining = in.readByte() != 0;
        NoOfBreaks = in.readInt();
        NoOfPositions = in.readInt();
        CreatedBy = in.readString();
        JobDisability = in.createTypedArray(JobDisabilitiesModel.CREATOR);
        JobLanguages = in.createTypedArray(JobLanguagesModel.CREATOR);
        JobLocations = in.createTypedArray(JobLocationsModel.CREATOR);
        JobSkills = in.createTypedArray(JobSkillsModel.CREATOR);
        JobSocialMedias = in.createTypedArray(JobSocialMediasModel.CREATOR);
        StartDate = (Date) in.readSerializable();
        EndDate = (Date) in.readSerializable();
    }

    @Override
    public void writeToParcel (Parcel dest, int flags) {
        dest.writeString(JobId);
        dest.writeString(CompanyId);
        dest.writeString(JobTitle);
        dest.writeString(CompanyName);
        dest.writeString(JobType);
        dest.writeInt(JobTypeId);
        dest.writeInt(JobCategoryId);
        dest.writeString(JobDescription);
        dest.writeString(JobEligibility);
        dest.writeString(JobBenefits);
        dest.writeDouble(PayPerDay);
        dest.writeDouble(NoOfHours);
        dest.writeDouble(NoOfJobDays);
        dest.writeInt(JobAvailabilityTimeId);
        dest.writeString(StartTime);
        dest.writeString(EndTime);
        dest.writeInt(AgeGroupFrom);
        dest.writeInt(AgeGroupTo);
        dest.writeString(Gender);
        dest.writeByte((byte) (IsDisabledAllowed ? 1 : 0));
        dest.writeByte((byte) (IsIdProofRequired ? 1 : 0));
        dest.writeByte((byte) (IsSocialRequired ? 1 : 0));
        dest.writeInt(GraduationId);
        dest.writeInt(WorkLocationId);
        dest.writeInt(JobActivityId);
        dest.writeByte((byte) (IsTrainingProvided ? 1 : 0));
        dest.writeInt(TrainingPriorDays);
        dest.writeInt(TrainingForNoOfDays);
        dest.writeByte((byte) (IsPaidForTraining ? 1 : 0));
        dest.writeInt(NoOfBreaks);
        dest.writeInt(NoOfPositions);
        dest.writeString(CreatedBy);
        dest.writeTypedArray(JobDisability, flags);
        dest.writeTypedArray(JobLanguages, flags);
        dest.writeTypedArray(JobLocations, flags);
        dest.writeTypedArray(JobSkills, flags);
        dest.writeTypedArray(JobSocialMedias, flags);
        dest.writeSerializable(StartDate);
        dest.writeSerializable(EndDate);
    }

    @Override
    public int describeContents () {
        return 0;
    }

    public static final Creator<PostJobRequestModel> CREATOR = new Creator<PostJobRequestModel>() {
        @Override
        public PostJobRequestModel createFromParcel (Parcel in) {
            return new PostJobRequestModel(in);
        }

        @Override
        public PostJobRequestModel[] newArray (int size) {
            return new PostJobRequestModel[size];
        }
    };
}
