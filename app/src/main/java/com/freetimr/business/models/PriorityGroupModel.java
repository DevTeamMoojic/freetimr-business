package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 25-Mar-17.
 */

public class PriorityGroupModel implements Parcelable {

    public int PriorityGroupId;
    public String PriorityGroupName;

    protected PriorityGroupModel(Parcel in) {
        PriorityGroupId = in.readInt();
        PriorityGroupName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(PriorityGroupId);
        dest.writeString(PriorityGroupName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PriorityGroupModel> CREATOR = new Creator<PriorityGroupModel>() {
        @Override
        public PriorityGroupModel createFromParcel(Parcel in) {
            return new PriorityGroupModel(in);
        }

        @Override
        public PriorityGroupModel[] newArray(int size) {
            return new PriorityGroupModel[size];
        }
    };
}
