package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 21-Mar-17.
 */

public class SkillModel implements Parcelable {

    public int SkillId;
    public String SkillName;
    public String SkillExperienceInYears;
    public String SkillExperienceInMonths;
    public String CreatedBy;
    public String CreatedDate;
    public String UpdatedBy;

    public SkillModel(int skillId, String skillName) {
        SkillId = skillId;
        SkillName = skillName;
    }

    protected SkillModel (Parcel in) {
        SkillId = in.readInt();
        SkillName = in.readString();
        SkillExperienceInYears = in.readString();
        SkillExperienceInMonths = in.readString();
        CreatedBy = in.readString();
        CreatedDate = in.readString();
        UpdatedBy = in.readString();
    }

    @Override
    public void writeToParcel (Parcel dest, int flags) {
        dest.writeInt(SkillId);
        dest.writeString(SkillName);
        dest.writeString(SkillExperienceInYears);
        dest.writeString(SkillExperienceInMonths);
        dest.writeString(CreatedBy);
        dest.writeString(CreatedDate);
        dest.writeString(UpdatedBy);
    }

    @Override
    public int describeContents () {
        return 0;
    }

    public static final Creator<SkillModel> CREATOR = new Creator<SkillModel>() {
        @Override
        public SkillModel createFromParcel (Parcel in) {
            return new SkillModel(in);
        }

        @Override
        public SkillModel[] newArray (int size) {
            return new SkillModel[size];
        }
    };
}
