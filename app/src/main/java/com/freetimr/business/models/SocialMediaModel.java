package com.freetimr.business.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 21-Mar-17.
 */

public class SocialMediaModel implements Parcelable{

    public int SocialMediaId;
    public String SocialMediaName;

    public SocialMediaModel (int socialMediaId, String socialMediaName) {
        SocialMediaId = socialMediaId;
        SocialMediaName = socialMediaName;
    }

    protected SocialMediaModel(Parcel in) {
        SocialMediaId = in.readInt();
        SocialMediaName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(SocialMediaId);
        dest.writeString(SocialMediaName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SocialMediaModel> CREATOR = new Creator<SocialMediaModel>() {
        @Override
        public SocialMediaModel createFromParcel(Parcel in) {
            return new SocialMediaModel(in);
        }

        @Override
        public SocialMediaModel[] newArray(int size) {
            return new SocialMediaModel[size];
        }
    };
}
