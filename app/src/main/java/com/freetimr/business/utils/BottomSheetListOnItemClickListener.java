package com.freetimr.business.utils;

import java.io.Serializable;

/**
 * Created by Admin on 21-Mar-17.
 */

public interface BottomSheetListOnItemClickListener extends Serializable {

    void onListItemSelected(int position);
}
