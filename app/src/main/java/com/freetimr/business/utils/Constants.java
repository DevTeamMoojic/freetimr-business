package com.freetimr.business.utils;

/**
 * Created by Admin on 09-Dec-16.
 */

public class Constants {

    public static String LOG_TAG = "FREETIMR";

    public static String KEY_CANDIDATE_INFO = "FreeTimrCandidateInfo";

    public static String BLACK_FONT = "Montserrat-Black.otf";
    public static String BOLD_FONT = "Montserrat-Bold.ttf";
    public static String EXTRA_BOLD_FONT = "Montserrat-ExtraBold.otf";
    public static String LIGHT_FONT = "Montserrat-Light.otf";
    public static String REGULAR_FONT = "Montserrat-Regular.ttf";
    public static String SEMI_BOLD_FONT = "Montserrat-SemiBold.otf";
    public static String ULTRA_LIGHT_FONT = "Montserrat-UltraLight.otf";

    public static int RESPONSE_CREATED = 201;
    public static int RESPONSE_SUCCESS = 200;
    public static int RESPONSE_BAD_REQUEST = 400;

    public static String KEY_CANDIDATE_MASTERS = "CandidateMasters";
    public static String KEY_COMPANY_MODEL = "CompanyModel";
    public static String KEY_JOB_MASTER = "JobMaster";

    public static final int HOME_ITEM_TYPE_JOB_APPLICATIONS = 1;
    public static final int HOME_ITEM_TYPE_POST_A_JOB = 2;
    public static final int HOME_ITEM_TYPE_CANDIDATE_SEARCH = 3;
    public static final int HOME_ITEM_TYPE_MESSAGE = 4;
    public static final int HOME_ITEM_TYPE_MY_ACCOUNT = 5;
    public static final int HOME_ITEM_TYPE_SUPPORT = 6;

    public static String IS_FIRST_JOB_POSTED = "IsFirstJobPosted";

    public static String EMPLOYEE_ADMIN_INFO = "EmployeeInfo";
}
