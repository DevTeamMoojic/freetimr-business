package com.freetimr.business.utils;

/**
 * Created by varunbarve on 14/3/17.
 */

public class FreeTimrURLs {

    public static NetworkConnector retrofitNetworkHandler;

    public static final String HOST = "http://137.59.54.53/FreeTimr/api/";

    public static final String REGISTER_COMPANY = "Company/Register";
    public static final String GET_COMPANY = "Company/";

    public static final String GET_JOB_MASTER = "Masters/JobMasters";
    public static final String GET_JOB_HEADERS = "Masters/JobMastersByCompany/";
    public static final String GET_JOB_BY_COMPANY = "Company/Job/GetJobsByCompany/";
    public static final String GET_CANDIDATE_PROFILE_BY_CANDIDATE_ID = "Candidate/Profile/GetProfileByCandidateId/";
    public static final String GET_JOB_BY_ID = "Company/Job/";
    public static final String GET_APPLIED_CANDIDATES = "Company/Job/GetAppliedCandidateByJobId/";
    public static final String GET_SHORTLISTED_CANDIDATES = "Company/Job/GetshortlistedCandidateByJobId/";
    public static final String GET_REJECTED_CANDIDATES = "Company/Job/GetRejectedCandidateByJobId/";

}
