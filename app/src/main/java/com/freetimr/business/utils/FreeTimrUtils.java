package com.freetimr.business.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentManager;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.freetimr.business.activities.SplashActivity;
import com.freetimr.business.fragments.MyBottomSheetDialogFragment;
import com.freetimr.business.models.AvailabilityTimeModel;
import com.freetimr.business.models.CityModel;
import com.freetimr.business.models.CompanyModel;
import com.freetimr.business.models.DisabilityModel;
import com.freetimr.business.models.EmployeeModel;
import com.freetimr.business.models.GraduationModel;
import com.freetimr.business.models.JobActivityModel;
import com.freetimr.business.models.JobApplicationStatusModel;
import com.freetimr.business.models.JobCategoryModel;
import com.freetimr.business.models.JobLanguagesModel;
import com.freetimr.business.models.JobMastersModel;
import com.freetimr.business.models.JobTypeModel;
import com.freetimr.business.models.LanguageModel;
import com.freetimr.business.models.SkillModel;
import com.freetimr.business.models.SocialMediaModel;
import com.freetimr.business.models.WorkLocationModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by varunbarve on 14/3/17.
 */

public class FreeTimrUtils {

    static DialogDisplay showDialog = new FreeTimrUtils.DialogDisplay();
    static LoadingDialogBox pDialog;
    static Runnable cancelDialog = new Runnable() {
        @Override
        public void run () {
            try {
                if (pDialog != null)
                    pDialog.cancel();
                pDialog = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    static class DialogDisplay implements Runnable {
        Activity activity;
        String message;

        public DialogDisplay updateActivity (Activity activity, String message) {
            this.activity = activity;
            this.message = message;
            return this;
        }


        @Override
        public void run () {
            try {
                if (pDialog != null) {
                    pDialog.cancel();
                }
                pDialog = new LoadingDialogBox(activity);
                pDialog.setCanceledOnTouchOutside(false);
                pDialog.setCancelable(false);
                pDialog.setMessage(message);
                pDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void cancelCurrentDialog (Activity activity) {
        if (pDialog != null && activity != null) {
            new Handler().postDelayed(cancelDialog, 150);
        }
    }

    public static void showProgressDialog (final Activity activity) {
        activity.runOnUiThread(showDialog.updateActivity(activity, null));
    }

    public static boolean isValidEmailId (String email) {
        String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern p = Pattern.compile(emailPattern);
        Matcher m = p.matcher(email);
        return m.matches();
    }

    public static void showMessage (Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void log (String logData) {
        Log.e(Constants.LOG_TAG, logData + "");
    }

    public static NetworkConnector getRetrofit () {
        if (FreeTimrURLs.retrofitNetworkHandler == null) {

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                    .readTimeout(120, TimeUnit.SECONDS)
                    .writeTimeout(120, TimeUnit.SECONDS)
                    .connectTimeout(120, TimeUnit.SECONDS);

            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept (Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder().header("Content-Type", "application/json").header("APIAccessKey", "FreeTimrDev");

                    Request request = requestBuilder.build();

                    return chain.proceed(request);
                }
            });

            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(Date.class, new GsonDateDeSerializer());
            Retrofit retrofit = new Retrofit.Builder()
                    .client(httpClient.build())
                    .baseUrl(FreeTimrURLs.HOST)
                    .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                    .build();
            FreeTimrURLs.retrofitNetworkHandler = retrofit.create(NetworkConnector.class);
        }


        return FreeTimrURLs.retrofitNetworkHandler;
    }

    public static Gson newGson () {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Date.class, new GsonDateSerializer());
        return builder.create();
    }

    /**
     * Encode Bitmap to Base64 String
     *
     * @param image Bitmap Image
     * @return Base64 String
     */
    public static String encodeTobase64 (Bitmap image) {
        Bitmap immage = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immage.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        Log.d("Image Log:", imageEncoded);
        return imageEncoded;
    }

    /**
     * Decode Base64 String to Bitmap
     *
     * @param input String Base64
     * @return Bitmap
     */
    public static Bitmap decodeBase64 (String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory
                .decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    public static void showBottomSheetDialog (FragmentManager fragmentManager, String title, String[] strings, BottomSheetListOnItemClickListener bottomSheetListOnItemClickListener) {
        BottomSheetDialogFragment bottomSheetDialogFragment = MyBottomSheetDialogFragment.getInstance(title, strings, bottomSheetListOnItemClickListener);
        bottomSheetDialogFragment.show(fragmentManager, "Dialog");
    }

    public static ArrayList<JobTypeModel> getJobTypeList (Context context) {
        ArrayList<JobTypeModel> jobTypeModelsList = new ArrayList<>();

        String jobCompanyStr = SharedPreferenceStore.getValue(context, Constants.KEY_JOB_MASTER, "");

        JobMastersModel jobMastersModel = newGson().fromJson(jobCompanyStr, JobMastersModel.class);

        JobTypeModel[] jobTypeModels = jobMastersModel.JobType;

        for (int i = 0; i < jobTypeModels.length; i++) {
            jobTypeModelsList.add(jobTypeModels[i]);
        }
        return jobTypeModelsList;
    }

    public static ArrayList<JobCategoryModel> getJobCategoryList (Context context) {
        ArrayList<JobCategoryModel> jobCategoryModelsList = new ArrayList<>();

        String jobCompanyStr = SharedPreferenceStore.getValue(context, Constants.KEY_JOB_MASTER, "");

        JobMastersModel jobMastersModel = newGson().fromJson(jobCompanyStr, JobMastersModel.class);

        JobCategoryModel[] jobCategoryModels = jobMastersModel.JobCategory;

        for (int i = 0; i < jobCategoryModels.length; i++) {
            jobCategoryModelsList.add(jobCategoryModels[i]);
        }
        return jobCategoryModelsList;
    }

    public static ArrayList<WorkLocationModel> getWorkLocationList (Context context) {
        ArrayList<WorkLocationModel> workLocationModelsList = new ArrayList<>();

        String jobCompanyStr = SharedPreferenceStore.getValue(context, Constants.KEY_JOB_MASTER, "");

        JobMastersModel jobMastersModel = newGson().fromJson(jobCompanyStr, JobMastersModel.class);

        WorkLocationModel[] workLocationModels = jobMastersModel.WorkLocation;

        for (int i = 0; i < workLocationModels.length; i++) {
            workLocationModelsList.add(workLocationModels[i]);
        }
        return workLocationModelsList;
    }

    public static ArrayList<CityModel> getCityList (Context context) {
        ArrayList<CityModel> cityModelsList = new ArrayList<>();

        String jobCompanyStr = SharedPreferenceStore.getValue(context, Constants.KEY_JOB_MASTER, "");

        JobMastersModel jobMastersModel = newGson().fromJson(jobCompanyStr, JobMastersModel.class);

        CityModel[] cityModels = jobMastersModel.City;

        for (int i = 0; i < cityModels.length; i++) {
            cityModelsList.add(cityModels[i]);
        }
        return cityModelsList;
    }

    public static ArrayList<LanguageModel> getLanguageList (Context context) {
        ArrayList<LanguageModel> languageModelsList = new ArrayList<>();

        String jobCompanyStr = SharedPreferenceStore.getValue(context, Constants.KEY_JOB_MASTER, "");

        JobMastersModel jobMastersModel = newGson().fromJson(jobCompanyStr, JobMastersModel.class);

        LanguageModel[] languageModels = jobMastersModel.Language;

        for (int i = 0; i < languageModels.length; i++) {
            languageModelsList.add(languageModels[i]);
        }
        return languageModelsList;
    }

    public static ArrayList<SkillModel> getSkillsList (Context context) {
        ArrayList<SkillModel> skillModelArrayList = new ArrayList<>();

        String jobCompanyStr = SharedPreferenceStore.getValue(context, Constants.KEY_JOB_MASTER, "");

        JobMastersModel jobMastersModel = newGson().fromJson(jobCompanyStr, JobMastersModel.class);

        SkillModel[] skillModels = jobMastersModel.Skill;

        for (int i = 0; i < skillModels.length; i++) {
            skillModelArrayList.add(skillModels[i]);
        }
        return skillModelArrayList;
    }

    public static ArrayList<DisabilityModel> getDisableList (Context context) {
        ArrayList<DisabilityModel> disabilityModelList = new ArrayList<>();

        String jobCompanyStr = SharedPreferenceStore.getValue(context, Constants.KEY_JOB_MASTER, "");

        JobMastersModel jobMastersModel = newGson().fromJson(jobCompanyStr, JobMastersModel.class);

        DisabilityModel[] disabilityModels = jobMastersModel.Disability;

        for (int i = 0; i < disabilityModels.length; i++) {
            disabilityModelList.add(disabilityModels[i]);
        }
        return disabilityModelList;
    }

    public static ArrayList<SocialMediaModel> getSocialCredsList (Context context) {
        ArrayList<SocialMediaModel> socialMediaModelsList = new ArrayList<>();

        String jobCompanyStr = SharedPreferenceStore.getValue(context, Constants.KEY_JOB_MASTER, "");

        JobMastersModel jobMastersModel = newGson().fromJson(jobCompanyStr, JobMastersModel.class);

        SocialMediaModel[] socialMediaModels = jobMastersModel.SocialMedia;

        for (int i = 0; i < socialMediaModels.length; i++) {
            socialMediaModelsList.add(socialMediaModels[i]);
        }
        return socialMediaModelsList;
    }

    public static ArrayList<JobApplicationStatusModel> getJobApplicationStatusList (Context context) {
        ArrayList<JobApplicationStatusModel> socialMediaModelsList = new ArrayList<>();

        String jobCompanyStr = SharedPreferenceStore.getValue(context, Constants.KEY_JOB_MASTER, "");

        JobMastersModel jobMastersModel = newGson().fromJson(jobCompanyStr, JobMastersModel.class);

        JobApplicationStatusModel[] jobApplicationStatusModels = jobMastersModel.JobApplicationStatus;

        for (int i = 0; i < jobApplicationStatusModels.length; i++) {
            socialMediaModelsList.add(jobApplicationStatusModels[i]);
        }
        return socialMediaModelsList;
    }

    public static String getCityNameById (Context context, int id) {
        ArrayList<CityModel> cityModelsList = getCityList(context);
        for (int i = 0; i < cityModelsList.size(); i++) {
            if (cityModelsList.get(i).CityId == id) {
                return cityModelsList.get(i).CityName;
            }
        }
        return "";
    }

    public static CompanyModel getCompanyModel (Context context) {
        String companyModelStr = SharedPreferenceStore.getValue(context, Constants.KEY_COMPANY_MODEL, "");
        CompanyModel companyModel = newGson().fromJson(companyModelStr, CompanyModel.class);
        return companyModel;
    }

    public static JobMastersModel getJobMasterModel (Context context) {
        String jobMastersModelStr = SharedPreferenceStore.getValue(context, Constants.KEY_JOB_MASTER, "");
        JobMastersModel jobMastersModel = newGson().fromJson(jobMastersModelStr, JobMastersModel.class);
        return jobMastersModel;
    }

    public static String changeFormat (String time) {
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
            final Date dateObj = sdf.parse(time);
            FreeTimrUtils.log("" + dateObj);
            FreeTimrUtils.log("" + new SimpleDateFormat("K:mm").format(dateObj));

            return "" + new SimpleDateFormat("K:mm").format(dateObj);

        } catch (final ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void saveAdminEmployeeInfo (Context context, String employeeInfo) {
        SharedPreferenceStore.storeValue(context, Constants.EMPLOYEE_ADMIN_INFO, employeeInfo);
    }

    public static EmployeeModel getAdminEmployeeInfo (Context context) {
        EmployeeModel employeeModel = FreeTimrUtils.newGson().fromJson(SharedPreferenceStore.getValue(context, Constants.EMPLOYEE_ADMIN_INFO, ""), EmployeeModel.class);
        return employeeModel;
    }

    public static String getSkillName (Context context, int id) {
        String name = "";

        SkillModel[] skills = getJobMasterModel(context).Skill;
        for (int i = 0; i < skills.length; i++) {
            if (skills[i].SkillId == id) {
                name = skills[i].SkillName;
            }
        }
        return name;
    }

    public static String getCategoryNameById (Context context, int id) {
        String name = "";

        JobCategoryModel[] category = getJobMasterModel(context).JobCategory;
        for (int i = 0; i < category.length; i++) {
            if (category[i].JobCategoryId == id) {
                name = category[i].JobCategoryName;
            }
        }
        return name;
    }

    public static String getJobTypeById (Context context, int id) {
        String name = "";

        JobTypeModel[] jobType = getJobMasterModel(context).JobType;
        for (int i = 0; i < jobType.length; i++) {
            if (jobType[i].JobTypeId == id) {
                name = jobType[i].JobTypeName;
            }
        }
        return name;
    }

    public static String getLanguageNameById (Context context, int id) {
        String name = "";

        LanguageModel[] language = getJobMasterModel(context).Language;
        for (int i = 0; i < language.length; i++) {
            if (language[i].LanguageId == id) {
                name = language[i].LanguageName;
            }
        }
        return name;
    }

    public static String getGraduationNameById (Context context, int id) {
        String name = "";

        GraduationModel[] graduation = getJobMasterModel(context).Graduation;
        for (int i = 0; i < graduation.length; i++) {
            if (graduation[i].GraduationId == id) {
                name = graduation[i].GraduationName;
            }
        }
        return name;
    }

    public static String getAvailabilityTimeById (Context context, int id) {
        String name = "";

        AvailabilityTimeModel[] availabilityTime = getJobMasterModel(context).AvailabilityTime;
        for (int i = 0; i < availabilityTime.length; i++) {
            if (availabilityTime[i].AvailabilityTimeId == id) {
                name = availabilityTime[i].AvailabilityTimeName;
            }
        }
        return name;
    }

    public static String getWorkLocationById (Context context, int id) {
        String name = "";

        WorkLocationModel[] workLocation = getJobMasterModel(context).WorkLocation;
        for (int i = 0; i < workLocation.length; i++) {
            if (workLocation[i].WorkLocationId == id) {
                name = workLocation[i].WorkLocationName;
            }
        }
        return name;
    }

    public static String getJobActivityById (Context context, int id) {
        String name = "";

        JobActivityModel[] jobActivity = getJobMasterModel(context).JobActivity;
        for (int i = 0; i < jobActivity.length; i++) {
            if (jobActivity[i].JobActivityId == id) {
                name = jobActivity[i].JobActivityName;
            }
        }
        return name;
    }

    public static String getDisabilityById (Context context, int id) {
        String name = "";

        DisabilityModel[] disability = getJobMasterModel(context).Disability;
        for (int i = 0; i < disability.length; i++) {
            if (disability[i].DisabilityId == id) {
                name = disability[i].DisabilityName;
            }
        }
        return name;
    }

    public static String getSocialCredsById (Context context, int id) {
        String name = "";

        SocialMediaModel[] socialMedia = getJobMasterModel(context).SocialMedia;
        for (int i = 0; i < socialMedia.length; i++) {
            if (socialMedia[i].SocialMediaId == id) {
                name = socialMedia[i].SocialMediaName;
            }
        }
        return name;
    }
}
