package com.freetimr.business.utils;

import com.freetimr.business.activities.LoginActivity;
import com.freetimr.business.fragments.PostAJobStepOneFragments;
import com.freetimr.business.models.CandidateProfileModel;
import com.freetimr.business.models.CandidateSearchRequestModel;
import com.freetimr.business.models.CloseJobModel;
import com.freetimr.business.models.CompanyModel;
import com.freetimr.business.models.EmployeeModel;
import com.freetimr.business.models.JobApplicationModel;
import com.freetimr.business.models.JobHeaderModel;
import com.freetimr.business.models.JobMastersModel;
import com.freetimr.business.models.JobsModel;
import com.freetimr.business.models.PostJobRequestModel;
import com.freetimr.business.models.ShortListCandidateModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Url;

/**
 * Created by varunbarve on 14/3/17.
 */

public interface NetworkConnector {

    @POST("Company/Register")
    Call<String> registerCompany (@Body CompanyModel companyModel);

    @GET
    Call<JobMastersModel> getJobMaster (@Url String url);

    @GET
    Call<JobsModel[]> getJobsByCompanyId (@Url String url);

    @GET
    Call<CompanyModel> getCompanyModel (@Url String url);

    @GET
    Call<PostAJobStepOneFragments.JobHeaderRequest> getJobHeader (@Url String url);

    @POST("Company/Job/PostJob")
    Call<PostJobRequestModel> postAJob (@Body PostJobRequestModel postJobRequestModel);

    @POST("Company/SearchCandidate")
    Call<CandidateProfileModel[]> candidateSearch (@Body CandidateSearchRequestModel candidateSearchRequestModel);

    @POST("Company/Employee/AuthEmployee")
    Call<EmployeeModel> loginEmployee (@Body LoginActivity.LoginModel body);

    @GET
    Call<CandidateProfileModel[]> getCandidateProfileInfo (@Url String url);

    @PUT("Company/Job/Update")
    Call<String> updateJob (@Body PostJobRequestModel model);

    @GET
    Call<JobsModel> getJobById (@Url String url);

    @POST("Company/Job/CloseJob")
    Call<Integer> closeJob (@Body CloseJobModel model);

    @POST("Company/Job/Shortlist")
    Call<JobApplicationModel> shortListCandidate (@Body ShortListCandidateModel model);

    @GET
    Call<JobApplicationModel[]> getAppliedShortListedRejected (@Url String url);
}
