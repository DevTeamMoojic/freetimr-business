package com.freetimr.business.utils;

/**
 * Created by Admin on 22-Dec-16.
 */

public interface OnScrollChangedCallback {
    void onScroll(int l, int t);
}
